# hosp-dev.tar.gzをインポートして開発環境構築

```
wsl --set-default-version 2
mkdir %USERPROFILE%\AppData\Local\Packages\hosp-dev
wsl --import hosp-dev %USERPROFILE%\AppData\Local\Packages\hosp-dev hosp-dev.tar.gz
wsl -d hosp-dev
/mnt/c/git/hospital/vm/wsl/start.sh
```

# コーディング規約
## 変数名
- Go言語、TypeScript：キャメルケース（myValue）
- DB、json：スネークケース（my_value）
- html（タグ名、属性名など）：ケバブケース（my-value）

# フロントエンドコンパイル（.ts→.js）

```
# cmd.exe
wsl -d hosp-dev

# wsl
cd /mnt/c/git/hospital/src/hospital
make jsdev

# 変更した箇所だけを自動的にコンパイルしたい場合は`make jsdev`の代わりに以下をご利用ください
make jswatch
```

# サーバーサイトコンパイル

```
# cmd.exe
wsl -d hosp-dev

# wsl
cd /mnt/c/git/hospital/src/hospital
make build

# とりあえず上記でビルドせず、vscodeからF5でデバッグ実行して動作確認
```

# DB初期化
- デバッグ実行やpgAdmin4などを終了し、DBのセッションを0にする。
- WSL上のbashから下記コマンドを実行する。

```
# cmd.exe
wsl -d hosp-dev

# wsl
# DB初期化
cd /mnt/c/git/hospital/src/hospital
sudo -u postgres psql -f init.sql
```

- vscodeを起動してWSLに接続し、hosp/hosp_test.goを開く。
- TestOnepath()の直上に表示されている"run test"をクリックする（ID80001の病院作成、ID10001のユーザー作成など）。


# hosp-dev.tar.gzの作成方法

```
# cmd.exe
wsl --set-default-version 2
mkdir %USERPROFILE%\AppData\Local\Packages\hosp-dev
wsl --import hosp-dev %USERPROFILE%\AppData\Local\Packages\hosp-dev focal-server-cloudimg-amd64-wsl.rootfs.tar.gz
(wsl -s hosp-dev)
wsl -d hosp-dev

# wsl
echo '' >> ~/.bashrc
echo "PS1='\W\$ '" >> ~/.bashrc
echo '[automount]' | tee /etc/wsl.conf
echo 'enabled = true' | tee -a /etc/wsl.conf
echo 'options = "metadata"' | tee -a /etc/wsl.conf
apt update
apt -y upgrade
exit

# cmd.exe
wsl --shutdown
wsl -d hosp-dev
# wsl
cd /mnt/c/git/hospital/vm/wsl
./init.sh dev
./start.sh
sudo -u postgres psql -f ../../src/hospital/init.sql

# vscodeを起動し、wslに接続、"/mnt/c/git/hospital/src/hospital/"を開く
# vscodeのWSL側に以下の拡張機能をインストール
## golang.go
## ms-ceintl.vscode-language-pack-ja
## mosapride.zenkaku
## redhat.vscode-yaml
## arjun.swagger-viewer
# Ctrl+Shift+pから"Go: Install/Update Tools"を実行し、すべてをインストール

# 
npm install -g npm
exit

# cmd.exe

```
