#!/bin/bash

PGVER=12
POSTGRES_USER='postgres'
POSTGRES_PASSWORD='nelt0001'
GOVER='1.15.6'
HOSP_PATH='/mnt/c/git/hospital'

# ready
## mkdir %USERPROFILE%\AppData\Local\Packages\hosp-dev
## wsl --import hosp-dev %USERPROFILE%\AppData\Local\Packages\hosp-dev focal-server-cloudimg-amd64-wsl.rootfs.tar.gz
## wsl -d hosp-dev
## cd /mnt/c/git/hospital/vm/wsl
## ./init.sh dev

# WSL
{ \
    echo '[automount]'; \
    echo 'enabled = true'; \
    echo 'root = /mnt/'; \
    echo 'options = "metadata,umask=22,fmask=11"'; \
    echo 'mountFsTab = true'; \
    echo ''; \
    echo '[network]'; \
    echo 'generateHosts = false'; \
    echo 'generateResolvConf = true'; \
} > /etc/wsl.conf

# bash
echo '' >> ~/.bashrc
echo "PS1='\W\$ '" >> ~/.bashrc
source ~/.bashrc

# OS
apt update
apt -y upgrade
## systemd
#curl https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/master/files/docker/systemctl3.py -o systemctl.py
#mv /usr/bin/systemctl /usr/bin/systemctl~
#mv systemctl.py /usr/bin/systemctl
#chmod +x /usr/bin/systemctl
## common
apt -y install ca-certificates tzdata gnupg gnupg2 lsb-release vim iproute2 iputils-ping lsof --no-install-recommends

# DB
## PostgreSQL
#curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
#echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
#apt update
apt -y install postgresql-${PGVER} --no-install-recommends
sed -i -e "s/#listen_addresses = 'localhost'/listen_addresses = '0.0.0.0'/" /etc/postgresql/${PGVER}/main/postgresql.conf
sed -i -e "s/ssl = on/ssl = off/" /etc/postgresql/${PGVER}/main/postgresql.conf
mv /etc/postgresql/${PGVER}/main/pg_hba.conf /etc/postgresql/${PGVER}/main/pg_hba.conf~
echo "local all all     peer" > /etc/postgresql/${PGVER}/main/pg_hba.conf
echo "host  all all all trust" >> /etc/postgresql/${PGVER}/main/pg_hba.conf
#echo "host  all all all password" >> /etc/postgresql/${PGVER}/main/pg_hba.conf
rm -rf /var/lib/postgresql/${PGVER}/main
echo $POSTGRES_PASSWORD > /postgres_password
su postgres -c "/usr/lib/postgresql/${PGVER}/bin/initdb -D /var/lib/postgresql/${PGVER}/main -E UTF8 --no-locale -U ${POSTGRES_USER} --pwfile=/postgres_password"
rm -f /postgres_password

# Web
## nginx
echo "deb http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" | tee /etc/apt/sources.list.d/nginx.list
curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add -
apt-key fingerprint ABF5BD827BD9BF62
apt update
apt -y install nginx --no-install-recommends
cp movacal.conf /etc/nginx/conf.d/
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf~
sed -i -e 's/worker_processes  1;/worker_processes  auto;\nworker_rlimit_nofile  32768;/' /etc/nginx/nginx.conf
sed -i -e 's/worker_connections  1024;/worker_connections  8192;/' /etc/nginx/nginx.conf
sed -i -e 's/"$http_x_forwarded_for"/"$http_x_forwarded_for" "$ssl_client_s_dn_ou" "$http_x_account" $request_time $server_port $ssl_protocol $ssl_cipher/' /etc/nginx/nginx.conf
sed -i -e 's/keepalive_timeout  65;/keepalive_timeout  5;/' /etc/nginx/nginx.conf
## redis
apt -y install redis --no-install-recommends
#sed -i -e "s/daemonize yes/daemonize no/" /etc/redis/redis.conf

if [ $# -ne 1 ]; then
    exit 0
fi

# Dev
apt -y install gcc make git
curl -o go.tar.gz -L https://golang.org/dl/go${GOVER}.linux-amd64.tar.gz
tar -C /usr/local -zxf go.tar.gz
/bin/rm go.tar.gz
echo 'export PATH=$PATH:/usr/local/go/bin:/root/go/bin' >> ~/.bashrc
echo 'export DBNAME=postgres' >> ~/.bashrc
echo "export DBSOURCE='host=127.0.0.1 port=5432 dbname=db_0 user=postgres password=nelt0001 sslmode=disable'" >> ~/.bashrc
source ~/.bashrc
go get -u -v github.com/alvaroloes/enumer
go get -u -v github.com/haya14busa/goverage
apt -y install nodejs npm --no-install-recommends
npm install -g n
n stable
apt -y purge nodejs npm
apt -y autoremove
#sed -i -e 's/user  nginx/user  root/' /etc/nginx/nginx.conf
echo "" >> /etc/nginx/conf.d/movacal.conf
echo "disable_symlinks off;" >> /etc/nginx/conf.d/movacal.conf
mkdir -p /var/www
ln -s ${HOSP_PATH}/src/hospital/assets /var/www/

# install HTML to PDF library
wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.focal_amd64.deb
apt install ./wkhtmltox_0.12.6-1.focal_amd64.deb
# install Japanese fonts for PDF library
# wget https://noto-website.storage.googleapis.com/pkgs/Noto-unhinted.zip \
# && unzip -d NotoSansJapanese Noto-unhinted.zip \
# && mkdir -p /usr/share/fonts/opentype \
# && mv -fv ./NotoSansJapanese /usr/share/fonts/opentype/NotoSansJapanese \
# && rm -rfv Noto-unhinted.zip \
# && fc-cache -fv
