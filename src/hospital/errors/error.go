package errors

import (
	"fmt"
	"strings"

	"golang.org/x/xerrors"
)

// ErrCode : エラーコードの型
//go:generate enumer -type=ErrCode -json
type ErrCode int

// エラーコード
const (
	ErrSuccess ErrCode = iota
	ErrNotFoundHosp
	ErrNotFoundProvider
	ErrNotFoundForum
	ErrNotFoundForumMessage
	ErrNotFoundForumOrParentMessage
	ErrNotFoundForumOrUser
	ErrNotFoundUser
	ErrNotFoundH13n
	ErrNotFound404
	ErrNotAuthAccount
	ErrNotAuthPassword
	ErrNotAuthLocked
	ErrNotAuthCredential
	ErrNotAuthAPIToken
	ErrNotPermit
	ErrInvalidParams
	ErrValidator
	ErrAlreadyExistUser
	ErrAlreadyExistForum
	ErrAlreadyExistForumMsg
	ErrAlreadyExistFloor
	ErrAlreadyExistSickBed
	ErrOverSessions
	ErrFatal
)

// Array : 複数エラー管理用構造体
type Array struct {
	Errs []error
}

// Error : エラーメッセージを取得する。
func (ary Array) Error() string {
	var sb strings.Builder
	for _, err := range ary.Errs {
		sb.WriteString(err.Error())
		sb.WriteString("\n")
	}
	return sb.String()
}

type errorex struct {
	err   error
	msg   string
	code  ErrCode
	args  []interface{}
	frame xerrors.Frame
}

func (e *errorex) Error() string {
	return e.msg
}

func (e *errorex) Unwrap() error {
	return e.err
}

func (e *errorex) Format(s fmt.State, v rune) {
	xerrors.FormatError(e, s, v)
}

func (e *errorex) FormatError(p xerrors.Printer) error {
	p.Print(e.msg)
	e.frame.Format(p)
	return e.err
}

// New : 独自エラーを生成する。
func (ec ErrCode) New(msg string, args ...interface{}) error {
	return &errorex{
		err:   nil,
		msg:   ec.String() + ": " + msg,
		code:  ec,
		args:  args,
		frame: xerrors.Caller(1),
	}
}

// Errorf : 独自エラーを生成する。
func (ec ErrCode) Errorf(format string, a ...interface{}) error {
	return &errorex{
		err:   nil,
		msg:   ec.String() + ": " + fmt.Sprintf(format, a...),
		code:  ec,
		args:  nil,
		frame: xerrors.Caller(1),
	}
}

// Wrap : 独自エラーを生成する。
func (ec ErrCode) Wrap(baseErr error, msg string, args ...interface{}) error {
	return &errorex{
		err:   baseErr,
		msg:   ec.String() + ": " + msg,
		code:  ec,
		args:  args,
		frame: xerrors.Caller(1),
	}
}

// Wrapf : 独自エラーを生成する。
func (ec ErrCode) Wrapf(baseErr error, format string, a ...interface{}) error {
	return &errorex{
		err:   baseErr,
		msg:   ec.String() + ": " + fmt.Sprintf(format, a...),
		code:  ec,
		args:  nil,
		frame: xerrors.Caller(1),
	}
}

// Info : errorからエラーコードなどの情報を取得する。
//  ec : エラーコード
//  ecRes : HTTPレスポンスに設定するエラーコード
//  errParams : エラーパラメーター
func Info(err error) (ec, ecRes ErrCode, errParams []interface{}) {
	errex, ok := err.(*errorex)
	if !ok {
		errex = ErrFatal.Wrap(err, "").(*errorex)
	}

	ec = errex.code
	errParams = errex.args
	ecStr := ec.String()
	switch {
	case ec == ErrNotFoundHosp || ec == ErrNotFoundProvider:
		//case strings.Index(ecStr, "ErrNotFound") == 0:
		ecRes = ErrNotFound404
	case strings.Index(ecStr, "ErrNotAuth") == 0:
		ecRes = ErrNotFound404
	default:
		ecRes = ec
	}
	return
}
