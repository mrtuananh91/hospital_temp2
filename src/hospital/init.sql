--\ENCODING UTF8;

DROP DATABASE db_0;
CREATE DATABASE db_0 ENCODING='UTF8' LC_COLLATE='C' LC_CTYPE='C';

\c db_0;
CREATE TABLE tbl_config (
	id BIGSERIAL PRIMARY KEY,
	config_json TEXT NOT NULL, -- 設定ファイル
	create_sql TEXT NOT NULL   -- 病院構築時のSQL文
);

CREATE TABLE tbl_provider (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(30) UNIQUE NOT NULL,
	outline VARCHAR(128),
	api_key BYTEA -- raw
);

INSERT INTO tbl_provider (name,outline) VALUES
	('PKI','PKIプロバイダー'),
	('App','Appプロバイダー');

CREATE TABLE tbl_provider_hospital (
	prov_id BIGINT NOT NULL REFERENCES tbl_provider(id) ON DELETE CASCADE ON UPDATE CASCADE,
	hosp_id BIGINT NOT NULL,
	PRIMARY KEY(prov_id, hosp_id)
);

INSERT INTO tbl_config (config_json, create_sql) VALUES (
	'
	{
		"server": {
			"no": 1,
			"_listen": "127.0.0.1",
			"listen": "/var/run/movacal-hospital.sock",
			"port": 0,
			"fileMode": 777,
			"maxConn": 128,
			"domain": {
				"main": "hospital.movacal.net",
				"app": "app.hospital.movacal.net",
				"link": "link.hospital.movacal.net"
			},
			"log": {
				"level": "warning",
				"omitDate": true,
				"access": false
			},
			"time": {
				"location": "Asia/Tokyo"
			},
			"cookie": {
				"path": "/",
				"secure": true
			}
		},
		"db": {
			"curTime": "(CURRENT_TIMESTAMP AT TIME ZONE ''Asia/Tokyo'')",
			"hospitals": [
				{
					"source": "host=127.0.0.1 port=5432 dbname=db_%d user=postgres password=nelt0001 sslmode=disable",
					"minID": 80001,
					"maxID": 81000
				},
				{
					"source": "host=127.0.0.1 port=5432 dbname=db_%d user=postgres password=nelt0001 sslmode=disable",
					"minID": 0,
					"maxID": 0
				},
				{
					"source": "host=127.0.0.1 port=5432 user=postgres password=nelt0001 sslmode=disable",
					"minID": -1,
					"maxID": -1
				}
			]
		},
		"redis": {
			"host": "127.0.0.1",
			"port": 6379,
			"maxIdle": 100,
			"maxActive": 0,
			"idleTimeout": 240,
			"wait": false,
			"expire": 7200,
			"expireAPI": 10
		},
		"hosp": {
			"user": {
				"maxFailCount": 10,
				"maxAddCount": 100,
				"maxSearchCount": 1000,
				"defSearchCount": 100
			},
			"patient": {
				"maxFailCount": 10,
				"maxAddCount": 100,
				"maxSearchCount": 1000,
				"defSearchCount": 100
			},
			"forum": {
				"maxSearchCount": 100,
				"defSearchCount": 5
			}
		}
	}
	',
	'
	-- ユーザーID         :  0 << 54 | hosp_id << 32 | no
	-- 患者ID             :  1 << 54 | hosp_id << 32 | no
	-- ファイルID         : 10 << 54 | hosp_id << 32 | no
	-- 掲示板ID           : 20 << 54 | hosp_id << 32 | no
	-- 掲示板メッセージID : 21 << 54 | hosp_id << 32 | no
	-- 入院ID             : 30 << 54 | hosp_id << 32 | no
	-- カルテID           : 40 << 54 | hosp_id << 32 | no
	-- 記録ID             : 41 << 54 | hosp_id << 32 | no
	-- フロアID           : 50 << 54 | hosp_id << 32 | no
	-- 病室ID             : 51 << 54 | hosp_id << 32 | no
	-- ベッドID           : 52 << 54 | hosp_id << 32 | no
	-- ベッドアサインID   : 53 << 54 | hosp_id << 32 | no

	CREATE OR REPLACE FUNCTION hosp_id()
	RETURNS INTEGER AS $$
		SELECT %d;
	$$ LANGUAGE sql;

	CREATE OR REPLACE FUNCTION timezone()
	RETURNS CHAR AS $$
		SELECT ''Asia/Tokyo'';
	$$ LANGUAGE sql;

	-- 病院情報
	CREATE TABLE tbl_hospital (
		hospital_id INT PRIMARY KEY,
		name VARCHAR(100) NOT NULL,
		zip_code VARCHAR(10) NOT NULL,
		address VARCHAR(128) NOT NULL,
		tel VARCHAR(16) NOT NULL,
		fax VARCHAR(16) NOT NULL,
		enabled BOOLEAN DEFAULT TRUE NOT NULL
	);

	-- 連携プロバイダー
	CREATE TABLE tbl_provider (
		provider_id INT PRIMARY KEY
	);

	-- ユーザーカテゴリ
	CREATE TABLE tbl_category (
		category_id SERIAL PRIMARY KEY,
		name VARCHAR(32) UNIQUE NOT NULL,
		order_no INT NOT NULL
	);

	INSERT INTO tbl_category (name, order_no) VALUES
		(''医師'',2000),
		(''看護師'',1990),
		(''事務'',1980),
		(''PT/OT/ST'',1970),
		(''相談員'',1970),
		(''検査会社用'',1960),
		(''看護助手'',1950),
		(''栄養士'',1940),
		(''薬剤師'',1930),
		(''検査技師'',1920),
		(''ヘルパー'',1910);

	-- ロール
	CREATE TABLE tbl_role (
		role_id SERIAL PRIMARY KEY,
		name VARCHAR(32) UNIQUE NOT NULL,
		order_no INT NOT NULL,
		permit_user INT NOT NULL,
		permit_partner_login INT NOT NULL
	);

	INSERT INTO tbl_role (name, order_no, permit_user, permit_partner_login) VALUES
		(''一般'', 2000, 0, 0),
		(''管理者'', 1990, 3, 3);

	-- ユーザーグループ
	CREATE TABLE tbl_usergroup (
		usergroup_id SERIAL PRIMARY KEY,
		name VARCHAR(32) UNIQUE NOT NULL,
		order_no INT NOT NULL
	);

	-- ユーザー
	CREATE TABLE tbl_user (
		user_id BIGSERIAL PRIMARY KEY, -- ユーザーID
		account VARCHAR(16) UNIQUE NOT NULL, -- アカウント名
		order_no INT DEFAULT 0 NOT NULL,
		category_id INT NOT NULL REFERENCES tbl_category(category_id),
		role_id INT NOT NULL REFERENCES tbl_role(role_id),
		orca_id VARCHAR(10),
		orca_id_outer1 VARCHAR(10),
		family_name VARCHAR(30) NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		family_name_kana VARCHAR(30) NOT NULL,
		first_name_kana VARCHAR(30) NOT NULL,
		gender INT NOT NULL,
		password BYTEA NOT NULL,
		password_changed TIMESTAMP DEFAULT ''1999-12-31'' NOT NULL,
		fail_count INT DEFAULT 0 NOT NULL,
		modified TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()) NOT NULL,
		enabled BOOLEAN DEFAULT TRUE NOT NULL
	);
	SELECT SETVAL(''tbl_user_user_id_seq'', (0::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	INSERT INTO tbl_user (user_id,account,category_id,role_id,family_name,first_name,family_name_kana,first_name_kana,gender,password) VALUES
		(-1,'''',1,1,'''','''','''','''',0,'''');

	CREATE TABLE tbl_usergroup_member (
		usergroup_id INT NOT NULL REFERENCES tbl_usergroup(usergroup_id) ON DELETE CASCADE,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE,
		PRIMARY KEY(usergroup_id, user_id)
	);

	-- 患者
	CREATE TABLE tbl_patient (
		patient_id BIGSERIAL PRIMARY KEY, -- 患者ID
		orca_id VARCHAR(20), -- ORCA ID
		chkcode VARCHAR(64), --
		family_name VARCHAR(30) NOT NULL, -- 苗字
		first_name VARCHAR(30) NOT NULL, -- 名前
		family_name_kana VARCHAR(30) NOT NULL, -- 苗字（カナ）
		first_name_kana VARCHAR(30) NOT NULL, -- 名前（カナ）
		gender INT NOT NULL, -- 性別（1:男性、2:女性）
		birthday DATE NOT NULL, -- 生年月日
		blood_type VARCHAR(8) NOT NULL, -- 血液型
		postal_code VARCHAR(10), -- 郵便番号
		country VARCHAR(16), -- 国籍
		address VARCHAR(200), -- 住所
		tel VARCHAR(16), -- 電話番号
		mobilephone VARCHAR(16), -- 携帯番号
		emg_name1 VARCHAR(32), -- 緊急連絡先 氏名1
		emg_name2 VARCHAR(32), -- 緊急連絡先 氏名2
		emg_name3 VARCHAR(32), -- 緊急連絡先 氏名3
		emg_tel1 VARCHAR(16), -- 緊急連絡先 電話番号1
		emg_tel2 VARCHAR(16), -- 緊急連絡先 電話番号2
		emg_tel3 VARCHAR(16), -- 緊急連絡先 電話番号3
		emg_mobilephone1 VARCHAR(16), -- 緊急連絡先 携帯番号1
		emg_mobilephone2 VARCHAR(16), -- 緊急連絡先 携帯番号2
		emg_mobilephone3 VARCHAR(16), -- 緊急連絡先 携帯番号3
		emg_rel1 VARCHAR(32), -- 緊急連絡先 本人との関係1
		emg_rel2 VARCHAR(32), -- 緊急連絡先 本人との関係2
		emg_rel3 VARCHAR(32), -- 緊急連絡先 本人との関係3
		key_person_name VARCHAR(32), -- キーパーソン 氏名
		key_person_rel VARCHAR(32), -- キーパーソン 本人との関係
		family_info TEXT, -- 家族情報
		disease_history TEXT, -- 病歴
		family_disease_history TEXT, -- 家族歴
		note TEXT, -- 備考
		dead_datetime TIMESTAMP, -- 死亡日時
		dead_type SMALLINT, -- 死亡タイプ
		dead_comment VARCHAR(128), -- 死亡コメント
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_patient_patient_id_seq'', (1::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);
	CREATE INDEX ON tbl_patient(orca_id);

	-- フロア
	CREATE TABLE tbl_floor (
		floor_id BIGSERIAL PRIMARY KEY,
		floor_num INT NOT NULL,
		max_room_num INT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_floor_floor_id_seq'', (50::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	-- 病室
	CREATE TABLE tbl_room (
		room_id BIGSERIAL PRIMARY KEY,
		floor_id BIGINT NOT NULL REFERENCES tbl_floor(floor_id) ON DELETE CASCADE, -- フロアID
		room_number VARCHAR(16) UNIQUE NOT NULL,
		max_bed_num INT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_room_room_id_seq'', (51::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE TABLE tbl_bed (
		bed_id BIGSERIAL PRIMARY KEY,
		floor_id BIGINT NOT NULL REFERENCES tbl_floor(floor_id) ON DELETE CASCADE, -- フロアID
		room_id BIGINT NOT NULL REFERENCES tbl_room(room_id) ON DELETE CASCADE, -- 病室ID
		bed_number VARCHAR(16) UNIQUE NOT NULL,
		bed_type INT
	);
	SELECT SETVAL(''tbl_bed_bed_id_seq'', (52::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE TABLE tbl_bed_assign (
		bed_assign_id BIGSERIAL PRIMARY KEY,
		bed_id BIGINT NOT NULL REFERENCES tbl_bed(bed_id) ON DELETE CASCADE, -- ベッドID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		start_date DATE NOT NULL,
		start_period INT NOT NULL,
		end_date DATE NOT NULL,
		end_period INT NOT NULL,
		delete_flag BOOLEAN NOT NULL DEFAULT false,
		upd_username VARCHAR(32) NOT NULL,
		upd_userid BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_bed_assign_bed_assign_id_seq'', (53::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE VIEW view_floor AS SELECT
		tbl_floor.floor_id,
		tbl_floor.floor_num,
		tbl_floor.max_room_num,
		tbl_floor.upd_datetime AS floor_upd_datetime,
		tbl_room.room_id,
		tbl_room.room_number,
		tbl_room.max_bed_num,
		tbl_room.upd_datetime AS room_upd_datetime,
		tbl_bed.bed_id,
		tbl_bed.bed_number,
		tbl_bed.bed_type,
		tbl_bed_assign.patient_id,
		tbl_patient.family_name,
		tbl_patient.first_name,
		tbl_bed_assign.start_date,
		tbl_bed_assign.start_period,
		tbl_bed_assign.end_date,
		tbl_bed_assign.end_period,
		tbl_bed_assign.upd_username,
		tbl_bed_assign.upd_userid,
		tbl_bed_assign.upd_datetime AS bed_assign_upd_datetime,
		tbl_bed_assign.reg_datetime AS bed_assign_reg_datetime
	FROM tbl_floor
	INNER JOIN tbl_room ON tbl_floor.floor_id=tbl_room.floor_id
	INNER JOIN tbl_bed ON tbl_room.room_id=tbl_bed.room_id
	INNER JOIN tbl_bed_assign ON tbl_bed.bed_id=tbl_bed_assign.bed_id
	INNER JOIN tbl_patient ON tbl_bed_assign.patient_id=tbl_bed_assign.patient_id;

	-- 入院テーブル
	CREATE TABLE tbl_h13n (
		h13n_id BIGSERIAL PRIMARY KEY, -- 入院ID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		start_datetime TIMESTAMP, -- 入院日
		end_datetime TIMESTAMP, -- 退院日
		end_tbc BOOLEAN, -- 退院日が予定の場合はtrue
		hosp_start_age INT, -- 入院時年齢
		hosp_end_age INT, -- 退院時年齢
		hosp_way INT, -- 入院方法（1:救急搬送、2:来院）
		hosp_way_det VARCHAR(128), -- 入院経緯
		hosp_facility INT, -- 施設（1:施設、2:自宅）
		hosp_facility_det VARCHAR(128), -- 施設詳細
		hosp_condition VARCHAR(512),
		life_independence INT,
		start_bedsore VARCHAR(16),
		start_bedsore_comment VARCHAR(128),
		end_bedsore VARCHAR(16),
		end_bedsore_comment VARCHAR(128),
		hosp_bt FLOAT,
		hosp_bph INT,
		hosp_bpl INT,
		hosp_spo2 FLOAT,
		vital_comment VARCHAR(128),
		note VARCHAR(512)
	);
	SELECT SETVAL(''tbl_h13n_h13n_id_seq'', (30::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);
	CREATE INDEX ON tbl_h13n(start_datetime);

	-- 患者の担当者
	CREATE TABLE tbl_patient_charge (
		patient_charge_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		h13n_id BIGINT NOT NULL REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		charge_type INT, -- 1:医師、2:看護師
		user_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		start_date DATE,
		end_date DATE
	);

	CREATE TABLE tbl_order (
		order_id BIGSERIAL PRIMARY KEY,
		h13n_id BIGINT REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		status INT NOT NULL,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		user_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		user_name VARCHAR(32),
		emg_flag INT,
		complete_num INT,
		comment TEXT,
		order_type INT NOT NULL,
		start_date DATE,
		start_date_period INT,
		end_date DATE,
		end_date_period INT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);

	CREATE TABLE tbl_order_history (
		order_history_id BIGSERIAL PRIMARY KEY,
		h13n_id BIGINT REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		order_id BIGINT REFERENCES tbl_order(order_id) ON DELETE CASCADE, -- オーダーID
		status INT NOT NULL,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		user_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		user_name VARCHAR(32),
		emg_flag INT,
		complete_num INT,
		comment TEXT,
		order_type INT NOT NULL,
		start_date DATE,
		start_date_period INT,
		end_date DATE,
		end_date_period INT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);

	CREATE TABLE tbl_order_med (
		order_med_id BIGSERIAL PRIMARY KEY,
		order_id BIGINT REFERENCES tbl_order(order_id) ON DELETE CASCADE, -- オーダーID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		order_type INT,
		date_period INT,
		medicine_code VARCHAR(30),
		medicine_name VARCHAR(30),
		usage_code VARCHAR(30),
		unit VARCHAR(10),
		comment VARCHAR(128)
	);

	CREATE TABLE tbl_order_injection (
		order_injection_id BIGSERIAL PRIMARY KEY,
		order_id BIGINT REFERENCES tbl_order(order_id) ON DELETE CASCADE, -- オーダーID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		order_type INT,
		date_period INT,
		injection_med_code VARCHAR(30),
		injection_med_jancode VARCHAR(30),
		injection_med_name VARCHAR(30),
		injection_med_quantum VARCHAR(30),
		injection_speed VARCHAR(30),
		comment VARCHAR(128)
	);

	CREATE TABLE tbl_order_meal (
		order_meal_id BIGSERIAL PRIMARY KEY,
		order_id BIGINT REFERENCES tbl_order(order_id) ON DELETE CASCADE, -- オーダーID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		order_type INT,
		date_period INT,
		meal_order_code INT,
		comment VARCHAR(128)
	);

	CREATE TABLE tbl_order_act (
		order_act_id BIGSERIAL PRIMARY KEY,
		order_id BIGINT REFERENCES tbl_order(order_id) ON DELETE CASCADE, -- オーダーID
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		order_type INT,
		date_period INT,
		order_date DATE,
		order_date_period INT,
		act_status1 BOOLEAN,
		act_status2 BOOLEAN,
		act_status3 BOOLEAN,
		act_status4 BOOLEAN,
		act_status5 BOOLEAN,
		act_status6 BOOLEAN,
		act_status7 BOOLEAN,
		act_status8 BOOLEAN,
		act_status9 BOOLEAN,
		act_status10 BOOLEAN,
		act_user1 VARCHAR(32),
		act_user2 VARCHAR(32),
		act_user3 VARCHAR(32),
		act_user4 VARCHAR(32),
		act_user5 VARCHAR(32),
		act_user6 VARCHAR(32),
		act_user7 VARCHAR(32),
		act_user8 VARCHAR(32),
		act_user9 VARCHAR(32),
		act_user10 VARCHAR(32),
		act_datetime1 TIMESTAMP,
		act_datetime2 TIMESTAMP,
		act_datetime3 TIMESTAMP,
		act_datetime4 TIMESTAMP,
		act_datetime5 TIMESTAMP,
		act_datetime6 TIMESTAMP,
		act_datetime7 TIMESTAMP,
		act_datetime8 TIMESTAMP,
		act_datetime9 TIMESTAMP,
		act_datetime10 TIMESTAMP,
		comment VARCHAR(128)
	);

	CREATE TABLE tbl_diagdept (
		diagdept_id SERIAL PRIMARY KEY,
		diagdept_code VARCHAR(2) NOT NULL,
		diagdept_name VARCHAR(64) NOT NULL,
		order_no INT NOT NULL,
		nouse_flag BOOLEAN DEFAULT false NOT NULL
	);

	CREATE TABLE tbl_diagtype (
		diagtype_id SERIAL PRIMARY KEY,
		diagtype_code INT NOT NULL,
		diagtype_name VARCHAR(64) NOT NULL,
		order_no INT NOT NULL,
		nouse_flag BOOLEAN DEFAULT false NOT NULL
	);

	CREATE TABLE tbl_karte (
		karte_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		h13n_id BIGINT REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		patient_karte_id BIGINT,
		org_karte_id BIGINT,
		chkcode VARCHAR(128),
		karte_title VARCHAR(64),
		diagdept_id INT REFERENCES tbl_diagdept(diagdept_id),
		diagtype_id INT REFERENCES tbl_diagtype(diagtype_id),
		karte_body TEXT,
		karte_memo TEXT,
		doctor_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		doctor_name VARCHAR(64),
		input_user_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		input_user_name VARCHAR(64),
		substitute_input BOOLEAN,
		act_start_datetime TIMESTAMP,
		act_end_datetime TIMESTAMP,
		next_datetime TIMESTAMP,
		status INT,
		karte_type INT,
		orca_send INT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_karte_karte_id_seq'', (40::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE TABLE tbl_karte_history (
		karte_history_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		h13n_id BIGINT REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		karte_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- カルテID
		chkcode VARCHAR(128),
		karte_title VARCHAR(64),
		diagdept_id INT REFERENCES tbl_diagdept(diagdept_id),
		diagtype_id INT REFERENCES tbl_diagtype(diagtype_id),
		karte_body TEXT,
		karte_memo TEXT,
		doctor_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		doctor_name VARCHAR(64),
		input_user_id BIGINT REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		input_user_name VARCHAR(64),
		substitute_input BOOLEAN,
		act_start_datetime TIMESTAMP,
		act_end_datetime TIMESTAMP,
		next_datetime TIMESTAMP,
		status INT,
		karte_type INT,
		orca_send INT,
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);

	CREATE TABLE tbl_karte_file (
		karte_file_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		karte_id BIGINT NOT NULL REFERENCES tbl_karte(karte_id) ON DELETE CASCADE, -- カルテID
		file_name VARCHAR(32),
		org_filename VARCHAR(32)
	);

	CREATE TABLE tbl_record (
		record_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		h13n_id BIGINT NOT NULL REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		status INT NOT NULL,
		record_type INT NOT NULL,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		user_name VARCHAR(32),
		act_datetime TIMESTAMP,
		act_detail TEXT,
		note TEXT,
		upd_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()),
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);
	SELECT SETVAL(''tbl_record_record_id_seq'', (41::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE TABLE tbl_record_history (
		record_history_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		patient_name VARCHAR(64),
		h13n_id BIGINT NOT NULL REFERENCES tbl_h13n(h13n_id) ON DELETE CASCADE, -- 入院ID
		record_id BIGINT NOT NULL REFERENCES tbl_record(record_id) ON DELETE CASCADE, -- レコードID
		status INT NOT NULL,
		record_type INT NOT NULL,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE, -- ユーザーID
		user_name VARCHAR(32),
		act_datetime TIMESTAMP,
		act_detail TEXT,
		note TEXT,
		reg_datetime TIMESTAMP NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone())
	);

	CREATE TABLE tbl_record_file (
		record_file_id BIGSERIAL PRIMARY KEY,
		patient_id BIGINT NOT NULL REFERENCES tbl_patient(patient_id) ON DELETE CASCADE, -- 患者ID
		record_id BIGINT NOT NULL REFERENCES tbl_record(record_id) ON DELETE CASCADE, -- レコードID
		file_name VARCHAR(32),
		org_filename VARCHAR(32)
	);

	CREATE VIEW view_record AS
	SELECT
		karte_id,
		patient_id,
		h13n_id,
		0 AS record_type,
		karte_title,
		tbl_diagdept.diagdept_id,
		tbl_diagdept.diagdept_name,
		tbl_diagtype.diagtype_id,
		tbl_diagtype.diagtype_name,
		karte_body,
		karte_memo,
		doctor_id,
		doctor_name,
		input_user_id,
		input_user_name,
		substitute_input,
		act_start_datetime,
		act_end_datetime,
		next_datetime,
		status,
		karte_type,
		orca_send,
		upd_datetime,
		reg_datetime
	FROM tbl_karte
	INNER JOIN tbl_diagdept ON tbl_karte.diagdept_id=tbl_diagdept.diagdept_id
	INNER JOIN tbl_diagtype ON tbl_karte.diagtype_id=tbl_diagtype.diagtype_id
	UNION ALL
	SELECT
		record_id,
		patient_id,
		h13n_id,
		record_type,
		'''' AS karte_title,
		0 AS diagdept_id,
		'''' AS diagdept_name,
		0 AS diagtype_id,
		'''' AS diagtype_name,
		act_detail,
		note,
		0 AS doctor_id,
		'''' AS doctor_name,
		user_id AS input_user_id,
		user_name input_user_name,
		false AS substitute_input,
		act_datetime AS act_start_datetime,
		null AS act_end_datetime,
		null AS next_datetime,
		0 AS status,
		0 AS karte_type,
		0 AS orca_send,
		null AS upd_datetime,
		reg_datetime
	FROM tbl_record;

	-- 掲示板一覧
	CREATE TABLE tbl_forum (
		forum_id BIGSERIAL PRIMARY KEY,
		name VARCHAR(32) UNIQUE NOT NULL,
		order_no INT DEFAULT 0 NOT NULL,
		created TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()) NOT NULL,
		enabled BOOLEAN DEFAULT TRUE NOT NULL
	);
	SELECT SETVAL(''tbl_forum_forum_id_seq'', (20::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	INSERT INTO tbl_forum (forum_id, name, order_no) VALUES (-1, '''', 2000);
	INSERT INTO tbl_forum (name, order_no) VALUES (''連絡事項'', 1990);

	-- 掲示板ACL
	CREATE TABLE tbl_forum_acl (
		forum_id BIGINT NOT NULL REFERENCES tbl_forum(forum_id) ON DELETE CASCADE,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE,
		PRIMARY KEY(forum_id, user_id)
	);

	-- 掲示板のメッセージ一覧
	CREATE TABLE tbl_forum_msg (
		forum_msg_id BIGSERIAL PRIMARY KEY,
		forum_id BIGINT NOT NULL REFERENCES tbl_forum(forum_id) ON DELETE CASCADE,
		parent_id BIGINT NOT NULL,
		user_id BIGINT NOT NULL, -- REFERENCES tbl_user(user_id) ON DELETE CASCADE（ユーザー削除後も残す）
		family_name VARCHAR(30) NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		content text NOT NULL,
		--file_name_1 VARCHAR(128),
		--file_id_1 BIGINT REFERENCES tbl_file(file_id),
		created TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()) NOT NULL
	);
	SELECT SETVAL(''tbl_forum_msg_forum_msg_id_seq'', (21::BIGINT<<54)::BIGINT|(hosp_id()::BIGINT<<32)::BIGINT|1, FALSE);

	CREATE TABLE tbl_forum_msg_del (
		forum_msg_del_id BIGINT PRIMARY KEY,
		forum_id BIGINT NOT NULL,
		parent_id BIGINT NOT NULL,
		user_id BIGINT NOT NULL,
		family_name VARCHAR(30) NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		content text NOT NULL,
		--file_name_1 VARCHAR(128),
		--file_id_1 BIGINT,
		created TIMESTAMP NOT NULL,
		deleted TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()) NOT NULL
	);

	-- 掲示板のメッセージ既読管理
	CREATE TABLE tbl_forum_read (
		forum_id BIGINT NOT NULL REFERENCES tbl_forum(forum_id) ON DELETE CASCADE,
		msg_id BIGINT NOT NULL REFERENCES tbl_forum_msg(forum_msg_id) ON DELETE CASCADE,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE,
		PRIMARY KEY(msg_id, user_id)
	);

	-- 掲示板のメッセージ確認管理
	CREATE TABLE tbl_forum_confirm (
		forum_msg_id BIGINT NOT NULL REFERENCES tbl_forum_msg(forum_msg_id) ON DELETE CASCADE,
		user_id BIGINT NOT NULL REFERENCES tbl_user(user_id) ON DELETE CASCADE,
		confirmed TIMESTAMP,
		PRIMARY KEY(forum_msg_id, user_id)
	);

	-- イベントログ
	CREATE TABLE tbl_eventlog (
		eventlog_id BIGSERIAL PRIMARY KEY,
		event_id INT NOT NULL,
		ip_addr VARCHAR(64),
		account VARCHAR(16),
		user_agent VARCHAR(512),
		error_code VARCHAR(32),
		info TEXT,
		created TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE timezone()) NOT NULL
	);
	'
);
