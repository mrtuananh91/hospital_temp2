package prov

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"fmt"
	"hospital/db"
	"hospital/errors"
	"hospital/session"
	"hospital/util"
)

func encryptID(id int64) string {
	return util.EncryptID(id)
}

func decryptID(encID string) int64 {
	return util.DecryptID(encID)
}

// Prov : サービス接続者情報
type Prov struct {
	ID      int64         `json:"-"`
	EncID   string        `json:"id"`
	Name    string        `json:"name"`
	Outline db.NullString `json:"outline"`
	APIKey  string        `json:"key"`
	apiKey  []byte
	errMsg  string
}

func newProv(id int64, name string) (p *Prov, err error) {
	p = &Prov{}
	if id == 0 {
		switch name {
		case "PKI":
			p.ID = 1
		case "App":
			p.ID = 2
		default:
			err = errors.ErrNotFoundProvider.New("name:" + name)
			return
		}
		p.Name = name
	} else {
		var con *sql.DB
		if con, err = db.New(db.CommonID); err != nil {
			return
		}

		q := fmt.Sprintf("SELECT id,name,outline,api_key FROM tbl_provider WHERE id=%d", id)
		if err = con.QueryRow(q).Scan(&p.ID, &p.Name, &p.Outline, &p.apiKey); err != nil {
			err = errors.ErrNotFoundProvider.Wrapf(err, "provID:%d, name:%s", id, name)
			return
		}
		p.APIKey = base64.StdEncoding.EncodeToString(p.apiKey)
	}

	p.EncID = encryptID(p.ID)
	p.errMsg = fmt.Sprintf("p.ID:%d, p.EncID:%s", p.ID, p.EncID)
	return
}

// NewPKI : PKIサービス接続者情報を生成する。
func NewPKI() *Prov {
	p, _ := newProv(0, "PKI")
	return p
}

// NewApp : Appサービス接続者情報を生成する。
func NewApp() *Prov {
	p, _ := newProv(0, "App")
	return p
}

// NewAPI : API連携用サービス接続者情報を生成する。
func NewAPI(encID string, random, signature []byte) (credential string, p *Prov, err error) {
	p, err = newProv(decryptID(encID), "")

	// 署名検証
	if p.apiKey == nil {
		err = errors.ErrNotAuthAPIToken.New("%s, p.apiKey:nil", p.errMsg)
		return
	}
	mac := hmac.New(sha256.New, p.apiKey)
	mac.Write(random)
	calMac := mac.Sum(nil)
	if !hmac.Equal(signature, calMac) {
		err = errors.ErrNotAuthAPIToken.New("%s, signature:bad", p.errMsg)
		return
	}

	// クレデンシャル情報を生成
	ss := session.New()
	defer ss.Close()
	credential, err = ss.Add(&session.Info{ProvID: p.ID}, false)
	return
}

// NewCredential : クレデンシャル情報からAPI連携用サービス接続者情報を生成する。
func NewCredential(credential string, keep bool) (p *Prov, err error) {
	ss := session.New()
	defer ss.Close()

	info, err := ss.Get(credential)
	if err != nil {
		return
	}
	if p, err = newProv(info.ProvID, ""); err != nil {
		return
	}

	if keep {
		err = ss.Update(credential, nil, false)
	} else {
		err = ss.Delete(credential)
	}
	return
}

// ErrMsg : エラーメッセージを取得する。
func (p *Prov) ErrMsg() string {
	return p.errMsg
}

// IsPKI : PKIプロバイダーの場合はtrueを返却する。
func (p *Prov) IsPKI() bool {
	return p.Name == "PKI"
}

// IsApp : Appプロバイダーの場合はtrueを返却する。
func (p *Prov) IsApp() bool {
	return p.Name == "App"
}

// IsAPI : APIプロバイダーの場合はtrueを返却する。
func (p *Prov) IsAPI() bool {
	return p.Name != "PKI" && p.Name != "App"
}
