package hosp_test

import (
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"hospital/hosp"
	hconf "hospital/hosp/conf"
	"hospital/hosp/forum"
	"hospital/hosp/patient"
	"hospital/hosp/user"
	"hospital/prov"
	"hospital/session"
	"log"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func init() {
	if err := godotenv.Load("../.env"); err != nil {
		log.Print("No .env file found")
	}

}
func TestMain(m *testing.M) {
	if err := conf.Load(); err != nil {
		panic(err)
	}
	session.Init()
	//if err := user.Init(); err != nil {
	//	panic(err)
	//}

	m.Run()

	os.Exit(0)
}
func TestAddUser(t *testing.T) {
	hospID := int64(80001)

	us := []*user.User{
		{
			Account:    "10005",
			Password:   "10005",
			Order:      100,
			CategoryID: 1,              // 医師
			RoleID:     user.RoleAdmin, // 管理者
		},
	}
	p := prov.NewPKI()
	h, err := hosp.New(p, hospID)
fmt.Println(h)
	if err == nil {
		um := h.UserMngr()
		um.Add(us)
	}

}
func TestOnepath(t *testing.T) {
	hospID := int64(80001)
	hosp.Delete(hospID)
	//t.Log(conf.Get().DB.CreateSQL)

	h := &hosp.Hosp{
		ID:      hospID,
		Name:    "NELT病院",
		ZipCode: "2210032",
		Address: "神奈川県横浜市神奈川区新浦島町1-1-32",
		Tel:     "045-000-0000",
		Fax:     "045-111-1111",
	}
	u1 := &user.User{
		Account:        "10001",
		Password:       "10001",
		FamilyName:     "テスト",
		FirstName:      "医者1",
		FamilyNameKana: "テスト",
		FirstNameKana:  "イシャ1",
		Order:          100,
		CategoryID:     1,              // 医師
		RoleID:         user.RoleAdmin, // 管理者
	}
	if err := hosp.Create(h, u1); err != nil {
		t.Fatalf("%+v\n", err)
	}

	p := prov.NewPKI()
	h, err := hosp.New(p, hospID)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if _, err = h.Auth(u1.Account, u1.Password+"1"); err == nil {
		t.Fatalf("User authentication error\n")
	}
	if ec, ecRes, _ := errors.Info(err); ec != errors.ErrNotAuthPassword || ecRes != errors.ErrNotFound404 {
		t.Fatalf("ec:%v, ecRes:%v, err:%+v\n", ec, ecRes, err)
	}
	credential, err := h.Auth(u1.Account, u1.Password)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	h2, err := hosp.NewCredential(p, credential)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if h.ID != h2.ID {
		t.Fatalf("h:%v, h2:%v\n", h, h2)
	}

	cm := h.ConfMngr()
	if err = cm.AddDiagDept(&hconf.DiagDept{Code: "01", Name: "内科", OrderNo: 0}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	if err = cm.AddDiagDept(&hconf.DiagDept{Code: "02", Name: "外科", OrderNo: 0}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	dds, err := cm.DiagDepts()
	for _, dd := range dds {
		t.Logf("DiagDept.Code:%s, DiagDept.Name:%s\n", dd.Code, dd.Name)
	}
	if err = cm.AddDiagType(&hconf.DiagType{Code: 1, Name: "定期診療", OrderNo: 0}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	if err = cm.AddDiagType(&hconf.DiagType{Code: 2, Name: "緊急診療", OrderNo: 0}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	dts, err := cm.DiagTypes()
	for _, dt := range dts {
		t.Logf("DiagType.Code:%d, DiagType.Name:%s\n", dt.Code, dt.Name)
	}

	fm := h.ForumMngr()
	if err = fm.Add(&forum.Forum{Name: "テスト"}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	fs, err := fm.Search(false)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	for _, f := range fs {
		t.Logf("forumID:%d, forumName:%s\n", f.ID, f.Name)
	}

	u2 := &user.User{
		Account:        "10002",
		Password:       "10002",
		FamilyName:     "テスト",
		FirstName:      "看護師1",
		FamilyNameKana: "テスト",
		FirstNameKana:  "カンゴシ1",
		Order:          100,
		CategoryID:     2,                // 看護師
		RoleID:         user.RoleGeneral, // 一般
	}
	u3 := &user.User{
		Account:        "10003",
		Password:       "10003",
		FamilyName:     "テスト",
		FirstName:      "リハ師1",
		FamilyNameKana: "テスト",
		FirstNameKana:  "リハシ1",
		Order:          100,
		CategoryID:     3,                // 事務
		RoleID:         user.RoleGeneral, // 一般
	}
	u4 := &user.User{
		Account:        "10004",
		Password:       "10004",
		FamilyName:     "テスト",
		FirstName:      "栄養師1",
		FamilyNameKana: "テスト",
		FirstNameKana:  "エイヨウシ1",
		Order:          100,
		CategoryID:     3,                // 事務
		RoleID:         user.RoleGeneral, // 一般
	}
	um := h.UserMngr()
	if err = um.Add([]*user.User{u2, u3, u4}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	us, err := um.Search(nil)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	limitForumAccount := fmt.Sprintf("%05d%s", h.ID, u2.Account)
	for _, u := range us {
		t.Logf("userID:%d, Account:%s\n", u.ID, u.Account)
		if u.Account == limitForumAccount {
			if err = u.UpdateForums([]int64{fs[0].ID}); err != nil {
				t.Fatalf("%+v\n", err)
			}
		}
		fs, err := u.Forums()
		if err != nil {
			t.Fatalf("%+v\n", err)
		}
		for _, f := range fs {
			t.Logf("%+v\n", f)
		}
	}

	ptm := h.PatientMngr()
	pt := &patient.Patient{
		OrcaID:         "1234567890",
		FamilyName:     "テスト",
		FirstName:      "太郎",
		FamilyNameKana: "テスト",
		FirstNameKana:  "タロウ",
		Gender:         1,
		BloodType:      "A",
	}
	pt.Birthday.Valid = true
	pt.Birthday.Time, err = time.Parse("2006-01-02", "1950-12-31")
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if err = ptm.Add([]*patient.Patient{pt}); err != nil {
		t.Fatalf("%+v\n", err)
	}
	pts, err := ptm.Search(&patient.Search{})
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	for _, pt := range pts {
		t.Logf("%+v\n", pt)
	}
	pt = pts[0]

	h13n := &patient.Hospitalization{
		EncID:        "-1",
		EncPatientID: pt.EncID,
	}
	h13n.StartDateTime.Valid = true
	h13n.StartDateTime.Time, err = time.Parse("2006-01-02 15:04", "2020-12-20 10:00")
	h13n.EndDateTime.Valid = true
	h13n.EndDateTime.Time, err = time.Parse("2006-01-02 15:04", "2020-12-25 12:00")
	if err = h.PatientMngr().AddHosp(h13n); err != nil {
		t.Fatalf("%+v\n", err)
	}
	h13n = &patient.Hospitalization{
		EncID:        "-1",
		EncPatientID: pt.EncID,
	}
	h13n.StartDateTime.Valid = true
	h13n.StartDateTime.Time, err = time.Parse("2006-01-02 15:04", "2021-01-20 10:00")
	if err = h.PatientMngr().AddHosp(h13n); err != nil {
		t.Fatalf("%+v\n", err)
	}

	/*f := h.Forum()
	fs, err := f.Search(true)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	for _, f := range fs {
		t.Logf("%d %s", f.ID, f.Name)
	}*/

	//if err = hosp.Delete(h.ID); err != nil {
	//	t.Fatalf("%+v\n", err)
	//}

	t.Logf("%+v\n", h)
}
