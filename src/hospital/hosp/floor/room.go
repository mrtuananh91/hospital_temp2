package floor

import (
	"hospital/db"
	"hospital/errors"
)

// Room : フロア情報
type Room struct {
	ID       int64       `json:"-"`
	EncID    string      `json:"id"`
	Name     string      `json:"name" validate:"required,max=32"`
	Order    int         `json:"order" validate:"omitempty,min=0,max=999"`
	Modified db.NullTime `json:"modified"`
}

// Create : 病室を作成する。
func (f *Floor) Create(name string, order int) (err error) {
	con, err := f.m.NewDB()
	if err != nil {
		return
	}

	if _, err = con.Exec("INSERT INTO tbl_room (name,order_no) VALUES ($1,$2)", name, order); err != nil {
		err = errors.ErrAlreadyExistFloor.Wrapf(err, "%s, name:%s", f.m.DebugMsg(), name)
		return
	}

	return
}
