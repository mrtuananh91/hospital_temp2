package floor

import (
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"time"

	"hospital/hosp/user"
	"hospital/util"
)

// Manager : フロア管理用構造体
type Manager struct {
	util.Manager
}

// New : フロア管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}

// Floor : フロア情報
type Floor struct {
	ID         int64   `json:"-"`
	EncID      string  `json:"floor_id"`
	FloorNum   int     `json:"floor_num" validate:"omitempty,min=-999,max=999"` // フロア（階数）
	MaxRoomNum int     `json:"max_room_num"`                                    // 最大部屋数
	Rooms      []*Room `json:"rooms"`
	m          *Manager
}

// UserAct : UserAct
type UserAct struct {
	ID         int64  `json:"-"`
	EncID      string `json:"id"`
	FirstName  string `json:"first_name"`
	FamilyName string `json:"family_name"`
	DateTime   string `json:"datetime"`
	Status     bool   `json:"status"`
}

// Create : フロアを作成する。
func (m *Manager) Create(floorNum int) (err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	if _, err = con.Exec("INSERT INTO tbl_floor (floor_num) VALUES ($1)", floorNum); err != nil {
		err = errors.ErrAlreadyExistFloor.Wrapf(err, "%x, floor_num:%x", m.DebugMsg(), floorNum)
		return
	}

	return
}

// Floors : フロア一覧を取得する。
func (m *Manager) Floors() (fs []*Floor, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	rows, err := con.Query("SELECT floor_id,floor_num,max_room_num  FROM tbl_floor ORDER BY floor_num ASC")
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	defer rows.Close()

	fs = make([]*Floor, 0)
	for rows.Next() {
		f := &Floor{}
		if err = rows.Scan(&f.ID, &f.FloorNum, &f.MaxRoomNum); err != nil {
			err = errors.ErrFatal.Wrap(err, m.DebugMsg())
			return
		}

		f.EncID = m.EncryptID(f.ID)
		f.m = m
		fs = append(fs, f)
	}

	return
}

// Floor : フロア一情報を取得する。
func (m *Manager) Floor(floorID int64) (f *Floor, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	rows, err := con.Query("SELECT floor_id,floor_num,max_room_num FROM tbl_floor WHERE floor_id=$1", floorID)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	defer rows.Close()

	f = &Floor{}
	if rows.Next() {
		if err = rows.Scan(&f.ID, &f.FloorNum, &f.MaxRoomNum); err != nil {
			err = errors.ErrFatal.Wrap(err, m.DebugMsg())
			return
		}

		f.EncID = m.EncryptID(f.ID)
		f.m = m
	} else {
		err = errors.ErrFatal.New("null data")
	}

	return
}

// CreateOrUpdateOrderActWithBarcode :
func (m *Manager) CreateOrUpdateOrderActWithBarcode(u *user.User, step int, floorID string, date string, orderType string, patientID string, orderID int, status bool) (user *UserAct, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	decPatientID := m.DecryptID(patientID)
	decFloorID := m.DecryptID(floorID)
	decOrderID := orderID

	time1, err := time.Parse("2006-01-02 15:04:05", date)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	dateStr := time1.Format("2006-01-02")

	sql := `SELECT * FROM tbl_order_injection toi
	INNER JOIN tbl_bed_assign tba ON tba.patient_id = toi.patient_id
	INNER JOIN tbl_bed tb ON tb.bed_id = tba.bed_id
	INNER JOIN tbl_room tr ON tr.room_id = tb.room_id
	INNER JOIN tbl_floor tf ON tf.floor_id = tb.floor_id	
	INNER JOIN tbl_order tor ON tor.order_id = toi.order_id
	WHERE 
		toi.patient_id = $1 
		AND tor.order_id = $2 
		AND tor.start_date <= '` + dateStr + `'::date AND tor.end_date >= '` + dateStr + `'::date 
		AND tf.floor_id = $3 LIMIT 1`

	rows, err := con.Query(sql, decPatientID, decOrderID, decFloorID)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	defer rows.Close()

	if rows.Next() {
		// テーブル更新
		sql := fmt.Sprintf(`SELECT order_act_id FROM tbl_order_act WHERE order_id=$1 AND patient_id=$2 AND order_type=$3 AND order_date=$4 LIMIT 1`)
		if rows, err1 := con.Query(sql, decOrderID, decPatientID, orderType, dateStr); err1 == nil {
			defer rows.Close()

			dt := time.Now()
			dateStrNow := dt.Format("2006-01-02 15:04")
			if rows.Next() {
				// 実績を更新
				sql := fmt.Sprintf(`UPDATE tbl_order_act SET act_user%d=$1, act_datetime%d=$2, act_status%d=$3 WHERE order_id=$4 AND patient_id=$5 AND order_date=$6`, step, step, step)
				if _, err = con.Exec(sql, fmt.Sprintf("%s%s", u.FirstName, u.FamilyName), dateStrNow, status, decOrderID, decPatientID, dateStr); err != nil {
					err = errors.ErrFatal.Wrap(err, m.DebugMsg())
					return
				}
			} else {
				// 実績を登録
				sql := fmt.Sprintf(`INSERT INTO tbl_order_act (order_id, order_date, patient_id, order_type, act_user%d, act_datetime%d, act_status%d) VALUES ($1, $2, $3, $4, $5, $6, $7)`, step, step, step)
				if _, err = con.Exec(sql, decOrderID, dateStr, decPatientID, orderType, fmt.Sprintf("%s%s", u.FirstName, u.FamilyName), dateStrNow, status); err != nil {
					err = errors.ErrFatal.Wrap(err, m.DebugMsg())
					return
				}
			}
			user = &UserAct{
				EncID:      m.EncryptID(u.ID),
				FirstName:  u.FirstName,
				FamilyName: u.FamilyName,
				DateTime:   dateStrNow,
			}
		} else {
			err = errors.ErrFatal.Wrap(err1, m.DebugMsg())
			return
		}
	}
	return
}

// RemoveCellOrderAct :
func (m *Manager) RemoveCellOrderAct(u *user.User, step int, floorID string, date string, orderType string, patientID string, orderID int) (passed bool, err error) {
	passed = false
	con, err := m.NewDB()
	if err != nil {
		return
	}

	decPatientID := m.DecryptID(patientID)
	decOrderID := orderID
	decFloorID := m.DecryptID(floorID)

	time1, err := time.Parse(time.RFC3339, date)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	dateStr := time1.Format("2006-01-02")

	sql := `SELECT * FROM tbl_order_injection toi
	INNER JOIN tbl_bed_assign tba ON tba.patient_id = toi.patient_id
	INNER JOIN tbl_bed tb ON tb.bed_id = tba.bed_id
	INNER JOIN tbl_room tr ON tr.room_id = tb.room_id
	INNER JOIN tbl_floor tf ON tf.floor_id = tb.floor_id	
	INNER JOIN tbl_order tor ON tor.order_id = toi.order_id
	WHERE 
		toi.patient_id = $1 
		AND toi.order_id = $2 
		AND tor.start_date <= '` + dateStr + `'::date AND tor.end_date >= '` + dateStr + `'::date 
		AND tf.floor_id = $3 LIMIT 1`

	rows, err := con.Query(sql, decPatientID, decOrderID, decFloorID)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	defer rows.Close()

	if rows.Next() {
		sql := fmt.Sprintf(`UPDATE tbl_order_act SET act_user%d=null, act_datetime%d=null, act_status%d=null WHERE order_id=$1 AND patient_id=$2 AND order_date=$3`, step, step, step)
		if _, err = con.Exec(sql, decOrderID, decPatientID, dateStr); err != nil {
			err = errors.ErrFatal.Wrap(err, m.DebugMsg())
			return
		}
		passed = true
	}
	return
}
