package forum

import (
	"fmt"
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	"path/filepath"
	"strconv"
	"strings"
)

// Message : メッセージ情報
type Message struct {
	id         int64
	ForumID    int           `json:"forumID,omitempty" validate:"required"`
	EncID      string        `json:"id" validate:"required,max=16,alphanum"`
	EncParID   *string       `json:"parID" validate:"omitempty,max=16,alphanum"`
	EncUserID  string        `json:"userID" validate:"required,max=16,alphanum"`
	FamilyName string        `json:"familyName"`
	FirstName  string        `json:"firstName"`
	Confirms   []*Confirm    `json:"confirms"`
	Content    string        `json:"content" validate:"required,max=4096"`
	Filename1  db.NullString `json:"filename1" validate:"omitempty,max=128"`
	Created    db.NullTime   `json:"created"`
	Childs     []*Message    `json:"childs"`
}

// Confirm : メッセージ確認
type Confirm struct {
	EncUserID  string      `json:"userID" validate:"required,max=16,alphanum"`
	FamilyName string      `json:"familyName"`
	FirstName  string      `json:"firstName"`
	Confirmed  db.NullTime `json:"confirmed"`
}

// SearchMessage : メッセージ検索パラメーター
type SearchMessage struct {
	ForumID int         `query:"forumID" validate:"required"`
	Offset  int         `query:"offset"`                              // オフセット値、子メッセージは対象外
	Limit   int         `query:"limit"`                               // 最大結果数（0以下の場合はデフォルト値、最大値以上の場合は最大値に強制変更）、子メッセージは対象外
	Keyword string      `query:"keyword" validate:"omitempty,max=32"` // 子メッセージは対象外
	Start   db.NullTime `query:"start"`                               // 子メッセージは対象外
	End     db.NullTime `query:"end"`                                 // 子メッセージは対象外
}

// SearchMessage : メッセージを検索する。
func (m *Manager) SearchMessage(srh *SearchMessage) (msgs []*Message, err error) {
	if srh.Limit <= 0 {
		srh.Limit = m.Conf().Forum.DefSearchCount
	}
	if srh.Limit > m.Conf().Forum.MaxSearchCount {
		srh.Limit = m.Conf().Forum.MaxSearchCount
	}

	con, err := m.NewDB()
	if err != nil {
		return
	}

	newError := func(ec errors.ErrCode, err error) error {
		return ec.Wrapf(err, "%s, searchMessage:%v", *srh)
	}

	// 親メッセージを検索
	q := fmt.Sprintf("select id,user_id,family_name,first_name,content,file_name_1,created from tbl_user where forum_id=%d and parent_id=0 ", srh.ForumID)
	args := make([]interface{}, 0)
	if srh.Keyword != "" {
		q += "and content like %$1% "
		args = append(args, srh.Keyword)
	}
	q += fmt.Sprintf("order by created desc,id desc offset %d limit %d", srh.Offset, srh.Limit)
	rows, err := con.Query(q, args)
	if err != nil {
		err = newError(errors.ErrFatal, err)
		return
	}
	defer rows.Close()

	var parIDs strings.Builder
	msgs = make([]*Message, 0)
	for rows.Next() {
		msg := &Message{}
		var userID int64
		if err = rows.Scan(&msg.id, &userID, &msg.FamilyName, &msg.FirstName, &msg.Content, &msg.Filename1, &msg.Created); err != nil {
			err = newError(errors.ErrFatal, err)
			return
		}
		parIDs.WriteString(strconv.FormatInt(msg.id, 10))
		parIDs.WriteString(",")
		msg.EncID = m.EncryptID(msg.id)
		msg.EncUserID = m.EncryptID(userID)
		msgs = append(msgs, msg)
	}

	// 子メッセージを検索
	q = fmt.Sprintf("select id,parent_id,user_id,family_name,first_name,content,file_name_1,created from tbl_user where parent_id in (%s) order by parent_id,created,id",
		strings.TrimSuffix(parIDs.String(), ","))
	if rows, err = con.Query(q); err != nil {
		err = newError(errors.ErrFatal, err)
		return
	}
	defer rows.Close()

	var parID, prevParID, userID int64
	var parMsg *Message
	for rows.Next() {
		chdMsg := &Message{}
		if err = rows.Scan(&chdMsg.id, &parID, &userID, &chdMsg.FamilyName, &chdMsg.FirstName, &chdMsg.Content, &chdMsg.Filename1, &chdMsg.Created); err != nil {
			err = newError(errors.ErrFatal, err)
			return
		}
		if parID != prevParID {
			for _, msg := range msgs {
				if msg.id == parID {
					parMsg = msg
					prevParID = parID
					break
				}
			}
		}
		chdMsg.EncID = m.EncryptID(chdMsg.id)
		chdMsg.EncParID = &parMsg.EncID
		chdMsg.EncUserID = m.EncryptID(userID)
		if parMsg.Childs == nil {
			parMsg.Childs = make([]*Message, 0)
		}
		parMsg.Childs = append(parMsg.Childs, chdMsg)
	}

	return
}

// AddMessage : メッセージを登録する。
func (m *Manager) AddMessage(msg *Message) (err error) {
	// 親メッセージIDの確認
	var parID interface{}
	if msg.EncParID == nil {
		parID = 0
	} else {
		parID = fmt.Sprintf("(select id from tbl_forum_msg where id=%d)", m.DecryptID(*msg.EncParID))
	}

	newError := func(ec errors.ErrCode, err error, errMsg string) error {
		if errMsg == "" {
			return ec.Wrapf(err, "%s, forumID:%d, parID:%v, userID:%d", msg.ForumID, parID, m.LoginUserID())
		}
		return ec.Wrapf(err, "%s, forumID:%d, parID:%v, userID:%d, %s", msg.ForumID, parID, m.LoginUserID(), errMsg)
	}

	// 添付ファイルが存在する場合、そのパス名からファイル名だけを取得
	if msg.Filename1.Valid {
		msg.Filename1.String = filepath.Base(msg.Filename1.String)
	}

	// メッセージを登録（フォーラムの存在＆ACL確認と親メッセージの存在確認）
	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	qf := fmt.Sprintf("(select forum_id from tbl_forum_acl where forum_id=%d and user_id=%d)", msg.ForumID, m.LoginUserID())
	q := fmt.Sprintf("insert into tbl_forum_msg (forum_id,parent_id,user_id,family_name,first_name,content,file_name_1,file_id_1,created) values "+
		"(%s,%v,%d,$1,$2,$3,$4,%s) returning (id)", qf, parID, m.LoginUserID(), conf.Get().DB.CurTime)
	var msgID int64
	if err = txm.Tx.QueryRow(q, m.LoginFamilyName(), m.LoginFirstName(), msg.Content, msg.Filename1).Scan(&msgID); err != nil {
		err = newError(errors.ErrNotFoundForumOrParentMessage, err, "")
		return
	}

	// 未読情報を登録
	q = fmt.Sprintf("insert into tbl_forum_read (forum_id,msg_id,user_id) "+
		"select %d as forum_id,%d as msg_id,user_id from tbl_forum_acl where forum_id=%d", msg.ForumID, msgID, msg.ForumID)
	if _, err = txm.Tx.Exec(q); err != nil {
		err = newError(errors.ErrNotFoundForumOrParentMessage, err, "")
		return
	}

	// 確認依頼を登録
	confirmsLen := len(msg.Confirms)
	if confirmsLen == 0 {
		txm.Commit()
		return
	}
	var sb, sb2 strings.Builder
	for _, confirm := range msg.Confirms {
		userID := m.DecryptID(confirm.EncUserID)
		sb.WriteString(strconv.FormatInt(userID, 10))
		sb.WriteString(",")
		sb2.WriteString(fmt.Sprintf("(%d,%d),", msgID, m.LoginUserID()))
	}
	qids := strings.TrimSuffix(sb.String(), ",")
	var count int
	if err = txm.Tx.QueryRow("select count(id) from tbl_user where id in (" + qids + ") and disabled=false").Scan(&count); err != nil || count != confirmsLen {
		err = newError(errors.ErrNotFoundUser, err, fmt.Sprintf("confirmsLen:%d, count:%d", confirmsLen, count))
		return
	}
	if _, err = txm.Tx.Exec("insert into tbl_forum_confirm (msg_id,user_id) values " + strings.TrimSuffix(sb2.String(), ",")); err != nil {
		err = newError(errors.ErrFatal, err, "")
		return
	}

	txm.Commit()

	return
}
