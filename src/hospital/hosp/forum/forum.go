package forum

import (
	"hospital/conf"
	"hospital/errors"
	"hospital/util"
)

// Manager : フォーラム管理用構造体
type Manager struct {
	util.Manager
}

// New : フォーラム管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}

// Forum : フォーラム情報構造体
type Forum struct {
	ID      int64  `json:"id"`
	Name    string `json:"name" validate:"required,max=32"`
	Order   int    `json:"order" validate:"omitempty,min=0,max=999"`
	Enabled bool   `json:"enabled"`
}

// Add : 掲示板を追加する。
func (m *Manager) Add(f *Forum) (err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	if _, err = con.Exec("INSERT INTO tbl_forum (name,order_no) VALUES ($1,$2)", f.Name, f.Order); err != nil {
		err = errors.ErrAlreadyExistForum.Wrapf(err, "%s, name:%s", m.DebugMsg(), f.Name)
		return
	}

	return
}

// Delete : 掲示板を削除する。メッセージが存在する場合（メッセージを削除して空であっても）、削除できない。
func (m *Manager) Delete(forumID int64) (err error) {
	if forumID <= 0 {
		err = errors.ErrAlreadyExistForumMsg.Errorf("%s, forumID:%d", forumID)
		return
	}

	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_forum_msg IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	var msgID int64
	if err = txm.Tx.QueryRow("SELECT id FROM tbl_forum_msg, tbl_forum_msg_del WHERE forum_id=$1 LIMIT 1", forumID).Scan(&msgID); err == nil {
		err = errors.ErrAlreadyExistForumMsg.Errorf("%s, forumID:%d", forumID)
		return
	}

	if _, err = txm.Tx.Exec("DELETE FROM tbl_forum WHERE id=$1", forumID); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, forumID:%d", forumID)
		return
	}

	txm.Commit()

	return
}

// Search : フォーラム情報一覧を取得する。
func (m *Manager) Search(alsoDisabled bool) (fs []*Forum, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := "SELECT forum_id,name,order_no,enabled FROM tbl_forum WHERE forum_id>=0 "
	if !alsoDisabled {
		q += "AND enabled=true "
	}
	q += "ORDER BY order_no DESC,forum_id"
	rows, err := con.Query(q)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, alsoDisabled:%v", m.DebugMsg(), alsoDisabled)
		return
	}
	fs = make([]*Forum, 0)
	for rows.Next() {
		f := &Forum{}
		if err = rows.Scan(&f.ID, &f.Name, &f.Order, &f.Enabled); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, alsoDisabled:%v", m.DebugMsg(), alsoDisabled)
			return
		}
		fs = append(fs, f)
	}

	return
}
