package patient

import (
	"hospital/db"
	"hospital/errors"
	"strings"
)

// Hospitalization :入退院履歴
type Hospitalization struct {
	m                   *Manager
	EncID               string      `json:"enc_id"`
	EncPatientID        string      `json:"enc_patient_id"`
	StartDateTime       db.NullTime `json:"start_date_time" validate:"required"`
	EndDateTime         db.NullTime `json:"end_date_time"`
	HospStartAge        int         `json:"hosp_start_age" `
	HospEndAge          int         `json:"hosp_end_age"`
	HospWay             int         `json:"hosp_way"`
	HospWayDet          string      `json:"hosp_way_det"`
	HospFacility        int         `json:"hosp_facility"`
	HospFacilityDet     string      `json:"hosp_facility_det"`
	HospCondition       string      `json:"hosp_condition"`
	LifeIndependence    int         `json:"life_independence"`
	StartBedsore        string      `json:"start_bedsore"`
	StartBedsoreComment string      `json:"start_bedsore_comment"`
	EndBedsore          string      `json:"end_bedsore"`
	EndBedsoreComment   string      `json:"end_bedsore_comment"`
	HospBt              float64     `json:"hosp_bt"`
	HospBph             int         `json:"hosp_bph"`
	HospBpl             int         `json:"hosp_bpl"`
	HospSpo2            int         `json:"hosp_spo2"`
	VitalComment        string      `json:"vital_comment"`
	Note                string      `json:"note"`
}

// PatientID : PatientID
type PatientID struct {
	EncPatientID string `query:"id"` // 患者ID。指定された場合はAndDisabled、Nameなどのパラメーターは無視。
}

// GetHospitalization :
func (m *Manager) GetHospitalization(encPatientID string) (hps []*Hospitalization, err error) {
	if encPatientID == "" {
		return
	}
	var q strings.Builder
	var tmpID, tmpPatientID int64
	q.WriteString("SELECT " +
		"h13n_id," +
		"patient_id," +
		"start_datetime," +
		"end_datetime," +
		"hosp_start_age," +
		"hosp_end_age," +
		"hosp_way," +
		"hosp_way_det," +
		"hosp_facility," +
		"hosp_facility_det," +
		"life_independence," +
		"hosp_condition," +
		"start_bedsore," +
		"start_bedsore_comment," +
		"end_bedsore," +
		"end_bedsore_comment," +
		"hosp_bt," +
		"hosp_bph," +
		"hosp_bpl," +
		"hosp_spo2," +
		"vital_comment," +
		"note")
	q.WriteString(" FROM tbl_h13n where patient_id=$1 order by start_datetime")
	// 暗号化解除された患者ID
	decID := m.DecryptID(encPatientID)
	con, err := m.NewDB()
	if err != nil {
		return
	}
	rows, err := con.Query(q.String(), decID)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), encPatientID)
		return
	}
	defer rows.Close()

	// 検索結果を作成
	hps = make([]*Hospitalization, 0, 50)
	for rows.Next() {
		hp := &Hospitalization{}
		err = rows.Scan(&tmpID, &tmpPatientID,  &hp.StartDateTime, &hp.EndDateTime, &hp.HospStartAge, &hp.HospEndAge, &hp.HospWay, &hp.HospWayDet, &hp.HospFacility, &hp.HospFacilityDet,
			&hp.LifeIndependence, &hp.HospCondition, &hp.StartBedsore, &hp.StartBedsoreComment, &hp.EndBedsore, &hp.EndBedsoreComment, &hp.HospBt, &hp.HospBph, &hp.HospBpl, &hp.HospSpo2, &hp.VitalComment, &hp.Note)
		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), &encPatientID)
			return
		}

		hp.EncID = m.EncryptID(tmpID)
		hp.EncPatientID = m.EncryptID(tmpPatientID)
		hps = append(hps, hp)
	}

	return
}

// AddHosp : 患者を追加する。
func (m *Manager) AddHosp(hp *Hospitalization) (err error) {
	var q string
	var encID int64
	if hp.EncID == "-1" {
		q = "INSERT INTO tbl_h13n (" +
			"patient_id," +
			"start_datetime," +
			"end_datetime," +
			"hosp_start_age," +
			"hosp_end_age," +
			"hosp_way," +
			"hosp_way_det," +
			"hosp_facility," +
			"hosp_facility_det," +
			"hosp_condition," +
			"life_independence," +
			"start_bedsore," +
			"start_bedsore_comment," +
			"end_bedsore," +
			"end_bedsore_comment," +
			"hosp_bt," +
			"hosp_bph," +
			"hosp_bpl," +
			"hosp_spo2," +
			"vital_comment," +
			"note" +
			") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21) "
	} else {
		encID = m.DecryptID(hp.EncID)
		q = "update tbl_h13n set " +
			"patient_id = $1," +
			"start_datetime = $2," +
			"end_datetime = $3," +
			"hosp_start_age = $4," +
			"hosp_end_age = $5," +
			"hosp_way = $6," +
			"hosp_way_det = $7," +
			"hosp_facility = $8," +
			"hosp_facility_det = $9," +
			"hosp_condition = $10," +
			"life_independence = $11," +
			"start_bedsore= $12," +
			"start_bedsore_comment = $13," +
			"end_bedsore = $14," +
			"end_bedsore_comment = $15," +
			"hosp_bt = $16," +
			"hosp_bph = $17," +
			"hosp_bpl = $18," +
			"hosp_spo2 = $19," +
			"vital_comment = $20," +
			"note = $21" +
			"where h13n_id = $22"
	}
	txm, err := m.NewTx()
	var tmpPatientID int64
	tmpPatientID = m.DecryptID(hp.EncPatientID)
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_h13n IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}

	if hp.EncID == "-1" {
		// T.B.D (Bulk insert)
		if _, err = txm.Tx.Exec(
			q, tmpPatientID,
			hp.StartDateTime,
			hp.EndDateTime,
			hp.HospStartAge,
			hp.HospEndAge,
			hp.HospWay,
			hp.HospWayDet,
			hp.HospFacility,
			hp.HospFacilityDet,
			hp.HospCondition,
			hp.LifeIndependence,
			hp.StartBedsore,
			hp.StartBedsoreComment,
			hp.EndBedsore,
			hp.EndBedsoreComment,
			hp.HospBt,
			hp.HospBph, hp.HospBpl,
			hp.HospSpo2,
			hp.VitalComment,
			hp.Note); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s", m.DebugMsg())
			return
		}
	} else {
		// T.B.D (Bulk insert)
		if _, err := txm.Tx.Exec(
			q, tmpPatientID,
			hp.StartDateTime,
			hp.EndDateTime,
			hp.HospStartAge,
			hp.HospEndAge,
			hp.HospWay,
			hp.HospWayDet,
			hp.HospFacility,
			hp.HospFacilityDet,
			hp.HospCondition,
			hp.LifeIndependence,
			hp.StartBedsore,
			hp.StartBedsoreComment,
			hp.EndBedsore,
			hp.EndBedsoreComment,
			hp.HospBt,
			hp.HospBph, hp.HospBpl,
			hp.HospSpo2,
			hp.VitalComment,
			hp.Note,
			encID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s", m.DebugMsg())
		}
	}

	txm.Commit()

	return
}
