package patient

import (
	"fmt"
	"hospital/db"
	"hospital/errors"
)

// Record : カルテ記録構造体
type Record struct {
	ID               int64       `json:"-"`                  // カルテID or レコードID
	EncID            string      `json:"karte_id"`           // カルテID or レコードID（暗号化）
	Type             int         `json:"record_type"`        // 記録タイプ
	KarteTitle       string      `json:"karte_title"`        // カルテタイトル
	DiagDeptID       int         `json:"diagdept_id"`        // 診療科ID
	DiagDeptName     string      `json:"diagdept_name"`      // 診療科名
	DiagTypeID       int         `json:"diagtype_id"`        // 診療タイプID
	DiagTypeName     string      `json:"diagtype_name"`      // 診療タイプ名
	Body             string      `json:"karte_body"`         // 記録
	Memo             string      `json:"karte_memo"`         // 備考
	EncDoctorID      string      `json:"doctor_id"`          // ドクターID
	DoctorName       string      `json:"doctor_name"`        // ドクター名
	EncUserID        string      `json:"input_user_id"`      // 入力ユーザーID
	UserName         string      `json:"input_user_name"`    // 入力ユーザー名
	SubstituteInput  bool        `json:"substitute_input"`   // 代理入力
	ActStartDateTime db.NullTime `json:"act_start_datetime"` // 診療開始日時
	ActEndDateTime   db.NullTime `json:"act_end_datetime"`   // 診療終了日時
	NextDateTime     db.NullTime `json:"next_datetime"`      // 次回診療開始日時
	Status           int         `json:"status"`             // ステータス
	KarteType        int         `json:"karte_type"`         // カルテタイプ
	OrcaSend         int         `json:"orca_send"`          // ORCA T.B.D
	UpdDateTime      db.NullTime `json:"upd_datetime"`
	RegDateTime      db.NullTime `json:"reg_datetime"`
}

var karteType Record

// Records : カルテ記録を取得する。
func (m *Manager) Records(encPatientID, encH13nID string, recordType int) (recs []*Record, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	patientID := m.DecryptID(encPatientID)
	h13nID := m.DecryptID(encH13nID)
	s := "FROM view_record WHERE patient_id=$1 AND h13n_id=$2"
	if recordType >= 0 {
		s += fmt.Sprintf(" AND record_type=%d", recordType)
	}
	s += " ORDER BY act_start_datetime DESC,record_type,karte_id"
	q := db.SelectQuery(karteType, "json", nil, s)
	rows, err := con.Query(q, patientID, h13nID)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	recs = make([]*Record, 0)
	for rows.Next() {
		rec := &Record{}
		var docID, userID db.NullInt64
		if err = rows.Scan(&rec.ID, &rec.Type, &rec.KarteTitle,
			&rec.DiagDeptID, &rec.DiagDeptName, &rec.DiagTypeID, &rec.DiagTypeName,
			&rec.Body, &rec.Memo, &docID, &rec.DoctorName, &userID, &rec.UserName, &rec.SubstituteInput,
			&rec.ActStartDateTime, &rec.ActEndDateTime, &rec.NextDateTime, &rec.Status, &rec.KarteType, &rec.OrcaSend,
			&rec.UpdDateTime, &rec.RegDateTime); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
			return
		}

		rec.EncID = m.EncryptID(rec.ID)
		rec.EncDoctorID = m.EncryptID(docID.Int64)
		rec.EncUserID = m.EncryptID(userID.Int64)

		recs = append(recs, rec)
	}

	return
}

// AddKarte : カルテを登録する。
func (m *Manager) AddKarte(encPatientID, encH13nID string, rec *Record) (err error) {
	patientID := m.DecryptID(encPatientID)
	h13nID := m.DecryptID(encH13nID)

	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := "INSERT INTO tbl_karte (" +
		"patient_id," +
		"h13n_id," +
		"karte_title," +
		"diagdept_id," +
		"diagtype_id," +
		"doctor_id," +
		"doctor_name," +
		"input_user_id," +
		"input_user_name," +
		"act_start_datetime," +
		"karte_body," +
		"karte_memo," +
		"substitute_input," +
		"status," +
		"karte_type," +
		"orca_send) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)"
	if _, err = con.Exec(q, patientID, h13nID, rec.KarteTitle, rec.DiagDeptID, rec.DiagTypeID,
		m.LoginUserID(), m.LoginFamilyName()+m.LoginFirstName(), // T.B.D
		m.LoginUserID(), m.LoginFamilyName()+m.LoginFirstName(),
		rec.ActStartDateTime, rec.Body, rec.Memo, rec.SubstituteInput,
		/*T.B.D*/ 0, 0, 0); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	return
}

// UpdateKarte : カルテを更新する。
func (m *Manager) UpdateKarte(rec *Record) (err error) {
	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	karteID := m.DecryptID(rec.EncID)
	q := "UPDATE tbl_karte SET " +
		"karte_title=$1," +
		"diagdept_id=$2," +
		"diagtype_id=$3," +
		"input_user_id=$4," +
		"input_user_name=$5," +
		"act_start_datetime=$6," +
		"karte_body=$7," +
		"karte_memo=$8," +
		"substitute_input=$9," +
		"status=$10," +
		"karte_type=$11," +
		"orca_send=$12 " +
		"WHERE karte_id=$13"
	r, err := txm.Tx.Exec(q, rec.KarteTitle, rec.DiagDeptID, rec.DiagTypeID, m.LoginUserID(), m.LoginFamilyName()+m.LoginFirstName(),
		rec.ActStartDateTime, rec.Body, rec.Memo, rec.SubstituteInput /*T.B.D*/, 0, 0, 0, karteID)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}
	count, err := r.RowsAffected()
	if err != nil || count != 1 {
		err = errors.ErrFatal.Wrapf(err, "%s, count:%d, sql:%s", m.DebugMsg(), count, q)
		return
	}

	txm.Tx.Commit()

	return
}
