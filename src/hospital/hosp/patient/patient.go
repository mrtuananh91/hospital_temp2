package patient

import (
	"database/sql"
	"fmt"
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	"hospital/util"
	"strings"
)

// Manager : 患者管理用構造体
type Manager struct {
	util.Manager
}

// New : 患者管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}

// Patient : 患者情報構造体
type Patient struct {
	m                    *Manager
	ID                   int64       `json:"-"`                                                     // 患者ID（整数）
	EncID                string      `json:"patient_id"`                                            // 患者ID
	OrcaID               string      `json:"orca_id" validate:"required,ascii,max=10"`              // ORCA ID
	FamilyName           string      `json:"family_name" validate:"required,max=30"`                // 苗字
	FirstName            string      `json:"first_name" validate:"required,max=30"`                 // 名前
	FamilyNameKana       string      `json:"family_name_kana" validate:"required,max=30"`           // 苗字（カナ）
	FirstNameKana        string      `json:"first_name_kana" validate:"required,max=30"`            // 名前（カナ）
	Gender               int         `json:"gender" validate:"min=1,max=2"`                         // 性別
	Birthday             db.NullTime `json:"birthday" validate:"required"`                          // 生年月日
	BloodType            string      `json:"blood_type" validate:"required,max=10"`                 // 血液型
	PostalCode           *string     `json:"postal_code" validate:"omitempty,numeric,len=7"`        // 郵便番号
	Country              *string     `json:"country" validate:"omitempty,max=16"`                   // 国籍
	Address              *string     `json:"address" validate:"omitempty,max=200"`                  // 住所
	Tel                  *string     `json:"tel" validate:"omitempty,max=16"`                       // 電話番号
	Mobilephone          *string     `json:"mobilephone" validate:"omitempty,max=16"`               // 携帯番号
	EMGName1             *string     `json:"emg_name1" validate:"omitempty,max=32"`                 // 緊急連絡先1（氏名）
	EMGName2             *string     `json:"emg_name2" validate:"omitempty,max=32"`                 // 緊急連絡先2（氏名）
	EMGName3             *string     `json:"emg_name3" validate:"omitempty,max=32"`                 // 緊急連絡先3（氏名）
	EMGTel1              *string     `json:"emg_tel1" validate:"omitempty,max=16"`                  // 緊急連絡先1（電話番号）
	EMGTel2              *string     `json:"emg_tel2" validate:"omitempty,max=16"`                  // 緊急連絡先2（電話番号）
	EMGTel3              *string     `json:"emg_tel3" validate:"omitempty,max=16"`                  // 緊急連絡先3（電話番号）
	EMGMobilephone1      *string     `json:"emg_mobilephone1" validate:"omitempty,max=16"`          // 緊急連絡先1（携帯番号）
	EMGMobilephone2      *string     `json:"emg_mobilephone2" validate:"omitempty,max=16"`          // 緊急連絡先2（携帯番号）
	EMGMobilephone3      *string     `json:"emg_mobilephone3" validate:"omitempty,max=16"`          // 緊急連絡先3（携帯番号）
	EMGRel1              *string     `json:"emg_rel1" validate:"omitempty,max=32"`                  // 緊急連絡先1（本人との関係）
	EMGRel2              *string     `json:"emg_rel2" validate:"omitempty,max=32"`                  // 緊急連絡先2（本人との関係）
	EMGRel3              *string     `json:"emg_rel3" validate:"omitempty,max=32"`                  // 緊急連絡先3（本人との関係）
	KeyPersonName        *string     `json:"key_person_name" validate:"omitempty,max=32"`           // キーパーソン（氏名）
	KeyPersonRel         *string     `json:"key_person_rel" validate:"omitempty,max=32"`            // キーパーソン（本人との関係）
	FamilyInfo           *string     `json:"family_info" validate:"omitempty,max=65536"`            // 家庭の事情
	DiseaseHistory       *string     `json:"disease_history" validate:"omitempty,max=65536"`        // 病歴
	FamilyDiseaseHistory *string     `json:"family_disease_history" validate:"omitempty,max=65536"` // 家族歴
	Note                 *string     `json:"note" validate:"omitempty,max=65536"`                   // 備考
	H13nDays             []*H13nDay  `json:"h13n_days"`                                             // 入院日リスト
	DeadDateTime         db.NullTime `json:"dead_datetime"`                                         // 死亡日時
	UpdDateTime          db.NullTime `json:"upd_datetime"`                                          // 更新日時
	RegDate              db.NullTime `json:"reg_date"`                                              // 登録日
}

// H13nDay : 入院
type H13nDay struct {
	EncID         string      `json:"id"`             // 入院ID
	StartDateTime db.NullTime `json:"start_datetime"` // 入院日時
	EndDateTime   db.NullTime `json:"end_datetime"`   // 退院日時
}

// Search : ユーザー検索パラメーター
type Search struct {
	EncPatientID  string `query:"patient_id"`     // 患者ID。指定された場合はAndDisabled、Nameなどのパラメーターは無視。
	Offset        int    `query:"offset"`         // オフセット値
	Limit         int    `query:"limit"`          // 最大結果数（-1の場合は制限なし、0と-2以下の場合はデフォルト値、最大値以上の場合は最大値に強制変更）
	Name          string `query:"name"`           // 検索キー（氏名）
	OrderID       bool   `query:"order_id"`       // IDの表示順（昇順：false、降順：true）
	OrderName     bool   `query:"order_name"`     // 名前（カナ）の表示順（昇順：false、降順：true）
	OrderBirthday bool   `query:"order_birthday"` // 誕生日の表示順（昇順：false、降順：true）
	OrderDays     bool   `query:"order_days"`     // 入院日数の表示順（昇順：false、降順：true）
	OrderFloor    bool   `query:"order_floor"`    // 階数の表示順（昇順：false、降順：true）
	OrderRoom     bool   `query:"order_room"`     // 部屋名の表示順（昇順：false、降順：true）
	AutoComplete  bool   `query:"auto_comp"`      // 氏名と性別と患者IDのみ返却（for オートコンプリート）
}

// Search : 患者を検索する。
func (m *Manager) Search(srh *Search) (pts []*Patient, err error) {
	if srh == nil {
		srh = &Search{}
	}
	if srh.Limit == 0 || srh.Limit < -1 {
		srh.Limit = m.Conf().Patient.DefSearchCount
	}
	if srh.Limit > m.Conf().Patient.MaxSearchCount {
		srh.Limit = m.Conf().Patient.MaxSearchCount
	}

	// 検索条件を設定
	var q strings.Builder
	q.WriteString("SELECT " +
		"tbl_patient.patient_id," +
		"family_name," +
		"first_name," +
		"family_name_kana," +
		"first_name_kana," +
		"gender")

	if !srh.AutoComplete {
		q.WriteString(",orca_id," +
			"birthday," +
			"blood_type," +
			"postal_code," +
			"country," +
			"address," +
			"tel," +
			"mobilephone," +
			"emg_name1," +
			"emg_name2," +
			"emg_name3," +
			"emg_tel1," +
			"emg_tel2," +
			"emg_tel3," +
			"emg_mobilephone1," +
			"emg_mobilephone2," +
			"emg_mobilephone3," +
			"emg_rel1," +
			"emg_rel2," +
			"emg_rel3," +
			"key_person_name," +
			"key_person_rel," +
			"family_info," +
			"disease_history," +
			"family_disease_history," +
			"note," +
			"dead_datetime," +
			"upd_datetime," +
			"reg_datetime")
	}
	// 担当患者の検索
	q.WriteString(" FROM tbl_patient WHERE 1=1")
	//q.WriteString(" FROM tbl_patient INNER JOIN tbl_patient_user ON tbl_patient_user.patient_id=tbl_patient.patient_id " +
	//	"WHERE tbl_patient_user.patient_id IN (SELECT patient_id FROM tbl_patient_user WHERE user_id IN (-1,$1))")
	args := make([]interface{}, 0)
	//args = append(args, m.LoginUserID())
	//i := 1
	i := 0
	if srh.EncPatientID != "" {
		// 患者ID検索
		i++
		q.WriteString(fmt.Sprintf(" AND tbl_patient.patient_id=$%d", i))
		args = append(args, m.DecryptID(srh.EncPatientID))
	} else {
		// 氏名検索
		if srh.Name != "" {
			i++
			q.WriteString(fmt.Sprintf(" AND (family_name LIKE %%$%d%% OR first_name LIKE %%%d%% OR family_name_kana LIKE %%$%d%% OR first_name_kana LIKE %%$%d%%)", i, i, i, i))
			args = append(args, srh.Name)
		}
		q.WriteString(fmt.Sprintf(" ORDER BY family_name_kana,first_name_kana,tbl_patient.patient_id OFFSET %d", srh.Offset))
		if srh.Limit != -1 {
			q.WriteString(fmt.Sprintf(" LIMIT %d", srh.Limit))
		}
	}

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}

	/*ids, sts, err := db.SelectQuery(con, Patient{}, "patient_id", "json", []string{"h13n_days"}, fmt.Sprintf("FROM tbl_patient ORDER BY family_name_kana,first_name_kana,tbl_patient.patient_id OFFSET %d", srh.Offset))
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}
	if len(sts) > 0 {
		pts = make([]*Patient, len(sts))
		for i, st := range sts {
			pts[i] = st.(*Patient)
			pts[i].ID = ids[i]
			pts[i].EncID = m.EncryptID(ids[i])
		}
	}*/

	var rows *sql.Rows
	if i == 0 {
		rows, err = con.Query(q.String())
	} else {
		rows, err = con.Query(q.String(), args...)
	}
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}
	defer rows.Close()

	// 検索結果を作成
	pts = make([]*Patient, 0, 50)
	for rows.Next() {
		pt := &Patient{m: m}
		if srh.AutoComplete {
			err = rows.Scan(&pt.ID, &pt.FamilyName, &pt.FirstName, &pt.FamilyNameKana, &pt.FirstNameKana, &pt.Gender)
		} else {
			err = rows.Scan(&pt.ID, &pt.FamilyName, &pt.FirstName, &pt.FamilyNameKana, &pt.FirstNameKana, &pt.Gender,
				&pt.OrcaID, &pt.Birthday, &pt.BloodType, &pt.PostalCode, &pt.Country, &pt.Address, &pt.Tel, &pt.Mobilephone,
				&pt.EMGName1, &pt.EMGName2, &pt.EMGName3, &pt.EMGTel1, &pt.EMGTel2, &pt.EMGTel3,
				&pt.EMGMobilephone1, &pt.EMGMobilephone2, &pt.EMGMobilephone3,
				&pt.EMGRel1, &pt.EMGRel2, &pt.EMGRel3, &pt.KeyPersonName, &pt.KeyPersonRel,
				&pt.FamilyInfo, &pt.DiseaseHistory, &pt.FamilyDiseaseHistory, &pt.Note, &pt.DeadDateTime, &pt.UpdDateTime, &pt.RegDate)
		}
		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		pt.EncID = m.EncryptID(pt.ID)
		pts = append(pts, pt)
	}

	if len(pts) != 1 || srh.EncPatientID == "" {
		return
	}

	// 患者ID検索の場合、入院情報も取得する。
	pt := pts[0]
	if rows, err = con.Query("SELECT h13n_id,start_datetime,end_datetime FROM tbl_h13n WHERE patient_id=$1 ORDER BY start_datetime DESC,h13n_id DESC", pt.ID); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
		return
	}

	h13Days := make([]*H13nDay, 0)
	for rows.Next() {
		h13n := &H13nDay{}
		var id int64
		if err = rows.Scan(&id, &h13n.StartDateTime, &h13n.EndDateTime); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		h13n.EncID = m.EncryptID(id)
		h13Days = append(h13Days, h13n)
	}
	if len(h13Days) > 0 {
		pt.H13nDays = h13Days
	}

	return
}

// Add : 患者を追加する。
func (m *Manager) Add(pts []*Patient) (err error) {
	q := "INSERT INTO tbl_patient (" +
		"orca_id," +
		"family_name," +
		"first_name," +
		"family_name_kana," +
		"first_name_kana," +
		"gender," +
		"birthday," +
		"blood_type," +
		"postal_code," +
		"country," +
		"address," +
		"tel," +
		"mobilephone," +
		"emg_name1," +
		"emg_name2," +
		"emg_name3," +
		"emg_tel1," +
		"emg_tel2," +
		"emg_tel3," +
		"emg_mobilephone1," +
		"emg_mobilephone2," +
		"emg_mobilephone3," +
		"emg_rel1," +
		"emg_rel2," +
		"emg_rel3," +
		"key_person_name," +
		"key_person_rel," +
		"family_info," +
		"disease_history," +
		"family_disease_history," +
		"note," +
		"dead_datetime)" +
		" VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32) RETURNING patient_id"

	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_patient IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}

	// T.B.D (Bulk insert)
	for _, pt := range pts {
		if err = txm.Tx.QueryRow(q, &pt.OrcaID, pt.FamilyName, pt.FirstName, pt.FamilyNameKana, pt.FirstNameKana, pt.Gender, pt.Birthday, pt.BloodType,
			pt.PostalCode, pt.Country, pt.Address, pt.Tel, pt.Mobilephone,
			pt.EMGName1, pt.EMGName2, pt.EMGName3,
			pt.EMGTel1, pt.EMGTel2, pt.EMGTel3,
			pt.EMGMobilephone1, pt.EMGMobilephone2, pt.EMGMobilephone3,
			pt.EMGRel1, pt.EMGRel2, pt.EMGRel3,
			pt.KeyPersonName, pt.KeyPersonRel, pt.FamilyInfo,
			pt.DiseaseHistory, pt.FamilyDiseaseHistory, pt.Note, pt.DeadDateTime).Scan(&pt.ID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, family:%s, first:%s", m.DebugMsg(), pt.FamilyNameKana, pt.FirstNameKana)
			return
		}

		//if _, err = txm.Tx.Exec("INSERT INTO tbl_patient_user (patient_id,user_id,main) VALUES ($1,-1,false)", pt.ID); err != nil {
		//	err = errors.ErrFatal.Wrapf(err, "%s, family:%s, first:%s", m.DebugMsg(), pt.FamilyNameKana, pt.FirstNameKana)
		//	return
		//}
	}

	txm.Commit()

	return
}
