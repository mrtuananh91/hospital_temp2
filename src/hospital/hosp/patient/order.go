package patient

import (
	"hospital/db"
	"hospital/errors"
	"strconv"
	"strings"
	"time"
)

// Order : オーダー構造体
type Order struct {
	ID          int64  `json:"-"`
	EncOrderID  string `json:"id"`
	OrderType   string `json:"order_type"`
	CompleteNum int    `json:"complete_num"`
}

// OrderInjection : オーダー注射構造体
type OrderInjection struct {
	ID                  int64  `json:"-" `      // 血液型
	EncID               string `json:"id" `     // 血液型
	InjectionMedCode    string `json:"code" `   // 血液型
	InjectionMedJancode string `json:"jancode"` // 血液型
	InjectionMedQuantum string `json:"quantum"` // 血液型
	InjectionMedName    string `json:"name"`    // 血液型
	InjectionSpeed      string `json:"injection_speed"`
}

type OrderAct struct {
	ID           int64   `json:"-" `                        // 血液型
	EncID        string  `json:"id" `                       // 血液型
	ActUser1     *string `json:"act_user1" default0:""`     // 血液型
	ActDatetime1 *string `json:"act_datetime1" default0:""` // 血液型
	ActStatus1   *string `json:"act_status1" default0:""`   // 血液型
	ActUser2     *string `json:"act_user2" default0:""`     // 血液型
	ActDatetime2 *string `json:"act_datetime2" default0:""` // 血液型
	ActStatus2   *string `json:"act_status2" default0:""`   // 血液型
	ActUser3     *string `json:"act_user3" default0:""`     // 血液型
	ActDatetime3 *string `json:"act_datetime3" default0:""` // 血液型
	ActStatus3   *string `json:"act_status3" default0:""`   // 血液型
	ActUser4     *string `json:"act_user4" default0:""`     // 血液型
	ActDatetime4 *string `json:"act_datetime4" default0:""` // 血液型
	ActStatus4   *string `json:"act_status4" default0:""`   // 血液型

}

// OrderMeal :
type OrderMeal struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// OrderReha :
type OrderReha struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// OrderPhysiological :
type OrderPhysiological struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// OrderRadiation :
type OrderRadiation struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// OrderOpe :
type OrderOpe struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// OrderTransfusion :
type OrderTransfusion struct {
	ID string `json:"id" ` // オーダー詳細ID
}

// PatientOrder : 患者オーダー構造体
type PatientOrder struct {
	ID         int64   `json:"-"`                                        // 患者ID（整数）
	EncID      string  `json:"id"`                                       // 患者ID
	OrcaID     *string `json:"orca_id" validate:"required,ascii,max=10"` // ORCA ID
	OrderID    int64   `json:"order_id" validate:"required"`
	EncOrderID string  `json:"-" validate:"required"` // オーダーID
	FloorID    int64   `json:"-" validate:"required"`
	EncFloorID string  `json:"floor_id" validate:"required"`
	StartDate  string  `json:"start_date" validate:"required"`
	Comment    *string `json:"-" `

	// オーダーID
	FamilyName          *string               `json:"family_name" validate:"required,max=30"`      // 苗字
	FirstName           *string               `json:"first_name" validate:"required,max=30"`       // 名前
	FamilyNameKana      *string               `json:"family_name_kana" validate:"required,max=30"` // 苗字（カナ）
	FirstNameKana       *string               `json:"first_name_kana" validate:"required,max=30"`  // 名前（カナ）
	Gender              int                   `json:"gender" validate:"min=1,max=2"`               // 性別
	Birthday            db.NullTime           `json:"birthday" validate:"required"`                // 生年月日
	BloodType           *string               `json:"blood_type" validate:"required,max=10"`       // 血液型
	RoomNumber          *string               `json:"room_number" `                                // 室番号
	H13nID              *string               `json:"h13n_id"`
	Orders              []*Order              `json:"orders"`
	OrderAct            *OrderAct             `json:"order_act"`
	OrderInjections     []*OrderInjection     `json:"order_injections"`
	OrderMeals          []*OrderMeal          `json:"order_meal"`
	OrderRehas          []*OrderReha          `json:"order_reha"`
	OrderPhysiologicals []*OrderPhysiological `json:"order_physiological"`
	OrderRadiations     []*OrderRadiation     `json:"order_radiation"`
	OrderOpes           []*OrderOpe           `json:"order_ope"`
	OrderTransfusions   []*OrderTransfusion   `json:"order_transfusion"`
}

// GetPatientOrders : 患者さんのオーダー情報を取得する。
func (m *Manager) GetPatientOrders(floorID string, date string, orderType int) (pos []*PatientOrder, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	// フロアーの全ての患者さんを取得するSQL文を作成する。
	var sql string = `
		SELECT
			tp.patient_id
			, tp.orca_id
			, tor.order_id
			, tr.room_number
			, tp.family_name
			, tp.first_name
			, th.h13n_id
			, tf.floor_id
			, tor.start_date
			, tor.comment
		FROM tbl_patient tp
            INNER JOIN tbl_bed_assign tba ON tba.patient_id = tp.patient_id
            INNER JOIN tbl_bed tb ON tb.bed_id = tba.bed_id
            INNER JOIN tbl_room tr ON tr.room_id = tb.room_id
            INNER JOIN tbl_floor tf ON tf.floor_id = tb.floor_id
			INNER JOIN tbl_h13n th ON th.patient_id = tp.patient_id
			INNER JOIN tbl_order tor ON tor.h13n_id = th.h13n_id AND tor.patient_id = tp.patient_id
		WHERE 1 = 1 
	`

	// フロアー条件を設定
	if strings.Compare(floorID, "all") != 0 {
		decID := m.DecryptID(floorID)
		sql += ` AND tf.floor_id = ` + strconv.FormatInt(decID, 10)
	}
	// 日付条件を設定
	if date != "" {
		sql += ` AND tor.start_date <= '` + date + `'::date AND tor.end_date >= '` + date + `'::date`
	}
	// オーダータイプ条件を設定
	if orderType > 0 {
		sql += ` AND tor.order_type = ` + strconv.Itoa(orderType)
	}
	// ルーム番号で並び替える
	sql += ` ORDER BY tr.room_number`

	// フロアーの全ての患者さんを取得する。
	rows, err := con.Query(sql)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), sql)
		return
	}
	defer rows.Close()

	// オーダー詳細の情報を取得する。
	pos = make([]*PatientOrder, 0)
	for rows.Next() {
		p := &PatientOrder{}
		if err = rows.Scan(
			&p.ID,
			&p.OrcaID,
			&p.OrderID,
			&p.RoomNumber,
			&p.FamilyName,
			&p.FirstName,
			&p.H13nID,
			&p.FloorID,
			&p.StartDate,
			&p.Comment,
		); err != nil {
			err = errors.ErrFatal.Wrap(err, m.DebugMsg())
			return
		}

		// 全てのオーダーの状態を取得する場合。
		if orderType < 1 {
			ords, _ := m.GetOrders(p.ID, date)
			p.Orders = ords
		}

		// 注射オーダーを取得する場合。
		if orderType == 2 {
			ois, _ := m.GetOrderInjections(p.ID, p.OrderID)
			p.OrderInjections = ois
			toa, _ := m.GetOrderAct(2, p.OrderID, date)
			p.OrderAct = toa
		}

		// 取得したオーダー詳細情報を患者オーダー情報に追加
		// p.EncOrderID = m.EncryptID(p.OrderID)
		p.EncFloorID = m.EncryptID(p.FloorID)
		p.EncID = m.EncryptID(p.ID)
		pos = append(pos, p)
	}

	return
}

func (m *Manager) GetOrderAct(orderType int, orderID int64, date string) (toa *OrderAct, err error) {

	con, err := m.NewDB()
	if err != nil {
		return
	}

	var sqlAct string = `
	SELECT 
		toa.order_act_id, 
		toa.act_user1, 
		toa.act_datetime1, 
		toa.act_user2, 
		toa.act_datetime2, 
		toa.act_user3,
		toa.act_datetime3, 
		toa.act_status3, 
		toa.act_user4,
		toa.act_datetime4 
	FROM tbl_order_act toa 
	WHERE toa.order_id = $1 AND toa.order_type = $2 AND toa.order_date=$3 LIMIT 1`
	if rows, err2 := con.Query(sqlAct, orderID, orderType, date); err2 == nil {
		if rows.Next() {
			toa = &OrderAct{}
			if err2 = rows.Scan(
				&toa.ID,
				&toa.ActUser1,
				&toa.ActDatetime1,
				&toa.ActUser2,
				&toa.ActDatetime2,
				&toa.ActUser3,
				&toa.ActDatetime3,
				&toa.ActStatus3,
				&toa.ActUser4,
				&toa.ActDatetime4); err2 != nil {
				err2 = errors.ErrFatal.Wrap(err2, m.DebugMsg())
			} else {
				toa.EncID = m.EncryptID(toa.ID)

				if toa.ActDatetime1 != nil {
					if time, err1 := time.Parse(time.RFC3339, *toa.ActDatetime1); err1 == nil {
						*toa.ActDatetime1 = time.Format("2006-01-02 15:04")
					}
				}

				if toa.ActDatetime2 != nil {
					if time, err1 := time.Parse(time.RFC3339, *toa.ActDatetime2); err1 == nil {
						*toa.ActDatetime2 = time.Format("2006-01-02 15:04")
					}
				}

				if toa.ActDatetime3 != nil {
					if time, err1 := time.Parse(time.RFC3339, *toa.ActDatetime3); err1 == nil {
						*toa.ActDatetime3 = time.Format("2006-01-02 15:04")
					}
				}
			}
		}
	}
	return

}

// GetOrders :
func (m *Manager) GetOrders(patientID int64, date string) (ords []*Order, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	var sql string = `
		SELECT
			tor.order_id
			, tor.order_type
			, tor.complete_num
		FROM tbl_order tor
			LEFT JOIN tbl_order_act toa
				ON tor.order_id = toa.order_id
				AND tor.patient_id = toa.patient_id
				AND tor.order_type = toa.order_type
		WHERE tor.patient_id = $1
			AND toa.order_date = $2
	`
	if rows, err1 := con.Query(sql, patientID, date); err1 == nil {
		for rows.Next() {
			o := &Order{}
			if err1 = rows.Scan(
				&o.ID,
				&o.OrderType,
				&o.CompleteNum,
			); err1 != nil {
				err1 = errors.ErrFatal.Wrap(err, m.DebugMsg())
			} else {
				o.EncOrderID = m.EncryptID(o.ID)
				ords = append(ords, o)
			}
		}
	}

	return
}

// GetOrderInjections :
func (m *Manager) GetOrderInjections(patientID int64, orderID int64) (ois []*OrderInjection, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	var sql string = `
		SELECT
			order_injection_id
			, injection_med_name
			, injection_med_quantum
			, injection_med_code
		FROM tbl_order_injection
		WHERE patient_id = $1 AND order_id = $2
	`
	if rows, err1 := con.Query(sql, patientID, orderID); err1 == nil {
		for rows.Next() {
			oi := &OrderInjection{}
			if err1 = rows.Scan(&oi.ID, &oi.InjectionMedName, &oi.InjectionMedQuantum, &oi.InjectionMedCode); err1 != nil {
				err1 = errors.ErrFatal.Wrap(err, m.DebugMsg())
			} else {
				oi.EncID = m.EncryptID(oi.ID)
				ois = append(ois, oi)
			}
		}
	}

	return
}
