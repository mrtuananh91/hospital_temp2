package conf

import (
	"hospital/db"
	"hospital/errors"
)

// DiagDept : 診療科
type DiagDept struct {
	ID        int64  `json:"diagdept_id" validate:"required,numeric,min=1"`
	Code      string `json:"diagdept_code" validate:"required,numeric,len=2"`
	Name      string `json:"diagdept_name" validate:"required,max=64"`
	OrderNo   int    `json:"order_no"`
	NoUseFlag bool   `json:"nouse_flag"`
}

var diagDeptType = DiagDept{}

// DiagDepts : 診療科リストを取得する。
func (m *Manager) DiagDepts() (dds []*DiagDept, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := db.SelectQuery(diagDeptType, "json", nil, "FROM tbl_diagdept ORDER BY order_no DESC, diagdept_id")
	rows, err := con.Query(q)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	dds = make([]*DiagDept, 0)
	for rows.Next() {
		dd := &DiagDept{}
		if err = rows.Scan(&dd.ID, &dd.Code, &dd.Name, &dd.OrderNo, &dd.NoUseFlag); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
			return
		}

		dds = append(dds, dd)
	}

	return
}

// AddDiagDept : 診療科を追加する。
func (m *Manager) AddDiagDept(dd *DiagDept) (err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := "INSERT INTO tbl_diagdept (diagdept_code,diagdept_name,order_no,nouse_flag) VALUES ($1,$2,$3,$4)"
	if _, err = con.Exec(q, dd.Code, dd.Name, dd.OrderNo, dd.NoUseFlag); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	return
}

// DiagType : 診療タイプ
type DiagType struct {
	ID        int64  `json:"diagtype_id" validate:"required,numeric,min=1"`
	Code      int    `json:"diagtype_code" validate:"required,min=1,max=999"`
	Name      string `json:"diagtype_name" validate:"required,max=64"`
	OrderNo   int    `json:"order_no"`
	NoUseFlag bool   `json:"nouse_flag"`
}

var diagTypeType = DiagType{}

// DiagTypes : 診療タイプリストを取得する。
func (m *Manager) DiagTypes() (dts []*DiagType, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := db.SelectQuery(diagTypeType, "json", nil, "FROM tbl_diagtype ORDER BY order_no DESC, diagtype_id")
	rows, err := con.Query(q)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	dts = make([]*DiagType, 0)
	for rows.Next() {
		dt := &DiagType{}
		if err = rows.Scan(&dt.ID, &dt.Code, &dt.Name, &dt.OrderNo, &dt.NoUseFlag); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
			return
		}

		dts = append(dts, dt)
	}

	return
}

// AddDiagType : 診療タイプを追加する。
func (m *Manager) AddDiagType(dt *DiagType) (err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	q := "INSERT INTO tbl_diagtype (diagtype_code,diagtype_name,order_no,nouse_flag) VALUES ($1,$2,$3,$4)"
	if _, err = con.Exec(q, dt.Code, dt.Name, dt.OrderNo, dt.NoUseFlag); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s", m.DebugMsg(), q)
		return
	}

	return
}
