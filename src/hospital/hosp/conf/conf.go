package conf

import (
	"hospital/conf"
	"hospital/util"
)

// Manager : フロア管理用構造体
type Manager struct {
	util.Manager
}

// New : フロア管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}
