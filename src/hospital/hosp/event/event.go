package event

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"hospital/db"
	"hospital/errors"
	"hospital/hosp/user"
)

// Event : イベント
//go:generate enumer -type=Event -json
type Event int

// イベントID
const (
	Login Event = iota + 1
)

// Manager : イベントログ管理用構造体
type Manager struct {
	hospID  int64
	logined *user.User
	errMsg  string
}

func (m *Manager) newDB() (*sql.DB, error) {
	return db.New(m.hospID)
}

// New : イベントログ管理用構造体を生成する。
func New(hospID int64, errMsg string) *Manager {
	return &Manager{
		hospID: hospID,
		errMsg: errMsg,
	}
}

// SetLoginInfo : ログインユーザー情報を設定する。
func (m *Manager) SetLoginInfo(lu *user.User) {
	m.logined = lu
	m.errMsg = lu.DebugMsg()
}

// Events : イベントログを取得する。
func (m *Manager) Events() (es []*Event, err error) {
	// T.B.D
	return
}

// WriteEvent : イベントを記録する。
func (m *Manager) WriteEvent(evt Event, ipAddr, userAgent string, info interface{}, ec errors.ErrCode) (err error) {
	errMsg := func() string {
		return fmt.Sprintf("%s, evt:%s, ec:%s", m.errMsg, evt.String(), ec.String())
	}

	var s string
	if info != nil {
		var b []byte
		if b, err = json.Marshal(info); err != nil {
			err = errors.ErrFatal.Wrapf(err, errMsg())
		}
		s = string(b)
	}

	con, err := m.newDB()
	if err != nil {
		return
	}

	if _, err = con.Exec("INSERT INTO tbl_eventlog (event_id,ip_addr,account,user_agent,error_code,info) VALUES ($1,$2,$3,$4,$5,$6)",
		evt, ipAddr, m.logined.Account, userAgent, ec.String(), s); err != nil {
		err = errors.ErrFatal.Wrapf(err, errMsg())
		return
	}

	return
}
