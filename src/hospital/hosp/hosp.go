package hosp

import (
	"database/sql"
	"fmt"
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	hconf "hospital/hosp/conf"
	"hospital/hosp/event"
	"hospital/hosp/floor"
	"hospital/hosp/forum"
	"hospital/hosp/inchg"
	"hospital/hosp/patient"
	"hospital/hosp/record"
	"hospital/hosp/user"
	"hospital/prov"
	"hospital/session"
	"hospital/util"
)

func encryptHospID(hospID int64) string {
	return util.EncryptID(hospID)
}

// DecryptHospID : 病院IDを復号化する。
func DecryptHospID(encHospID string) int64 {
	return util.DecryptID(encHospID)
}

// Hosp : 病院情報
type Hosp struct {
	Prov        *prov.Prov `json:"-"`
	ID          int64      `json:"-"`
	EncID       string     `json:"id"`
	Name        string     `json:"name" validate:"required,max=100"`
	ZipCode     string     `json:"zipCode" validate:"required,len=7,numeric"`
	Address     string     `json:"address" validate:"required,max=128"`
	Tel         string     `json:"tel" validate:"required,tel"`
	Fax         string     `json:"fax" validate:"required,tel"`
	userMngr    *user.Manager
	patientMngr *patient.Manager
	floorMngr   *floor.Manager
	forumMngr   *forum.Manager
	confMngr    *hconf.Manager
	eventMngr   *event.Manager
	recordMngr  *record.Manager
	cfg         *conf.HospConfig
	inchgMngr   *inchg.Manager
	errMsg      string
}

func (h *Hosp) newDB() (*sql.DB, error) {
	return db.New(h.ID)
}

// Conf : 病院の設定情報を取得する。
func (h *Hosp) Conf() *conf.HospConfig {
	return h.cfg
}

// DebugMsg : エラーメッセージを取得する。
func (h *Hosp) DebugMsg() string {
	errMsg := h.errMsg
	if h.userMngr.Logined() != nil {
		errMsg = h.userMngr.Logined().DebugMsg()
	}
	return errMsg
}

// New : 病院情報を生成する。
//  prov : プロバイダー
//  id : 病院ID
func New(prov *prov.Prov, id int64) (h *Hosp, err error) {
	h = &Hosp{
		Prov: prov,
		ID:   id,
	}

	// 病院検索
	con, err := h.newDB()
	if err != nil {
		return
	}
	q := fmt.Sprintf("SELECT name FROM tbl_hospital WHERE hospital_id=%d and enabled=true", h.ID)
	if err = con.QueryRow(q).Scan(&h.Name); err != nil {
		err = errors.ErrNotFoundHosp.Wrapf(err, "%s, h.ID:%d", prov.ErrMsg(), h.ID)
		return
	}

	// APIの場合、連携許可を確認
	if prov.IsAPI() {
		var provID int64
		if err = con.QueryRow("SELECT provider_id FROM tbl_provider WHERE provider_id=$1", prov.ID).Scan(&provID); err != nil {
			err = errors.ErrNotFoundHosp.Wrapf(err, "%s, h.ID:%d", prov.ErrMsg(), id)
			return
		}
	}

	h.EncID = encryptHospID(h.ID)
	h.errMsg = fmt.Sprintf("%s, h.ID:%d, h.EncID:%s", prov.ErrMsg(), h.ID, h.EncID)
	h.cfg = &conf.Get().Hosp // T.B.D クリニックごとに設定できるように
	h.userMngr = user.New(h.ID, h.cfg, h.errMsg)
	h.eventMngr = event.New(h.ID, h.errMsg)
	return
}

// NewCredential : 認証済み状態で病院情報を生成する。
//  prov : プロバイダー
//  credential : クレデンシャル情報
func NewCredential(prov *prov.Prov, credential string) (h *Hosp, err error) {
	ss := session.New()
	defer ss.Close()

	info, err := ss.Get(credential)
	if err != nil {
		return
	}
	if h, err = New(prov, info.HospID); err != nil {
		return
	}
	h.userMngr = user.New(h.ID, h.cfg, h.errMsg)
	if _, err = h.userMngr.LoginByID(info.UserID); err != nil {
		return
	}
	if err = ss.Update(credential, nil, h.Prov.IsPKI()); err != nil {
		return
	}
	h.cfg = &conf.Get().Hosp // T.B.D

	return
}

func (h *Hosp) sessionInfo() *session.Info {
	var userID int64
	if h.userMngr.Logined() != nil {
		userID = h.userMngr.Logined().ID
	}
	return &session.Info{
		ProvID: h.Prov.ID,
		HospID: h.ID,
		UserID: userID,
	}
}

// Auth : ユーザーを認証する。
func (h *Hosp) Auth(number, password string) (credential string, err error) {
	if _, err = h.userMngr.Login(number, password); err != nil {
		return
	}

	// クレデンシャル情報を返却
	ss := session.New()
	defer ss.Close()
	if credential, err = ss.Add(h.sessionInfo(), h.Prov.IsPKI()); err != nil {
		return
	}

	return
}

// UserMngr : ユーザー情報を取得する。
func (h *Hosp) UserMngr() *user.Manager {
	return h.userMngr
}

// PatientMngr : 患者情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) PatientMngr() *patient.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.patientMngr == nil {
		h.patientMngr = patient.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.patientMngr
}

// FloorMngr : フロア情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) FloorMngr() *floor.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.floorMngr == nil {
		h.floorMngr = floor.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.floorMngr
}

// ForumMngr : フォーラム情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) ForumMngr() *forum.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.forumMngr == nil {
		h.forumMngr = forum.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.forumMngr
}

// InchgMngr : インチャージ食事情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) InchgMngr() *inchg.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.inchgMngr == nil {
		h.inchgMngr = inchg.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.inchgMngr
}

// RecordMngr : 記録情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) RecordMngr() *record.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.recordMngr == nil {
		h.recordMngr = record.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.recordMngr
}

// ConfMngr : 設定情報を取得する（未ログインの場合はpanic）。
func (h *Hosp) ConfMngr() *hconf.Manager {
	lu := h.userMngr.Logined()
	if lu == nil {
		panic(errors.ErrNotAuthAccount.New(h.errMsg))
	}
	if h.confMngr == nil {
		h.confMngr = hconf.New(h.ID, lu.ID, lu.FamilyName, lu.FirstName, h.cfg, lu.DebugMsg())
	}
	return h.confMngr
}
