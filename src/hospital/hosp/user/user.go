package user

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	"hospital/util"
	"strconv"
	"strings"
)

// Manager : ユーザー管理用構造体
type Manager struct {
	util.Manager
	logined User
}

// New : ユーザー管理用構造体を生成する。
func New(hospID int64, cfg *conf.HospConfig, debugMsg string) *Manager {
	m := &Manager{}
	m.Manager = util.NewManager(hospID, 0, "", "", cfg, debugMsg)
	return m
}

// User : ユーザー情報構造体
type User struct {
	m              *Manager
	ID             int64         `json:"-"`                                                           // ユーザーID（整数）
	EncID          string        `json:"id"`                                                          // ユーザーID
	Account        string        `json:"account" validator:"required,ascii,max=10"`                   // アカウント
	Password       string        `json:"password,omitempty" validator:"omitempty,ascii,min=5,max=32"` // パスワード
	Order          int           `json:"order" validator:"omitempty,min=0,max=999"`                   // 順番
	OrcaID         db.NullString `json:"orcaID" validator:"omitempty,ascii,max=10"`                   // ORCA ID
	CategoryID     int           `json:"categoryID" validator:"omitempty,min=1"`                      // ユーザーカテゴリー
	RoleID         int64         `json:"roleID" validate:"required"`                                  // ロールID
	FamilyName     string        `json:"family_name" validator:"required,max=30"`                     // 苗字
	FirstName      string        `json:"first_name" validator:"required,max=30"`                      // 名前
	FamilyNameKana string        `json:"family_name_kana" validator:"required,max=30"`                // 苗字（カナ）
	FirstNameKana  string        `json:"first_name_kana" validator:"required,max=30"`                 // 名前（カナ）
	Gender         int           `json:"gender" validator:"min=0,max=2"`                              // 性別
	UserGroupIDs   []int         `json:"userGroupIDs"`                                                // 所属するユーザーグループ
	ForumIDs       []int         `json:"forumIDs"`                                                    // アクセスできる掲示板
	Locked         bool          `json:"locked"`                                                      // ロック状態
	Enabled        bool          `json:"enabled"`                                                     // 有効無効
	Modified       db.NullTime   `json:"modified"`                                                    // 更新日時
	Role           struct {
		Name               string     `json:"name"`               // ロール名
		PermitUser         PermitType `json:"permitUser"`         // ユーザー管理などの権限
		PermitPartnerLogin PermitType `json:"permitPertnerLogin"` // 提携先へのログイン権限
	} `json:"role"` // ロール
	pwdMac []byte
}

// DebugMsg : デバッグメッセージを取得する。
func (u *User) DebugMsg() string {
	return u.m.DebugMsg()
}

// LoginByID : ログイン済みユーザー情報を生成する。
func (m *Manager) LoginByID(id int64) (u *User, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	u = &User{m: m, ID: id}
	q := fmt.Sprintf("SELECT "+
		"tbl_user.user_id,"+
		"account,"+
		"password,"+
		"category_id,"+
		"family_name,"+
		"first_name,"+
		"tbl_role.role_id,"+
		"tbl_role.name,"+
		"tbl_role.permit_user,"+
		"tbl_role.permit_partner_login "+
		"FROM tbl_user "+
		"INNER JOIN tbl_role ON tbl_role.role_id=tbl_user.role_id WHERE tbl_user.user_id=%d AND tbl_user.enabled=true", u.ID)
	if err = con.QueryRow(q).Scan(&u.ID, &u.Account, &u.pwdMac, &u.CategoryID, &u.FamilyName, &u.FirstName,
		&u.RoleID, &u.Role.Name, &u.Role.PermitUser, &u.Role.PermitPartnerLogin); err != nil {
		err = errors.ErrNotAuthAccount.Wrapf(err, "%s, userID:%d", m.DebugMsg(), u.ID)
		return
	}

	u.EncID = m.EncryptID(u.ID)
	m.SetDebugMsg(fmt.Sprintf("%s, lu.ID=%d, lu.EncID=%s, lu.Account:%s", m.DebugMsg(), u.ID, u.EncID, u.Account))
	m.logined = *u
	return
}

// Login : ログインしてユーザー情報を生成する。
func (m *Manager) Login(account, password string) (u *User, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	// ユーザーを検索
	var userID int64
	var failCount int
	var pwdMac []byte
	q := "SELECT user_id,password,fail_count FROM tbl_user WHERE account=$1 AND user_id>0 AND enabled=true"
	if err = con.QueryRow(q, fmt.Sprintf("%05d%s", m.HospID(), account)).Scan(&userID, &pwdMac, &failCount); err != nil {
		err = errors.ErrNotAuthAccount.Wrapf(err, "%s, account:%s", m.DebugMsg(), account)
		return
	}

	if failCount > m.Conf().User.MaxFailCount {
		err = errors.ErrNotAuthLocked.Errorf("%s, account:%s", m.DebugMsg(), account)
		return
	}

	// パスワード認証
	account = fmt.Sprintf("%05d%s", m.HospID(), account)
	mac := hmac.New(sha256.New, []byte(account))
	mac.Write([]byte(password))
	calMac := mac.Sum(nil)
	if hmac.Equal(pwdMac, calMac) {
		if failCount > 0 {
			q = fmt.Sprintf("UPDATE tbl_user SET fail_count=0 WHERE id=%d", userID)
			if _, err = con.Exec(q); err != nil {
				err = errors.ErrFatal.Wrapf(err, "%s, account:%s", m.DebugMsg(), account)
			}
		}
		u, err = m.LoginByID(userID)
		return
	}

	// 認証失敗時に失敗回数をインクリメント
	q = fmt.Sprintf("UPDATE tbl_user SET fail_count=fail_count+1 WHERE user_id=%d", userID)
	if _, err = con.Exec(q); err == nil {
		err = errors.ErrNotAuthPassword.Errorf("%s, account:%s", m.DebugMsg(), account)
	} else {
		err = errors.ErrFatal.Wrapf(err, "%s, account:%s", m.DebugMsg(), account)
	}
	return
}

// Logined : ログイン済みユーザーを取得する。
func (m *Manager) Logined() (u *User) {
	if m.logined.ID != 0 {
		u = &m.logined
	}
	return
}

// Search : ユーザー検索パラメーター
type Search struct {
	EncUserID   string `query:"userID"`      // ユーザーID。指定された場合はAndDisabled、Nameなどのパラメーターは無視。
	Offset      int    `query:"offset"`      // オフセット値
	Limit       int    `query:"limit"`       // 最大結果数（0以下の場合はデフォルト値、最大値以上の場合は最大値に強制変更）
	AndInfo     bool   `query:"andInfo"`     // 所属グループやアクセスできる掲示板なども取得する場合はtrue
	AndDisabled bool   `query:"andDisabled"` // Disabled状態のユーザーも検索対象とする場合はtrue。EncUserID!=""の場合はtrueとして扱う。
	Name        string `query:"name"`        // 検索キー（氏名）
	Category    string `query:"category"`    // 検索キー（カテゴリー）
}

// Search : ユーザーを検索する。
func (m *Manager) Search(srh *Search) (us []*User, err error) {
	if !m.logined.HasPermitUser(PermitReadOnly) {
		err = errors.ErrNotPermit.New(m.DebugMsg())
		return
	}

	if srh == nil {
		srh = &Search{}
	}
	if srh.Limit <= 0 {
		srh.Limit = m.Conf().User.DefSearchCount
	}
	if srh.Limit > m.Conf().User.MaxSearchCount {
		srh.Limit = m.Conf().User.MaxSearchCount
	}

	// 検索条件を設定
	var q strings.Builder
	q.WriteString("SELECT " +
		"tbl_user.user_id," +
		"account," +
		"tbl_user.order_no," +
		"orca_id," +
		"family_name," +
		"first_name," +
		"family_name_kana," +
		"first_name_kana," +
		"gender," +
		"fail_count," +
		"enabled," +
		"modified," +
		"tbl_role.role_id," +
		"tbl_role.name " +
		"FROM tbl_user " +
		"INNER JOIN tbl_role ON tbl_role.role_id=tbl_user.role_id " +
		"WHERE tbl_user.user_id>0")
	i := 0
	args := make([]interface{}, 0)
	if srh.EncUserID != "" {
		i++
		q.WriteString(fmt.Sprintf(" AND user_id=$%d AND enabled=true", i))
		args = append(args, m.DecryptID(srh.EncUserID))
	} else {
		if !srh.AndDisabled {
			q.WriteString(fmt.Sprintf(" AND enabled=true"))
		}
		if srh.Name != "" {
			i++
			q.WriteString(fmt.Sprintf(" AND (family_name LIKE %%$%d%% OR first_name LIKE %%%d%% OR family_name_kana LIKE %%$%d%% OR first_name_kana LIKE %%$%d%%)", i, i, i, i))
			args = append(args, srh.Name)
		}
		if srh.Category != "" {
			i++
			q.WriteString(fmt.Sprintf(" AND category=$%d", i))
			args = append(args, srh.Category)
		}
		q.WriteString(fmt.Sprintf(" ORDER BY enabled DESC,tbl_user.order_no DESC,tbl_user.user_id OFFSET %d LIMIT %d", srh.Offset, srh.Limit))
	}

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}
	rows, err := con.Query(q.String(), args...)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}
	defer rows.Close()

	// 検索結果を作成
	us = make([]*User, 0, 50)
	var sb strings.Builder
	for rows.Next() {
		u := &User{m: m}
		var failCount int
		if err = rows.Scan(&u.ID, &u.Account, &u.Order, &u.OrcaID, &u.FamilyName, &u.FirstName, &u.FamilyNameKana, &u.FirstNameKana, &u.Gender, &failCount, &u.Enabled, &u.Modified, &u.RoleID, &u.Role.Name); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		u.EncID = m.EncryptID(u.ID)
		u.Locked = failCount >= m.Conf().User.MaxFailCount
		us = append(us, u)

		sb.WriteString(strconv.FormatInt(u.ID, 10))
		sb.WriteString(",")
	}

	// 所属するユーザーグループ
	if !srh.AndInfo {
		return
	}

	quids := strings.TrimSuffix(sb.String(), ",")
	if rows, err = con.Query("SELECT usergroup_id,user_id FROM tbl_usergroup_member WHERE user_id IN (" + quids + ")"); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
		return
	}
	defer rows.Close()

	gidsMap := map[int64][]int{}
	for rows.Next() {
		var gid int
		var userID int64
		if err = rows.Scan(&gid, &userID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		gids, ok := gidsMap[userID]
		if !ok {
			gids = make([]int, 0)
			gidsMap[userID] = gids
		}
		gids = append(gids, gid)
	}
	for _, u := range us {
		if gids, ok := gidsMap[u.ID]; ok {
			u.UserGroupIDs = gids
		}
	}

	// アクセスできる掲示板一覧
	if rows, err = con.Query("SELECT forum_id,user_id FROM user_id WHERE user_id IN (" + quids + ")"); err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
		return
	}
	defer rows.Close()

	fidsMap := map[int64][]int{}
	for rows.Next() {
		var fid int
		var userID int64
		if err = rows.Scan(&fid, &userID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		fids, ok := fidsMap[userID]
		if !ok {
			fids = make([]int, 0)
			fidsMap[userID] = fids
		}
		fids = append(fids, fid)
	}
	for _, u := range us {
		if fids, ok := fidsMap[u.ID]; ok {
			u.ForumIDs = fids
		}
	}

	return
}

// Add : ユーザーを追加する。
func (m *Manager) Add(us []*User) (err error) {
	// Bulk insert
	var sb strings.Builder
	sb.WriteString("INSERT INTO tbl_user (" +
		"account," +
		"order_no," +
		"category_id," +
		"role_id," +
		"family_name," +
		"first_name," +
		"family_name_kana," +
		"first_name_kana," +
		"gender," +
		"password" +
		") VALUES ")
	count := 6
	size := len(us) * count
	args := make([]interface{}, 0, size)
	accounts := make([]interface{}, 0, size)
	i := 1
	var account string
	for _, u := range us {
		sb.WriteString(fmt.Sprintf("($%d,%d,%d,%d,$%d,$%d,$%d,$%d,%d,$%d),", i, u.Order, u.CategoryID, u.RoleID, i+1, i+2, i+3, i+4, u.Gender, i+5))
		account = fmt.Sprintf("%05d%s", m.HospID(), u.Account)
		mac := hmac.New(sha256.New, []byte(account))
		mac.Write([]byte(u.Password))
		pwdMac := mac.Sum(nil)
		args = append(args, account, u.FamilyName, u.FirstName, u.FamilyNameKana, u.FirstNameKana, pwdMac)
		accounts = append(accounts, account)
		i += count
	}

	txm, err := m.NewTx()
//	if err != nil {
//		return
//	}
//	defer txm.Rollback()

//	if _, err = txm.Tx.Exec("LOCK TABLE tbl_user IN EXCLUSIVE MODE"); err != nil {
//		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
//		return
//	}
	q := strings.TrimSuffix(sb.String(), ",")
	if _, err = txm.Tx.Exec(q, args...); err != nil {
		err = errors.ErrAlreadyExistUser.Wrapf(err, "%s, lastAccount:%s", m.DebugMsg(), account)
		return
	}

	// すべての掲示板にアクセス可
//	sb.Reset()/
//	sb.WriteString("INSERT INTO tbl_forum_acl (forum_id,user_id) SELECT -1 AS forum_id, user_id AS user_id from tbl_user WHERE account IN (")
//	for i := range accounts {/
//		sb.WriteString(fmt.Sprintf("$%d,", i+1))
//	}
//	q = strings.TrimSuffix(sb.String(), ",") + ")"
//	if _, err = txm.Tx.Exec(q, accounts...); err != nil {
//		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
//		return
//	}

	txm.Commit()
fmt.Println("zzzz")
	return
}

// Disabled :
func (m *Manager) Disabled(encUserID string, ok bool) (err error) {
	// T.B.D
	// tbl_forum_readクリア（連携ホスピタルも）
	// tbl_forum_confirmクリア（連携ホスピタルも）
	return
}

// Delete :
func (m *Manager) Delete(encUserID string) (err error) {
	// T.B.D
	// tbl_forum_readクリア（連携ホスピタルも）
	// tbl_forum_confirmクリア（連携ホスピタルも）
	return
}
