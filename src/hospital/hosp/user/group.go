package user

import (
	"hospital/errors"
)

// Group : ユーザーグループ情報
type Group struct {
	ID    int64  `json:"id"`
	Name  string `json:"name" validate:"required,max=32"`
	Order int    `json:"order" validate:"omitempty,min=0,max=999"`
}

// Groups : ユーザーグループ一覧を取得する。
func (m *Manager) Groups() (gs []*Group, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	rows, err := con.Query("SELECT id,name,order_no FROM tbl_usergroup ORDER BY order_no DESC,id")
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
		return
	}

	gs = make([]*Group, 0)
	for rows.Next() {
		g := &Group{}
		if err = rows.Scan(&g.ID, &g.Name, &g.Order); err != nil {
			err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
			return
		}
		gs = append(gs, g)
	}

	return
}
