package user

import (
	"hospital/errors"
)

// Category : ユーザーカテゴリ
type Category struct {
	ID    int64  `json:"id"`
	Name  string `json:"name" validate:"required,max=32"`
	Order int    `json:"order" validate:"omitempty,min=0,max=999"`
}

// Categorys : カテゴリ一覧を取得する。
func (m *Manager) Categorys() (cs []*Category, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	rows, err := con.Query("SELECT id,name,order_no FROM tbl_category ORDER BY order_no DESC,id")
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
		return
	}

	cs = make([]*Category, 0)
	for rows.Next() {
		c := &Category{}
		if err = rows.Scan(&c.ID, &c.Name, &c.Order); err != nil {
			err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
			return
		}
		cs = append(cs, c)
	}

	return
}
