package user

import (
	"fmt"
	"hospital/errors"
	"strings"
)

// Forum : 掲示板情報
type Forum struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	UnreadCount int    `json:"unreadCount"`
}

// Forums : アクセスできる掲示板の一覧を取得する。
func (u *User) Forums() (fs []*Forum, err error) {
	errMsg := func() string {
		return fmt.Sprintf("%s, u.ID:%d u.Account:%s", u.m.DebugMsg(), u.ID, u.Account)
	}

	// 権限チェック
	if u.ID != u.m.logined.ID && !u.m.logined.HasPermitUser(PermitReadOnly) {
		err = errors.ErrNotPermit.New(errMsg())
		return
	}

	con, err := u.m.NewDB()
	if err != nil {
		return
	}

	// ユーザーの全掲示板アクセス可否を確認
	// T.B.D(ひとつのSQLに)
	var forumID int64
	q := fmt.Sprintf("SELECT forum_id FROM tbl_forum_acl where user_id=%d limit 1", u.ID)
	if err = con.QueryRow(q).Scan(&forumID); err != nil {
		err = errors.ErrFatal.Wrap(err, errMsg())
		return
	}

	q = "SELECT tbl_forum.forum_id,tbl_forum.name,COALESCE(r.unread_count, 0) AS unread_count FROM tbl_forum "
	if forumID == -1 {
		q += fmt.Sprintf("LEFT JOIN (SELECT forum_id,COUNT(msg_id) AS unread_count FROM tbl_forum_read WHERE user_id=%d GROUP BY forum_id) AS r ON TRUE "+
			"WHERE tbl_forum.forum_id>=0 AND ", u.ID)
	} else {
		q += fmt.Sprintf("INNER JOIN tbl_forum_acl ON tbl_forum_acl.forum_id=tbl_forum.forum_id "+
			"LEFT JOIN (SELECT forum_id,COUNT(msg_id) AS unread_count,user_id FROM tbl_forum_read GROUP BY forum_id,user_id) AS r on r.user_id=tbl_forum_acl.user_id "+
			"WHERE tbl_forum_acl.user_id=%d AND ", u.ID)
	}
	q += " tbl_forum.enabled=true ORDER BY tbl_forum.order_no DESC,tbl_forum.forum_id"
	rows, err := con.Query(q)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, errMsg())
		return
	}
	fs = make([]*Forum, 0)
	for rows.Next() {
		f := &Forum{}
		if err = rows.Scan(&f.ID, &f.Name, &f.UnreadCount); err != nil {
			err = errors.ErrFatal.Wrap(err, errMsg())
			return
		}
		fs = append(fs, f)
	}

	return
}

// UpdateForums : アクセスできる掲示板を更新する。
func (u *User) UpdateForums(forumIDs []int64) (err error) {
	debugMsg := func() string {
		return fmt.Sprintf("%s, u.ID:%d u.Account:%s", u.m.DebugMsg(), u.ID, u.Account)
	}

	// 権限チェック
	if !u.m.logined.HasPermitUser(PermitEdit) {
		err = errors.ErrNotPermit.New(debugMsg())
		return
	}

	if len(forumIDs) == 0 {
		err = errors.ErrInvalidParams.Errorf("%s, len(forumIDs):0", debugMsg())
		return
	}

	txm, err := u.m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	// 掲示板ACLを全削除
	q := fmt.Sprintf("DELETE FROM tbl_forum_acl WHERE user_id=%d", u.ID)
	if _, err = txm.Tx.Exec(q); err != nil {
		err = errors.ErrFatal.Wrap(err, debugMsg())
		return
	}

	// 掲示板ACLを追加
	var sb strings.Builder
	for _, forumID := range forumIDs {
		if forumID == -1 {
			sb.Reset()
			sb.WriteString(fmt.Sprintf("(-1,%d),", u.ID))
			break
		}
		sb.WriteString(fmt.Sprintf("(%d,%d),", forumID, u.ID))
	}
	q = "INSERT INTO tbl_forum_acl (forum_id,user_id) VALUES " + strings.TrimSuffix(sb.String(), ",")
	if _, err = txm.Tx.Exec(q); err != nil {
		err = errors.ErrNotFoundForumOrUser.Wrap(err, debugMsg())
		return
	}

	txm.Commit()

	return
}
