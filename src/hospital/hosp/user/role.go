package user

import (
	"hospital/errors"
)

// ロールの種類
const (
	RoleGeneral = 1
	RoleAdmin   = 2
)

// PermitType : 権限タイプ
type PermitType int

// 権限タイプの種類
const (
	PermitNone     PermitType = 0
	PermitReadOnly PermitType = 1
	PermitEdit     PermitType = 3
)

// Role : ロール情報
type Role struct {
	ID                 int64  `json:"id"`
	Name               string `json:"name" validate:"required,max=32"`
	Order              int    `json:"order" validate:"omitempty,min=0,max=999"`
	PermitUser         int    `json:"permitUser" validate:"min=0,max=2"`
	PermitPartnerLogin int    `json:"permitPartnerLogin" validate:"min=0,max=2"`
}

// Roles : ロール一覧を取得する。
func (m *Manager) Roles() (rs []*Role, err error) {
	con, err := m.NewDB()
	if err != nil {
		return
	}

	rows, err := con.Query("SELECT id,name,order_no,permit_user,permit_partner_login FROM tbl_role ORDER BY order_no DESC,id")
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
		return
	}

	rs = make([]*Role, 0)
	for rows.Next() {
		r := &Role{}
		if err = rows.Scan(&r.ID, &r.Name, &r.Order, &r.PermitUser); err != nil {
			err = errors.ErrFatal.Wrapf(err, m.DebugMsg())
			return
		}
		rs = append(rs, r)
	}

	return
}

// HasPermitUser : ユーザー管理関連の権限有無を確認する。
func (u *User) HasPermitUser(pt PermitType) bool {
	return u.Role.PermitUser&pt != 0
}

// HasPermitPartnerLogin : 提携先ログインの権限有無を確認する。
func (u *User) HasPermitPartnerLogin(pt PermitType) bool {
	return u.Role.PermitPartnerLogin&pt != 0
}
