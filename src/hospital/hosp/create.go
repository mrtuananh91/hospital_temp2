package hosp

import (
	"fmt"
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	"hospital/hosp/user"
	"hospital/prov"
)

// Create : 病院を構築する。
func Create(h *Hosp, admin *user.User) (err error) {
	// DB生成
	con, err := db.New(db.CreateDB)
	if err != nil {
		return
	}

	if _, err = con.Exec(fmt.Sprintf("CREATE DATABASE db_%d ENCODING='UTF8' LC_COLLATE='C' LC_CTYPE='C'", h.ID)); err != nil {
		err = errors.ErrFatal.Wrapf(err, "id:%d, name:%s", h.ID, h.Name)
		return
	}

	// テーブル作成
	if con, err = db.New(h.ID); err != nil {
		return
	}

	q := fmt.Sprintf(conf.Get().DB.CreateSQL, h.ID)
	if _, err = con.Exec(q); err != nil {
		err = errors.ErrFatal.Wrapf(err, "id:%d, name:%s", h.ID, h.Name)
		return
	}

	q = fmt.Sprintf("INSERT INTO tbl_hospital (hospital_id,name,zip_code,address,tel,fax) VALUES (%d,$1,$2,$3,$4,$5)", h.ID)
	if _, err = con.Exec(q, h.Name, h.ZipCode, h.Address, h.Tel, h.Fax); err != nil {
		err = errors.ErrFatal.Wrapf(err, "id:%d, name:%s", h.ID, h.Name)
		return
	}

	// 最初のユーザーを作成
	newHosp, err := New(prov.NewPKI(), h.ID)
	if err != nil {
		return
	}
	if err = newHosp.UserMngr().Add([]*user.User{admin}); err != nil {
		return
	}

	return
}

// Delete : 病院を削除する（T.B.D 他マシンのプロセスでコネクションがあるとエラーになる可能性あり）。
func Delete(hospID int64) (err error) {
	// DB削除
	db.Close(hospID)

	con, err := db.New(db.CreateDB)
	if err != nil {
		return
	}

	if _, err = con.Exec(fmt.Sprintf("drop database db_%d", hospID)); err != nil {
		err = errors.ErrFatal.Wrapf(err, "id:%d", hospID)
		return
	}

	return
}
