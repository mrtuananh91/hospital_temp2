package inchg

import (
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"hospital/util"
)

// Manager : インチャージ管理用構造体
type Manager struct {
	util.Manager
}

// New : インチャージ管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}

// UpdateInchg : インチャージ更新パラメーター
type UpdateInchg struct {
	//m				*Manager
	OrderActID int64 `json:"order_act_id"`
	StatusNo   int64 `json:"status_no"`
	Checked    bool  `json:"checked"`
}

// UpdateInchgStatus : インチャージの実施を更新する。
func (m *Manager) UpdateInchgStatus(val *UpdateInchg) (err error) {

	fmt.Println("[Higuchi][inchg.go]UpdateInchgStatus()")

	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_order_act IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}

	var q string

	if val.Checked {
		q = fmt.Sprintf("UPDATE tbl_order_act set act_status%d = %t, act_user%d = '%s', act_datetime%d = current_timestamp where order_act_id = %d", val.StatusNo, val.Checked, val.StatusNo, m.LoginFamilyName()+m.LoginFirstName(), val.StatusNo, val.OrderActID)
	} else {
		q = fmt.Sprintf("UPDATE tbl_order_act set act_status%d = %t, act_user%d = %s, act_datetime%d = null where order_act_id = %d", val.StatusNo, val.Checked, val.StatusNo, "null", val.StatusNo, val.OrderActID)
	}

	fmt.Println("[Higuchi][inchgmed.go]UpdateInchgMedStatus()sql == " + q) //デバッグ用

	if _, err = txm.Tx.Exec(q); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}

	txm.Commit()

	return
}
