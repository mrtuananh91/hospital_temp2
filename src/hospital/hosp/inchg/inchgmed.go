package inchg

import (
	"database/sql"
	"fmt"
	"hospital/errors"
	"strings"
)

// InchgMed : インチャージ薬情報構造体
type InchgMed struct {
	m               *Manager
	OrderActID      int64   `json:"order_act_id"`
	OrderID         int64   `json:"order_id"`
	PatientID       int64   `json:"patient_id"`
	OrderDatePeriod int64   `json:"order_date_period"`
	ActStatus1      bool    `json:"act_status1"`
	ActStatus2      bool    `json:"act_status2"`
	ActUser1        *string `json:"act_user1"`
	ActUser2        *string `json:"act_user2"`
	ActDateTime1    *string `json:"act_datetime1"`
	ActDateTime2    *string `json:"act_datetime2"`
	OrcaID          int64   `json:"orca_id"`
	FamilyName      string  `json:"family_name"`
	FirstName       string  `json:"first_name"`
	Order           string  `json:"order"`
}

// SearchInchgMed : インチャージ薬検索パラメーター
type SearchInchgMed struct {
	OrderDate string `query:"order_date"`
}

// SearchInchgMed : インチャージ薬を検索する。
func (m *Manager) SearchInchgMed(srh *SearchInchgMed) (ret []*InchgMed, err error) {

	//fmt.Println("[Higuchi][inchgmed.go]Search()")	//デバッグ用

	if srh == nil {
		srh = &SearchInchgMed{}
	}

	// 検索条件を設定
	var q strings.Builder
	q.WriteString(
		"SELECT " +
			"order_act_id," +
			"order_id," +
			"patient_id," +
			"order_date_period," +
			"act_status1," +
			"act_status2," +
			"act_user1," +
			"act_user2," +
			"to_char(act_datetime1, 'YYYY-MM-DD HH24:MI')," +
			"to_char(act_datetime2, 'YYYY-MM-DD HH24:MI')," +
			"orca_id," +
			"family_name," +
			"first_name ")

	q.WriteString(" FROM tbl_order_act JOIN tbl_patient USING (patient_id) WHERE order_type = 1 ")

	args := make([]interface{}, 0)

	i := 1 //1始まりでないとバインドするときエラーになる

	//日付
	q.WriteString(fmt.Sprintf(" AND order_date=$%d", i))
	args = append(args, srh.OrderDate)
	i++

	//並び順
	q.WriteString(" ORDER BY order_act_id")

	//fmt.Println("[Higuchi][inchgmed.go]q.String() == " + q.String())	//デバッグ用
	//fmt.Printf("[Higuchi][inchgmed.go]args == (%%#v) %#v\n", args)	//デバッグ用

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}

	var rows *sql.Rows
	rows, err = con.Query(q.String(), args...)

	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}

	defer rows.Close()

	// 検索結果を作成
	ret = make([]*InchgMed, 0, 50)
	i = 1
	for rows.Next() {
		fmt.Printf("[Higuchi][inchgmed.go]取得データ%d番目\n", i)
		i++
		tmp := &InchgMed{m: m}
		err = rows.Scan(
			&tmp.OrderActID,
			&tmp.OrderID,
			&tmp.PatientID,
			&tmp.OrderDatePeriod,
			&tmp.ActStatus1,
			&tmp.ActStatus2,
			&tmp.ActUser1,
			&tmp.ActUser2,
			&tmp.ActDateTime1,
			&tmp.ActDateTime2,
			&tmp.OrcaID,
			&tmp.FamilyName,
			&tmp.FirstName,
		)

		fmt.Printf("(%%#v) %#v\n", tmp)

		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}

		//取得したデータのOrderDatePeriodごとにデータを取得するために使う配列
		//朝に含まれる服薬
		morningCode := []int{50, 11, 12, 42}
		morningName := []string{"【起床時】", "【朝食前】", "【朝食後】", "【食間（朝昼）】"}
		//昼に含まれる服薬
		afternoonCode := []int{21, 22, 42}
		afternoonName := []string{"【昼食前】", "【昼食後】", "【食間（昼夕）】"}
		//夕に含まれる服薬
		eveningCode := []int{31, 32}
		eveningName := []string{"【夕食前】", "【夕食後】"}
		//眠前に含まれる服薬
		bedtimeCode := []int{60}
		bedtimeName := []string{"【眠前】"}
		//その他に含まれる服薬
		etcCode := []int{70}
		etcName := []string{"【時間指定】"}

		switch tmp.OrderDatePeriod {
		case 1:
			tmp.Order, err = m.getMedicenName(tmp.OrderID, morningCode, morningName)
		case 2:
			tmp.Order, err = m.getMedicenName(tmp.OrderID, afternoonCode, afternoonName)
		case 3:
			tmp.Order, err = m.getMedicenName(tmp.OrderID, eveningCode, eveningName)
		case 4:
			tmp.Order, err = m.getMedicenName(tmp.OrderID, bedtimeCode, bedtimeName)
		case 5:
			tmp.Order, err = m.getMedicenName(tmp.OrderID, etcCode, etcName)
		}

		fmt.Println("[Higuchi: inchgmed.go]tmp.Order == " + tmp.Order)

		ret = append(ret, tmp)
	}

	//for i := 0; i < len(ret); i++ {fmt.Printf("[Higuchi: inchgmed.go]inchgmed.goのSearchの戻り値：(%%#v) %#v\n", ret[i])}	//デバッグ用

	return
}

//朝、昼、夜、眠前などの区分ごとにtbl_order_medからデータを取得し、薬剤に「朝食前」「朝食後」などの服薬時間をつけて返す
func (m *Manager) getMedicenName(OrderID int64, code []int, name []string) (ret string, err error) {

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}

	var rows *sql.Rows
	var q string
	var medicineName string
	for i := 0; i < len(code); i++ {

		q = fmt.Sprintf("SELECT medicine_name from tbl_order_med where order_id = %d and date_period = %d order by order_med_id", OrderID, code[i])

		//fmt.Println("[Higuchi][inchgmed.go]getMedicenName()q==" + q)	//デバッグ用

		rows, err = con.Query(q)
		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q, OrderID)
			return
		}
		defer rows.Close()

		medTimeFlg := false
		for rows.Next() {

			//データ取得
			err = rows.Scan(&medicineName)
			if err != nil {
				err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), OrderID)
				return
			}

			//最初だけ「朝食前」などの区分を入れる
			if !medTimeFlg {
				ret += name[i] + "\n"
				medTimeFlg = true
			}

			//薬剤名を改行つきで戻りに追加
			ret += medicineName + "\n"
			fmt.Println("[Higuchi][inchgmed.go]getMedicenName()ret==" + ret)
		}

	}
	return
}
