package inchg

import (
	"database/sql"
	"fmt"
	"hospital/errors"
	"strings"
)

// InchgEat : インチャージ食事情報構造体
type InchgEat struct {
	m            *Manager
	OrderActID   int64   `json:"order_act_id"`
	OrderID      int64   `json:"order_id"`
	PatientID    int64   `json:"patient_id"`
	ActStatus1   bool    `json:"act_status1"`
	ActStatus2   bool    `json:"act_status2"`
	ActUser1     *string `json:"act_user1"`
	ActUser2     *string `json:"act_user2"`
	ActDateTime1 *string `json:"act_datetime1"`
	ActDateTime2 *string `json:"act_datetime2"`
	OrcaID       int64   `json:"orca_id"`
	FamilyName   string  `json:"family_name"`
	FirstName    string  `json:"first_name"`
}

// SearchInchgEat : インチャージ食事検索パラメーター
type SearchInchgEat struct {
	OrderDate string `query:"order_date"`
}

// SearchInchgEat : インチャージ食事を検索する。
func (m *Manager) SearchInchgEat(srh *SearchInchgEat) (ret []*InchgEat, err error) {

	fmt.Println("[Higuchi][inchgeat.go]Search()")

	if srh == nil {
		srh = &SearchInchgEat{}
	}

	// 検索条件を設定
	var q strings.Builder
	q.WriteString(
		"SELECT " +
			"order_act_id," +
			"order_id," +
			"patient_id," +
			"act_status1," +
			"act_status2," +
			"act_user1," +
			"act_user2," +
			"to_char(act_datetime1, 'YYYY-MM-DD HH24:MI')," +
			"to_char(act_datetime2, 'YYYY-MM-DD HH24:MI')," +
			"orca_id," +
			"family_name," +
			"first_name ")

	q.WriteString(" FROM tbl_order_act JOIN tbl_patient USING (patient_id) WHERE order_type = 3 ")

	args := make([]interface{}, 0)

	i := 1 //1始まりでないとバインドするときエラーになる

	//日付
	q.WriteString(fmt.Sprintf(" AND order_date=$%d", i))
	args = append(args, srh.OrderDate)
	i++

	//並び順
	q.WriteString(" ORDER BY order_act_id")

	fmt.Println("[Higuchi][inchgeat.go]q.String() == " + q.String())
	fmt.Printf("[Higuchi][inchgeat.go]args == (%%#v) %#v\n", args)

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}

	var rows *sql.Rows
	rows, err = con.Query(q.String(), args...)

	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}

	defer rows.Close()

	// 検索結果を作成
	ret = make([]*InchgEat, 0, 50)
	i = 1
	for rows.Next() {
		fmt.Printf("[Higuchi][inchgeat.go]取得データ%d番目\n", i)
		i++
		tmp := &InchgEat{m: m}
		err = rows.Scan(
			&tmp.OrderActID,
			&tmp.OrderID,
			&tmp.PatientID,
			&tmp.ActStatus1,
			&tmp.ActStatus2,
			&tmp.ActUser1,
			&tmp.ActUser2,
			&tmp.ActDateTime1,
			&tmp.ActDateTime2,
			&tmp.OrcaID,
			&tmp.FamilyName,
			&tmp.FirstName,
		)

		fmt.Printf("(%%#v) %#v\n", tmp)

		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			return
		}
		ret = append(ret, tmp)
	}

	//for i := 0; i < len(ret); i++ {fmt.Printf("[Higuchi: inchgeat.go]inchgeat.goのSearchの戻り値：(%%#v) %#v\n", ret[i])}	//デバッグ用

	return
}
