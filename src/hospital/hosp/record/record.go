package record

import (
	"hospital/conf"
	"hospital/db"
	"hospital/errors"
	"log"
	"strings"
	"time"

	//"hospital/errors"
	"hospital/util"
)

// Manager : 患者管理用構造体
type Manager struct {
	util.Manager
}

// New : 患者管理情報を生成する。
func New(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) *Manager {
	return &Manager{util.NewManager(hospID, loginUserID, loginFamilyName, loginFirstName, cfg, debugMsg)}
}

type Record struct {
	m            *Manager
	ID           int64       `json:"-"`
	EncID        string      `json:"id"` // recordID
	PatientID    int64       `json:"-"`
	EncPatientID string      `json:"patient_id"`
	H13nID       int64       `json:"-"`
	EncH13nID    string      `json:"h13n_id"`
	RecordType   int32       `json:"record_type" validate:"omitempty,min=1,max=3"`
	UserID       int64       `json:"-"`
	EncUserID    string      `json:"user_id" validate:"required"`
	ActDate      db.NullTime `json:"act_date" validate:"required"`
	ActTime      db.NullTime `json:"act_time" validate:"required"`
	ActDetail    string      `json:"act_detail" validate:"required"`
	RegDatetime  db.NullTime `json:"reg_datetime"`
	Note         *string     `json:"note"`
	UserName     string      `json:"user_name" validate:"required"`
}

type RecordSearch struct {
	EncID        string `json:"id"`
	EncH13nID    string `json:"h13n_id"`
	EncPatientID string `json:"patient_id"`
	RecordType   int    `json:"record_type"`
	Offset       int    `query:"offset"` // オフセット値
	Limit        int    `query:"limit"`  // 最大結果数（-1の場合は制限なし、0と-2以下の場合はデフォルト値、最大値以上の場合は最大値に強制変更）
}

// Search : 看護記録を検索する。
func (m *Manager) Search(srh *RecordSearch) (rcd []*Record, err error) {
	if srh == nil {
		srh = &RecordSearch{}
	}
	log.Print(m.DecryptID("pbf2vg4p2krb"))
	// 検索条件を設定
	var q strings.Builder
	q.WriteString("SELECT " +
		"record_id," +
		"patient_id," +
		"record_type," +
		"user_id," +
		"act_date," +
		"act_time," +
		"act_detail," +
		"reg_datetime," +
		"note," +
		"user_name")

	q.WriteString(" FROM tbl_record")

	args := make([]interface{}, 0)

	q.WriteString(" WHERE record_type=$1 AND h13n_id=$2")
	args = append(args, srh.RecordType, m.DecryptID(srh.EncH13nID))
	q.WriteString(" AND patient_id=$3")
	args = append(args, m.DecryptID(srh.EncPatientID))

	if srh.EncID != "" {
		q.WriteString(" AND record_id=$4")
		args = append(args, m.DecryptID(srh.EncID))
	}

	q.WriteString(" ORDER BY act_date desc, act_time desc")

	// 検索
	con, err := m.NewDB()
	if err != nil {
		return
	}
	rows, err := con.Query(q.String(), args...)
	if err != nil {
		err = errors.ErrFatal.Wrapf(err, "%s, sql:%s, search:%v", m.DebugMsg(), q.String(), *srh)
		return
	}
	defer rows.Close()
	//if srh.Limit == 0 || srh.Limit < -1 {
	//	srh.Limit = m.Conf().Patient.DefSearchCount
	//}
	//if srh.Limit > m.Conf().Patient.MaxSearchCount {
	//	srh.Limit = m.Conf().Patient.MaxSearchCount
	//}

	rcd = make([]*Record, 0, 50)
	for rows.Next() {
		rc := &Record{m: m}

		err = rows.Scan(&rc.ID, &rc.PatientID, &rc.RecordType, &rc.UserID, &rc.ActDate, &rc.ActTime, &rc.ActDetail,
			&rc.RegDatetime, &rc.Note, &rc.UserName)
		if err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, search:%v", m.DebugMsg(), *srh)
			log.Fatal(err)
			return
		}

		rc.EncID = m.EncryptID(rc.ID)
		rc.EncPatientID = m.EncryptID(rc.PatientID)
		rc.EncH13nID = m.EncryptID(rc.H13nID)
		rc.EncUserID = m.EncryptID(rc.UserID)
		rcd = append(rcd, rc)
	}
	return
}

func (m *Manager) Add(rcd []*Record) (err error) {
	q := "INSERT INTO tbl_record (" +
		"patient_id," +
		"h13n_id," +
		"record_type," +
		"user_id," +
		"act_date," +
		"act_time," +
		"act_detail," +
		"note," +
		"user_name)" +
		" VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING record_id"

	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_record IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}

	// T.B.D (Bulk insert)
	for _, rd := range rcd {
		log.Print("\n", m.DecryptID(rd.EncH13nID), "\n")
		if err = txm.Tx.QueryRow(q, m.DecryptID(rd.EncPatientID), m.DecryptID(rd.EncH13nID), rd.RecordType, m.DecryptID(rd.EncUserID), rd.ActDate, rd.ActTime, rd.ActDetail, rd.Note,
			rd.UserName).Scan(&rd.ID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, record_id:%s, reg_user:%s", m.DebugMsg(), rd.ID, rd.UserName)
			return
		}
	}

	txm.Commit()

	return
}

func (m *Manager) Update(rcd []*Record) (err error) {
	q := "UPDATE tbl_record SET (" +
		"user_id," +
		"act_date," +
		"act_time," +
		"act_detail," +
		"note," +
		"user_name," +
		"reg_datetime) =" +
		" ($1,$2,$3,$4,$5,$6,$7)" +
		" WHERE record_id = $8 RETURNING record_id"

	txm, err := m.NewTx()
	if err != nil {
		return
	}
	defer txm.Rollback()

	if _, err = txm.Tx.Exec("LOCK TABLE tbl_record IN EXCLUSIVE MODE"); err != nil {
		err = errors.ErrFatal.Wrap(err, m.DebugMsg())
		return
	}
	// T.B.D (Bulk insert)
	for _, rd := range rcd {
		if err = txm.Tx.QueryRow(q, m.DecryptID(rd.EncUserID), rd.ActDate, rd.ActTime, rd.ActDetail, rd.Note,
			rd.UserName, time.Now(), m.DecryptID(rd.EncID)).Scan(&rd.ID); err != nil {
			err = errors.ErrFatal.Wrapf(err, "%s, record_id:%s, reg_user:%s", m.DebugMsg(), rd.ID, rd.UserName)
			log.Print(err, q)
			log.Print("\n", "userid:", m.DecryptID(rd.EncUserID), "actdate:", rd.ActDate, "acttime:", rd.ActTime, "actdetail:", rd.ActDetail, "note:", rd.Note,
				"username:", rd.UserName, "regdatetime:", time.Now(), "recordid:", m.DecryptID(rd.EncID))
			return
		}
	}

	txm.Commit()

	return
}
