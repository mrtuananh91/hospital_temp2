package session

import (
	"crypto/aes"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"net"
	"time"

	"github.com/gomodule/redigo/redis"
)

var _pool *redis.Pool
var _chanSession chan []byte
var _key = []byte{
	0x74, 0x44, 0x60, 0xef, 0x93, 0x67, 0xa1, 0xbc,
	0x90, 0x60, 0xc4, 0xa5, 0xf6, 0x67, 0x71, 0xd8,
}

// Init : 初期化する。
func Init() {
	// Redisのコネクションプール
	cfg := conf.Get()
	_pool = &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%s:%d", cfg.Redis.Host, cfg.Redis.Port))
		},
		MaxIdle:     cfg.Redis.MaxIdle,
		MaxActive:   cfg.Redis.MaxActive,
		IdleTimeout: cfg.Redis.IdleTimeout * time.Second,
		Wait:        cfg.Redis.Wait,
	}

	// セッションID生成
	_chanSession = make(chan []byte)
	go func() {
		// シード（time_t）
		var seed = uint64(time.Now().UnixNano())

		// シード（IPアドレス）
		addrs, err := net.InterfaceAddrs()
		if err != nil {
			panic(err)
		}
		var ip4 net.IP
		for _, addr := range addrs {
			if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
				ip4 = ipnet.IP.To4()
				if ip4 != nil {
					break
				}
			}
		}
		if ip4 == nil {
			panic(addrs)
		}

		cipher, err := aes.NewCipher(_key)
		if err != nil {
			panic(err)
		}

		for {
			b := make([]byte, 16)
			seed++
			binary.BigEndian.PutUint64(b, seed)
			b[8] = ip4[0]
			b[9] = ip4[1]
			b[10] = ip4[2]
			b[11] = ip4[3]
			cipher.Encrypt(b, b)
			_chanSession <- b
		}
	}()
}

// Conn : セッション
type Conn struct {
	con redis.Conn
}

// Info : セッション情報
type Info struct {
	ProvID int64 `json:"provID"`
	HospID int64 `json:"hospID"`
	UserID int64 `json:"userID"`
}

func (info *Info) header() string {
	return fmt.Sprintf("%d-%d-%d", info.ProvID, info.HospID, info.UserID)
}

func (info *Info) errMsg() string {
	return fmt.Sprintf("provID:%d, hospID:%d, userID:%d", info.ProvID, info.HospID, info.UserID)
}

// New : セッションを生成する。
func New() *Conn {
	return &Conn{con: _pool.Get()}
}

// Close : セッションをクローズする。
func (c Conn) Close() error {
	return c.con.Close()
}

// Get : セッション情報を取得する。
func (c Conn) Get(sessionID string) (info *Info, err error) {
	s, err := redis.String(c.con.Do("get", sessionID))
	if err != nil {
		err = errors.ErrNotAuthCredential.Wrapf(err, "sessionID:%s", sessionID)
		return
	}
	if err = json.Unmarshal([]byte(s), &info); err != nil {
		err = errors.ErrFatal.Wrapf(err, "sessionID:%s", sessionID)
		return
	}
	return
}

// Add : セッションを追加する。
func (c Conn) Add(info *Info, isPKI bool) (sessionID string, err error) {
	sessionID = base64.StdEncoding.EncodeToString(<-_chanSession)
	err = c.Update(sessionID, info, isPKI)
	return
}

// Update : セッション情報を更新する。
func (c Conn) Update(sessionID string, info *Info, isPKI bool) (err error) {
	expire := conf.Get().Redis.Expire
	if !isPKI {
		expire = conf.Get().Redis.ExpireAPI
	}

	if info == nil {
		if _, err = c.con.Do("expire", sessionID, expire); err != nil {
			err = errors.ErrNotAuthCredential.Wrapf(err, "sessionID:%s, %s", sessionID, info.errMsg())
			return
		}
	} else {
		var b []byte
		if b, err = json.Marshal(info); err != nil {
			err = errors.ErrFatal.Wrapf(err, "sessionID:%s, %s", sessionID, info.errMsg())
			return
		}
		if _, err = c.con.Do("setex", sessionID, expire, string(b)); err != nil {
			err = errors.ErrNotAuthCredential.Wrapf(err, "sessionID:%s, %s", sessionID, info.errMsg())
			return
		}
	}
	return
}

// Delete : セッション情報を削除する。
func (c Conn) Delete(sessionID string) (err error) {
	c.con.Do("del", sessionID)
	return
}
