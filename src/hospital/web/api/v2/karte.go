package v2

import (
	"hospital/errors"
	"hospital/hosp/patient"
	"net/http"
	"reflect"

	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// GetKarteRecord : GET /services/api/v2/hospital/patient/karte/record
func GetKarteRecord(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req struct {
		RequestPatientID
		RecordType int `query:"record_type" validate:"min=-1,max=3"`
	}
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	// 患者検索
	recs, err := h.PatientMngr().Records(req.EncPatientID, req.EncH13nID, req.RecordType)
	if err != nil {
		return
	}

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		Records   []*patient.Record `json:"records"`
		LoginInfo *LoginInfo        `json:"login_info"`
	}{
		Records:   recs,
		LoginInfo: NewLoginInfo(h),
	})
}

// AddKarte : POST /services/api/v2/hospital/patient/karte/record
func AddKarte(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req struct {
		RequestPatientID
		patient.Record
	}
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}

	// カルテ登録
	if err = h.PatientMngr().AddKarte(req.EncPatientID, req.EncH13nID, &req.Record); err != nil {
		return
	}

	// レスポンス作成
	return ctx.NoContent(http.StatusNoContent)
}

// UpdateKarte : PUT /services/api/v2/hospital/patient/karte/record
func UpdateKarte(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req patient.Record
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}

	// カルテ登録
	if err = h.PatientMngr().UpdateKarte(&req); err != nil {
		return
	}

	// レスポンス作成
	return ctx.NoContent(http.StatusNoContent)
}
