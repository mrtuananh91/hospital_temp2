package v2

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// FloorLists : フロアー一覧
func FloorLists(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	floors, err := h.FloorMngr().Floors()
	return ctx.JSON(http.StatusOK, floors)

}
