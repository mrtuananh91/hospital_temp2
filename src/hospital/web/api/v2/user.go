package v2

import (
	"fmt"
	"hospital/errors"
	"hospital/hosp/user"
	"net/http"
	"reflect"

	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// GetUser : GET /services/api/v2/hospital/user
func GetUser(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req user.Search
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	// ユーザー検索
	us, err := h.UserMngr().Search(&req)
	if err != nil {
		return
	}

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		Users     []*user.User `json:"users"`
		LoginInfo *LoginInfo   `json:"login_info"`
	}{
		Users:     us,
		LoginInfo: NewLoginInfo(h),
	})
}

// AddUser : POST /services/api/v2/hospital/user
func AddUser(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	u := []*user.User{}
	if err = ctx.Bind(&u); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	tag := fmt.Sprintf("required,max=%d,dive", h.Conf().User.MaxAddCount)
	if err = validator.New().Var(&u, tag); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(u), "json", h.DebugMsg())
		return
	}

	if err = h.UserMngr().Add(u); err != nil {
		return
	}

	return ctx.NoContent(http.StatusNoContent)
}
