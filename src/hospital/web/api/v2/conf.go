package v2

import (
	"hospital/hosp/conf"
	"net/http"

	"github.com/labstack/echo/v4"
)

// GetConf : GET /services/api/v2/hospital/conf
func GetConf(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	dds, err := h.ConfMngr().DiagDepts()
	if err != nil {
		return
	}

	dts, err := h.ConfMngr().DiagTypes()
	if err != nil {
		return
	}

	return ctx.JSON(http.StatusOK, &struct {
		DiagDepts []*conf.DiagDept `json:"diagdepts"`
		DiagTypes []*conf.DiagType `json:"diagtypes"`
		LoginInfo *LoginInfo       `json:"login_info"`
	}{
		DiagDepts: dds,
		DiagTypes: dts,
		LoginInfo: NewLoginInfo(h),
	})
}
