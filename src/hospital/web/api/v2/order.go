package v2

import (
	"hospital/errors"
	"net/http"

	"github.com/labstack/echo/v4"
)

// GetOrderOrderList : オーダー一覧を取得する。
func GetOrderOrderList(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエストパラメータ
	floorID := ctx.QueryParam("floor_id")
	date := ctx.QueryParam("date")
	orderType := 0

	// 患者一覧取得
	orders, err := h.PatientMngr().GetPatientOrders(floorID, date, orderType)

	// レスポンス作成

	return ctx.JSON(http.StatusOK, orders)
}

// GetOrderInjections : オーダー注射を取得する。
func GetOrderInjections(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエストパラメータ
	floorID := ctx.QueryParam("floor_id")
	date := ctx.QueryParam("date")
	orderType := 2

	// 患者一覧取得
	patients, err := h.PatientMngr().GetPatientOrders(floorID, date, orderType)

	// レスポンス作成
	return ctx.JSON(http.StatusOK, patients)
}

// ValidateBarcode :
func ValidateBarcode(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	req := struct {
		FloorID   string `json:"floor_id"`
		OrderID   int    `json:"order_id"`
		Step      int    `json:"step"`
		Date      string `json:"start_date"`
		OrderType string `json:"order_type"`
		PatientID string `json:"patient_id"`
		Status    bool   `json:"status"`
	}{
		Status: true,
	}

	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}

	u := h.UserMngr().Logined()
	// 薬剤バーコード
	if user, err := h.FloorMngr().CreateOrUpdateOrderActWithBarcode(u, req.Step, req.FloorID, req.Date, req.OrderType, req.PatientID, req.OrderID, req.Status); err == nil {
		if user == nil {
			return ctx.JSON(http.StatusNoContent, nil)
		}
		return ctx.JSON(http.StatusOK, user)
	}

	return ctx.JSON(http.StatusBadRequest, nil)
}

// OrderInjectionUncheck :
func OrderInjectionUncheck(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	req := struct {
		FloorID   string `json:"floor_id"`
		OrderID   int    `json:"order_id"`
		PatientID string `json:"patient_id"`
		Step      int    `json:"step"`
		Date      string `json:"start_date"`
		OrderType string `json:"order_type"`
	}{
		OrderType: "2",
	}

	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	u := h.UserMngr().Logined()

	passed, _ := h.FloorMngr().RemoveCellOrderAct(u, req.Step, req.FloorID, req.Date, req.OrderType, req.PatientID, req.OrderID)

	return ctx.JSON(http.StatusOK, struct {
		Passed bool `json:"passed"`
	}{
		Passed: passed,
	})

}
