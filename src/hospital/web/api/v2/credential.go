package v2

import (
	"hospital/conf"
	"hospital/errors"
	"hospital/hosp"
	"hospital/prov"
	"hospital/session"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

// PostCredential : POST /services/api/v2/hospital/credential
func PostCredential(ctx echo.Context) (err error) {
	var p *prov.Prov
	var hospID int64

	cfg := conf.Get()
	reqHeader := ctx.Request().Header
	resHeader := ctx.Response().Header()
	domain := reqHeader.Get("X-Forwarded-Domain")
	switch domain {
	// ブラウザ
	case cfg.Server.Domain.Main:
		ou := reqHeader.Get("SSL_CLIENT_S_DN_OU")
		if hospID, err = strconv.ParseInt(ou, 10, 64); err != nil {
			err = errors.ErrNotFound404.Wrapf(err, "domain:%s, SSL_CLIENT_S_DN_OU:%s", domain, ou)
			return
		}
		p = prov.NewPKI()
		resHeader.Add(HeaderXPID, strconv.FormatInt(p.ID, 10))
		resHeader.Add(HeaderXHID, ou)
	// アプリ
	case cfg.Server.Domain.App:
		p = prov.NewApp()
		resHeader.Add(HeaderXPID, strconv.FormatInt(p.ID, 10))
	// API連携
	case cfg.Server.Domain.Link:
	default:
		err = errors.ErrNotFound404.New("domain:" + domain)
		return
	}

	// リクエスト検証
	req := struct {
		EncID     string `json:"id"`        // App:HospID, API:ProvID
		Account   string `json:"account"`   // PKI, App
		Password  string `json:"password"`  // PKI, App
		Random    []byte `json:"random"`    // API
		Signature []byte `json:"signature"` // API
	}{}
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrNotFound404.Wrap(err, "domain:"+domain)
		return
	}

	// クレデンシャル情報取得
	var credential string
	if p == nil {
		if credential, p, err = prov.NewAPI(req.EncID, req.Random, req.Signature); err != nil {
			return
		}
		resHeader.Add(HeaderXPID, strconv.FormatInt(p.ID, 10))
	} else {
		if p.IsApp() {
			hospID = hosp.DecryptHospID(req.EncID)
			resHeader.Add(HeaderXHID, strconv.FormatInt(hospID, 10))
		}

		resHeader.Add(HeaderXAccount, req.Account)

		var h *hosp.Hosp
		if h, err = hosp.New(p, hospID); err != nil {
			return
		}
		if credential, err = h.Auth(req.Account, req.Password); err != nil {
			return
		}
	}

	if p.IsPKI() {
		ctx.SetCookie(&http.Cookie{
			Name:     HeaderXCredential,
			Value:    credential,
			Path:     cfg.Server.Cookie.Path,
			HttpOnly: true,
			//Secure:   cfg.Server.Cookie.Secure, // T.B.D
			//Domain:   cfg.Server.Domain.Main, // T.B.D
			SameSite: http.SameSiteLaxMode,
		})
		err = ctx.NoContent(http.StatusNoContent)
	} else {
		err = ctx.JSON(http.StatusOK, &struct {
			Credentail string `json:"credential"`
		}{
			Credentail: credential,
		})
	}
	return
}

// DeleteCredential : DELETE /services/api/v2/hospital/credential
func DeleteCredential(ctx echo.Context) (err error) {
	credential, _, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	ss := session.New()
	defer ss.Close()
	ss.Delete(credential)

	return ctx.NoContent(http.StatusNoContent)
}
