package v2

import (
	"hospital/errors"
	"hospital/hosp/patient"
	"net/http"
	"reflect"

	validator "github.com/go-playground/validator/v10"

	"github.com/labstack/echo/v4"
)

// GetHospitalization : GET /services/api/v2/hospital/hospitalizationn
func GetHospitalization(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req patient.PatientID
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	encPatientID := req.EncPatientID

	// 患者検索
	hps, err := h.PatientMngr().GetHospitalization(encPatientID)
	if err != nil {
		return
	}

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		Hospitalization []*patient.Hospitalization `json:"hospitalization"`
		LoginInfo       *LoginInfo                 `json:"loginInfo"`
	}{
		Hospitalization: hps,
		LoginInfo:       NewLoginInfo(h),
	})
}

// AddHosp : POST /services/api/v2/hospital/addhospitalization
func AddHosp(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req patient.Hospitalization
	//var req patient.Patient
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}

	h.PatientMngr().AddHosp(&req)
	return ctx.NoContent(http.StatusNoContent)
}
