package v2

import (
	"fmt"
	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"hospital/errors"
	"hospital/hosp/inchg"
	"net/http"
	"reflect"
)

//GetInchgMed : GET /services/api/v2/hospital/orderinchgmed
//インチャージ食事取得
func GetInchgMed(ctx echo.Context) (err error) {
	fmt.Println("[Higuchi][orderinchgmed.go]GetInchgMed()")
	fmt.Printf("[Higuchi][orderinchgmed.go]ctx==(%%#v) %#v\n", ctx)
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req inchg.SearchInchgMed
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	//fmt.Printf("[Higuchi][orderinchgmed.go]req==(%%#v) %#v\n", req)	//デバッグ用

	// 患者検索
	med, err := h.InchgMngr().SearchInchgMed(&req)

	if err != nil {
		return
	}

	//for i := 0; i < len(med); i++ {fmt.Printf("[Higuchi]SearchInchgMedからの戻り値 == %#v\n", med[i])}	//デバッグ用

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		//初期化
		Inchgmed  []*inchg.InchgMed `json:"inchgmed"`
		LoginInfo *LoginInfo        `json:"login_info"`
	}{
		//値代入
		Inchgmed:  med,
		LoginInfo: NewLoginInfo(h),
	})
}

// UpdateInchgMed : PUT /services/api/v2/hospital/orderinchgmed
// インチャージ食事の対応更新
func UpdateInchgMed(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	//fmt.Println("[Higuchi][orderinchgmed.go]UpdateInchgMed()")	//デバッグ用
	//fmt.Printf("[Higuchi][orderinchgmed.go]ctx==%#v\n", ctx)	//デバッグ用
	//fmt.Printf("[Higuchi][orderinchgmed.go]h==%#v\n", h)		//デバッグ用

	// リクエスト検証
	var req inchg.UpdateInchg
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	//fmt.Printf("[Higuchi][orderinchgmed.go]UpdateInchgMed()req==(%%#v) %#v\n", &req)	//デバッグ用

	if err = h.InchgMngr().UpdateInchgStatus(&req); err != nil {
		return
	}

	return ctx.NoContent(http.StatusNoContent)

}
