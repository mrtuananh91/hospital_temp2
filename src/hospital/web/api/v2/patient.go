package v2

import (
	"hospital/errors"
	"hospital/hosp/patient"
	"net/http"
	"reflect"

	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// RequestPatientID : 患者IDと入院ID
type RequestPatientID struct {
	EncPatientID string `query:"patient_id"`
	EncH13nID    string `query:"h13n_id"`
}

// GetPatient : GET /services/api/v2/hospital/patient
func GetPatient(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req patient.Search
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	// 患者検索
	pts, err := h.PatientMngr().Search(&req)
	if err != nil {
		return
	}

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		Patients  []*patient.Patient `json:"patients"`
		LoginInfo *LoginInfo         `json:"login_info"`
	}{
		Patients:  pts,
		LoginInfo: NewLoginInfo(h),
	})
}

// AddPatients : POST /services/api/v2/hospital/patient
func AddPatients(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req struct {
		Patients []*patient.Patient `json:"patients" validate:"required,min=1"`
	}
	//var req patient.Patient
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}
	for _, pt := range req.Patients {
		if err = validator.New().Struct(pt); err != nil {
			err = TranslateValidationError(err, reflect.TypeOf(*pt), "json", h.DebugMsg())
			return
		}
	}

	// 患者登録
	//if err = h.PatientMngr().Add([]*patient.Patient{&req}); err != nil {
	if err = h.PatientMngr().Add(req.Patients); err != nil {
		return
	}

	// レスポンス作成
	return ctx.NoContent(http.StatusNoContent)
}
