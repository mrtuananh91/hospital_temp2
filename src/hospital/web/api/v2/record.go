package v2

import (
	"hospital/errors"
	"hospital/hosp/record"
	"net/http"

	//"net/http"
	"reflect"

	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// GetRecord : GET /services/api/v2/hospital/patient/record
func GetRecord(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req record.RecordSearch
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}
	// 患者検索
	rcd, err := h.RecordMngr().Search(&req)
	if err != nil {
		return
	}

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		Records   []*record.Record `json:"records"`
		LoginInfo *LoginInfo       `json:"loginInfo"`
	}{
		Records:   rcd,
		LoginInfo: NewLoginInfo(h),
	})
}

func AddRecord(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req struct {
		Records []*record.Record `json:"record" validate:"required,min=1"`
	}

	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}
	for _, rd := range req.Records {
		if err = validator.New().Struct(rd); err != nil {
			err = TranslateValidationError(err, reflect.TypeOf(*rd), "json", h.DebugMsg())
			return
		}
	}

	//user_id user_nameをセッションから取得

	// 看護記録登録
	//if err = h.PatientMngr().Add([]*patient.Patient{&req}); err != nil {
	if err = h.RecordMngr().Add(req.Records); err != nil {
		return
	}

	// レスポンス作成
	return ctx.NoContent(http.StatusNoContent)

}

func UpdateRecord(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req struct {
		Records []*record.Record `json:"record" validate:"required,min=1"`
	}

	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "json", h.DebugMsg())
		return
	}
	for _, rd := range req.Records {
		if err = validator.New().Struct(rd); err != nil {
			err = TranslateValidationError(err, reflect.TypeOf(*rd), "json", h.DebugMsg())
			return
		}
	}

	// todo user_id user_nameをセッションから取得?

	// 看護記録更新
	if err = h.RecordMngr().Update(req.Records); err != nil {
		return
	}

	// レスポンス作成
	return ctx.NoContent(http.StatusNoContent)
}
