package v2

import (
	//"fmt"
	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"hospital/errors"
	"hospital/hosp/inchg"
	"net/http"
	"reflect"
)

//GetInchgEat : GET /services/api/v2/hospital/orderinchgeat
//インチャージ食事取得
func GetInchgEat(ctx echo.Context) (err error) {

	//fmt.Println("[Higuchi][orderinchgeat.go]GetInchgEat()")	//デバッグ用
	//fmt.Printf("[Higuchi][orderinchgeat.go]ctx==(%%#v) %#v\n", ctx)	//デバッグ用

	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエスト検証
	var req inchg.SearchInchgEat
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	//fmt.Printf("[Higuchi][orderinchgeat.go]req==(%%#v) %#v\n", req)	//デバッグ用

	// 患者検索
	eat, err := h.InchgMngr().SearchInchgEat(&req)

	if err != nil {
		return
	}

	//for i := 0; i < len(eat); i++ {fmt.Printf("[Higuchi]SearchInchgEatからの戻り値 == %#v\n", eat[i])}	//デバッグ用　SearchInchgEatからの戻り値表示

	// レスポンス作成
	return ctx.JSON(http.StatusOK, &struct {
		//初期化
		Inchgeat  []*inchg.InchgEat `json:"inchgeat"`
		LoginInfo *LoginInfo        `json:"login_info"`
	}{
		//値代入
		Inchgeat:  eat,
		LoginInfo: NewLoginInfo(h),
	})
}

// UpdateInchgEat : PUT /services/api/v2/hospital/orderinchgeat
// インチャージ食事の対応更新
func UpdateInchgEat(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	//fmt.Println("[Higuchi][orderinchgeat.go]UpdateInchgEat()")	//デバッグ用
	//fmt.Printf("[Higuchi][orderinchgeat.go]ctx==%#v\n", ctx)	//デバッグ用
	//fmt.Printf("[Higuchi][orderinchgeat.go]h==%#v\n", h)	//デバッグ用

	// リクエスト検証
	var req inchg.UpdateInchg
	if err = ctx.Bind(&req); err != nil {
		err = errors.ErrInvalidParams.Wrap(err, h.DebugMsg())
		return
	}
	if err = validator.New().Struct(&req); err != nil {
		err = TranslateValidationError(err, reflect.TypeOf(req), "query", h.DebugMsg())
		return
	}

	//fmt.Printf("[Higuchi][orderinchgeat.go]UpdateInchgEat()req==(%%#v) %#v\n", &req)	//デバッグ用

	if err = h.InchgMngr().UpdateInchgStatus(&req); err != nil {
		return
	}

	return ctx.NoContent(http.StatusNoContent)

}
