package v2

import (
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"hospital/hosp"
	"hospital/prov"
	"log"
	"net/http"
	"reflect"
	"strconv"

	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// 独自ヘッダー
const (
	HeaderXCredential = "X-CREDENTIAL"
	HeaderXPID        = "X-PID"
	HeaderXHID        = "X-HID"
	HeaderXAccount    = "X-Account"
)

// Echoコンテキストに設定するキー値
const (
	EchoCtxProv = "Hospital-Provider"
	EchoCtxHosp = "Hospital-Hospital"
)

// LoginInfo : ログイン情報
type LoginInfo struct {
	HospID   int64   `json:"hosp_id"`
	HospName string  `json:"hosp_name"`
	UserID   *string `json:"user_id"`
	Account  *string `json:"account"`
}

// NewLoginInfo : ログイン情報を生成する。
func NewLoginInfo(h *hosp.Hosp) *LoginInfo {
	var userID, account *string
	lu := h.UserMngr().Logined()
	if lu != nil {
		userID = &lu.EncID
		account = &lu.Account
	}
	return &LoginInfo{
		HospID:   h.ID,
		HospName: h.Name,
		UserID:   userID,
		Account:  account,
	}
}

// AuthCredential : クレデンシャル情報を認証する。
func AuthCredential(ctx echo.Context) (credential string, h *hosp.Hosp, err error) {
	cfg := conf.Get()
	reqHeader := ctx.Request().Header
	domain := reqHeader.Get("X-Forwarded-Domain")

	switch domain {
	case cfg.Server.Domain.Main:
		var cookie *http.Cookie
		if cookie, err = ctx.Cookie(HeaderXCredential); err != nil {
			err = errors.ErrNotAuthCredential.Wrap(err, "cookie:nil")
			return
		}
		credential = cookie.Value
		if h, err = hosp.NewCredential(prov.NewPKI(), credential); err != nil {
			return
		}
		ctx.Set(EchoCtxHosp, h)
	case cfg.Server.Domain.App:
		credential = reqHeader.Get(HeaderXCredential)
		if h, err = hosp.NewCredential(prov.NewApp(), credential); err != nil {
			return
		}
		ctx.Set(EchoCtxHosp, h)
	case cfg.Server.Domain.Link:
		credential = reqHeader.Get(HeaderXCredential)
		var p *prov.Prov
		if p, err = prov.NewCredential(credential, ctx.QueryParam("keepAuth") == "true"); err != nil {
			return
		}
		encHospID := ctx.QueryParam("hospID")
		if h, err = hosp.New(p, hosp.DecryptHospID(encHospID)); err != nil {
			return
		}
		ctx.Set(EchoCtxProv, p)
	default:
		err = errors.ErrNotFound404.New("domain:" + domain)
		return
	}

	resHeader := ctx.Response().Header()
	resHeader.Add(HeaderXPID, strconv.FormatInt(h.Prov.ID, 10))
	resHeader.Add(HeaderXHID, strconv.FormatInt(h.ID, 10))
	lu := h.UserMngr().Logined()
	if lu != nil {
		resHeader.Add(HeaderXAccount, lu.Account)
	}
	return
}

// TranslateValidationError : validationエラーを[]errors.ErrValidatorエラーに変換する。
//  err : validator.ValidationErrors型エラー
//  t : 検証対象の（構造体）タイプ
//  tag : t内のメタ情報のタグ（"json" or "query" or "form" ...）
//  errMsg : err.Error()に追加するエラーメッセージ
func TranslateValidationError(err error, t reflect.Type, tag, errMsg string) *errors.Array {
	ves, ok := err.(validator.ValidationErrors)
	if !ok {
		panic(err)
	}

	errs := make([]error, 0)
	for _, ve := range ves {
		sf, _ := t.FieldByName(ve.Field())
		errs = append(errs, errors.ErrValidator.Wrap(ve,
			fmt.Sprintf("%s, field:%s, tag:%s, param:%v", errMsg, ve.Field(), ve.Tag(), ve.Param()),
			sf.Tag.Get(tag), ve.Tag(), ve.Param()))
	}

	return &errors.Array{Errs: errs}
}

/*
// ErrConv : Validatorエラー変換構造体
type ErrConv struct {
	StructTag string
	Field     string
	Tag       string
	ErrCode   errors.ErrCode
	TagToArgs bool
	Args      []interface{}
}

// NewErrorByVal : Validatorエラーからerrors.Err*エラーを生成する。
func NewErrorByVal(err error, t reflect.Type, errConvs []*ErrConv, errMsg string) error {
	valErrs, ok := err.(validator.ValidationErrors)
	if ok {
		for _, valErr := range valErrs {
			sf, _ := t.FieldByName((valErr.Field()))
			for _, errConv := range errConvs {
				field := sf.Tag.Get(errConv.StructTag)
				if field == errConv.Field {
					if errConv.Tag == "" || errConv.Tag == valErr.Tag() {
						if errMsg == "" {
							errMsg = fmt.Sprintf("field:%s, tag:%s", field, errConv.Tag)
						} else {
							errMsg = fmt.Sprintf("%s, field:%s, tag:%s", errMsg, field, errConv.Tag)
						}
						args := errConv.Args
						if errConv.ErrCode == errors.ErrInvalidValidator {
							args = []interface{}{errConv.Tag, field}
						} else if errConv.TagToArgs {
							args = []interface{}{errConv.Tag}
						}
						return errConv.ErrCode.Wrap(err, errMsg, args...)
					}
				}
			}
		}
	}

	for _, errConv := range errConvs {
		if errConv.Field == "" {
			if errMsg == "" {
				errMsg = "field:empty"
			} else {
				errMsg = fmt.Sprintf("%s, field.empty", errMsg)
			}
			return errConv.ErrCode.Wrap(err, errMsg, errConv.Args...)
		}
	}

	return nil
}
*/

// ErrorHandler : echoのエラーハンドラーを定義する。
func ErrorHandler(err error, ctx echo.Context) {
	if he, ok := err.(*echo.HTTPError); ok {
		log.Printf("warning: %+v\n", err)
		ctx.Response().WriteHeader(he.Code)
		return
	}

	var errs []error
	if ary, ok := err.(*errors.Array); ok {
		errs = ary.Errs
	} else {
		errs = []error{err}
	}

	prov := ctx.Get(EchoCtxProv)
	hosp := ctx.Get(EchoCtxHosp)
	isAuth := prov != nil || hosp != nil

	type errInfo struct {
		Code   errors.ErrCode `json:"errorCode"`
		Params []interface{}  `json:"errorParams"`
	}
	errInfos := make([]*errInfo, len(errs))

	var status int
	no := conf.Get().Server.No
	for i, err := range errs {
		ec, ecRes, errParams := errors.Info(err)

		// T.B.D
		if !isAuth || ecRes == errors.ErrNotFound404 {
			log.Printf("warning: %+v\n", err)
			ctx.Response().WriteHeader(http.StatusNotFound)
			return
		} else if ec == errors.ErrFatal {
			log.Printf("alert: %+v\n", err)
			status = http.StatusInternalServerError
			errInfos = []*errInfo{}
			break
		} else {
			log.Printf("warning: %+v\n", err)
			status = http.StatusBadRequest
			errInfos[i] = &errInfo{
				Code:   ec,
				Params: errParams,
			}
		}
	}

	if err = ctx.JSON(status, &struct {
		No     int        `json:"no"`
		Errors []*errInfo `json:"errors"`
	}{
		No:     no,
		Errors: errInfos,
	}); err != nil {
		log.Printf("alert: %+v\n", err)
		ctx.Response().WriteHeader(http.StatusInternalServerError)
	}
}
