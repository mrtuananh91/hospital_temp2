package v2

import (
	"hospital/hosp"
	"hospital/util"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
)

type TempData struct {
	PatientInfo string
	OrderInfo   string
	Comment     string
	Jancode     string
}

// GetOrderInchgShotPdfURL :
func GetOrderInchgShotPdfURL(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	// リクエストパラメータ
	pdfType := ctx.QueryParam("type")
	date := ctx.QueryParam("date")
	// 出力PDFファイルパス
	outputPath := "./assets/dist/pdf/" + strconv.FormatInt(int64(time.Now().Unix()), 10) + ".pdf"

	switch pdfType {
	case "1":
		// PDFを作成
		floorID := ctx.QueryParam("floor_id")

		patientOrders, _ := h.PatientMngr().GetPatientOrders(floorID, date, 2)
		if time, err1 := time.Parse("2006-01-02 15:04:05", date); err1 == nil {
			date = time.Format("2006.01.02")
		}
		// テンプレートパス
		templatePath := "./storage/pdf/templates/order-inchg-shot-pdf-1.html"

		// テンプレートに入れるデータ

		// query database

		// PDF出力処理を宣言
		r := util.NewRequestPdf("")
		// テンプレートにPDFデータを導入
		err = r.ParseTemplate(templatePath, map[string]interface{}{
			"OrderDate": date,
			"temps":     patientOrders,
		})

		if _, err = r.GeneratePDF(outputPath); err != nil {
			return ctx.JSON(http.StatusBadRequest, struct {
				message string
			}{
				message: "Err:err",
			})
		}

		break

	}

	// 返却
	return ctx.JSON(http.StatusOK, outputPath[8:])
}

func exportListInjection(h *hosp.Hosp) (r *util.RequestPdf, err error) {

	return
}
