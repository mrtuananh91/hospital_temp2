package v2

import (
	"hospital/hosp"
	"net/http"

	"github.com/labstack/echo/v4"
)

// GetHospital : GET /services/api/v2/hospital
func GetHospital(ctx echo.Context) (err error) {
	_, h, err := AuthCredential(ctx)
	if err != nil {
		return
	}

	return ctx.JSON(http.StatusOK, &struct {
		Hospital  *hosp.Hosp `json:"hospital"`
		LoginInfo *LoginInfo `json:"login_info"`
	}{
		Hospital:  h,
		LoginInfo: NewLoginInfo(h),
	})
}
