package conf

import (
	"database/sql"
	"encoding/json"
	"hospital/errors"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/comail/colog"
)

// Config : グローバル設定情報
type Config struct {
	Version    string `json:"-"`
	CommitHash string `json:"-"`
	Server     struct {
		No       int         `json:"no"`
		Listen   string      `json:"listen"`
		Port     int         `json:"port"`
		FileMode os.FileMode `json:"fileMode"`
		MaxConn  int         `json:"maxConn"`
		Domain   struct {
			Main string `json:"main"`
			App  string `json:"app"`
			Link string `json:"link"`
		} `json:"domain"`
		Log struct {
			Level      string      `json:"level"`
			CologLevel colog.Level `json:"-"`
			OmitDate   bool        `json:"omitDate"`
			Access     bool        `json:"access"`
		} `json:"log"`
		Time struct {
			Location     *time.Location `json:"-"`
			LocationName string         `json:"location"`
		} `json:"time"`
		Cookie struct {
			Path   string `json:"path"`
			Secure bool   `json:"secure"`
		} `json:"cookie"`
	} `json:"server"`
	DB struct {
		Name      string `json:"-"`
		Source    string `json:"source"`
		CurTime   string `json:"curTime"`
		CreateSQL string `json:"-"`
		Hospitals []struct {
			Source string `json:"source"`
			MinID  int64  `json:"minID"`
			MaxID  int64  `json:"maxID"`
		} `json:"hospitals"`
	} `json:"db"`
	Redis struct {
		Host        string        `json:"host"`
		Port        int           `json:"port"`
		MaxIdle     int           `json:"maxIdle"`
		MaxActive   int           `json:"maxActive"`
		IdleTimeout time.Duration `json:"idleTimeout"`
		Wait        bool          `json:"wait"`
		Expire      int           `json:"expire"`
		ExpireAPI   int           `json:"expireAPI"`
	}
	Hosp HospConfig `json:"hosp"`
}

// HospConfig : 病院ごとの設定情報
type HospConfig struct {
	User struct {
		MaxFailCount   int `json:"maxFailCount"`
		MaxAddCount    int `json:"maxAddCount"`
		MaxSearchCount int `json:"maxSearchCount"`
		DefSearchCount int `json:"defSearchCount"`
	} `json:"user"`
	Patient struct {
		MaxFailCount   int `json:"maxFailCount"`
		MaxAddCount    int `json:"maxAddCount"`
		MaxSearchCount int `json:"maxSearchCount"`
		DefSearchCount int `json:"defSearchCount"`
	} `json:"patient"`
	Forum struct {
		MaxSearchCount int `json:"maxSearchCount"`
		DefSearchCount int `json:"defSearchCount"`
	} `json:"forum"`
}

var _cfg Config
var _mutex sync.RWMutex

func init() {
	_mutex = sync.RWMutex{}
}

// Get : 設定情報を取得する。
func Get() *Config {
	_mutex.RLock()
	defer _mutex.RUnlock()
	return &_cfg
}

// Load : 設定ファイルを読み込む。
func Load() (err error) {
	_mutex.Lock()
	defer _mutex.Unlock()

	dbName := os.Getenv("DBNAME")
	// DBSOURCE=host=db port=5432 dbname=db_0 user=postgres password=nelt0001 sslmode=disable
	dbSource := os.Getenv("DBSOURCE")
	serverNo := os.Getenv("SERVERNO")
	if serverNo == "" {
		serverNo = "0"
	}

	con, err := sql.Open(dbName, dbSource)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, "db:error")
		return
	}

	var cfgJSON, createSQL string
	if err = con.QueryRow("SELECT config_json,create_sql FROM tbl_config WHERE id=1").Scan(&cfgJSON, &createSQL); err != nil {
		err = errors.ErrFatal.Wrap(err, "db:error")
		return
	}

	cfg := Config{}
	if err = json.Unmarshal([]byte(cfgJSON), &cfg); err != nil {
		err = errors.ErrFatal.Wrap(err, "json:error")
		return
	}

	cfg.DB.Name = dbName
	cfg.DB.CreateSQL = createSQL

	if cfg.Server.Log.CologLevel, err = colog.ParseLevel(cfg.Server.Log.Level); err != nil {
		err = errors.ErrFatal.Wrap(err, "loglevel:error")
		return
	}

	if cfg.Server.Time.Location, err = time.LoadLocation(cfg.Server.Time.LocationName); err != nil {
		err = errors.ErrFatal.Wrap(err, "location:error")
		return
	}
	time.Local = cfg.Server.Time.Location

	cfg.Server.No, err = strconv.Atoi(serverNo)
	if err != nil {
		err = errors.ErrFatal.Wrap(err, "serverNo:error")
		return
	}

	_cfg = cfg

	return
}
