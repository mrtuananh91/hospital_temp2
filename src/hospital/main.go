package main

import (
	"fmt"
	"hospital/conf"
	"hospital/session"
	wa2 "hospital/web/api/v2"
	"log"
	"net"
	"os"

	"github.com/comail/colog"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
	"golang.org/x/net/netutil"
)

// Version : バージョン
var Version string

// CommitHash : コミットハッシュ
var CommitHash string

func main() {
	// 設定情報初期化
	if err := conf.Load(); err != nil {
		log.Fatalf("alert: %+v\n", err)
	}
	cfg := conf.Get()
	cfg.Version = Version
	cfg.CommitHash = CommitHash
	session.Init()

	// ログ初期化
	colog.SetDefaultLevel(colog.LError)
	colog.SetMinLevel(cfg.Server.Log.CologLevel)
	cologFlag := log.Llongfile
	if !cfg.Server.Log.OmitDate {
		cologFlag = log.Ldate | log.Ltime | log.Llongfile
	}
	colog.SetFormatter(&colog.StdFormatter{
		Colors: false,
		Flag:   cologFlag,
	})
	colog.Register()

	// echo初期化
	e := echo.New()
	network := "unix"
	address := cfg.Server.Listen
	if cfg.Server.Port > 0 {
		network = "tcp4"
		address = fmt.Sprintf("%s:%d", cfg.Server.Listen, cfg.Server.Port)
	} else {
		os.Remove(address)
	}
	l, err := net.Listen(network, address)
	if err != nil {
		log.Fatalf("alert: %+v\n", err)
	}
	if cfg.Server.Port == 0 {
		os.Chmod(address, 0777)
		// os.Chmod(address, cfg.Server.FileMode) T.B.D
	}
	e.Listener = netutil.LimitListener(l, cfg.Server.MaxConn)
	e.HideBanner = true
	e.HTTPErrorHandler = wa2.ErrorHandler
	e.Use(middleware.Recover())
	//if cfg.Server.Log.Access {
	e.Use(middleware.Logger())
	//}

	e.POST("/services/api/v2/hospital/credential", wa2.PostCredential)
	e.DELETE("/services/api/v2/hospital/credential", wa2.DeleteCredential)
	e.GET("/services/api/v2/hospital", wa2.GetHospital)
	e.GET("/services/api/v2/hospital/user", wa2.GetUser)
	e.GET("/services/api/v2/hospital/patient", wa2.GetPatient)
	e.GET("/services/api/v2/hospital/patient/hospitalization", wa2.GetHospitalization)
	e.POST("/services/api/v2/hospital/patient/hospitalization", wa2.AddHosp)
	e.PUT("/services/api/v2/hospital/patient/hospitalization", wa2.AddHosp)
	e.POST("/services/api/v2/hospital/patient", wa2.AddPatients)
	e.GET("/services/api/v2/hospital/patient/record", wa2.GetRecord)
	e.POST("/services/api/v2/hospital/patient/record", wa2.AddRecord)
	e.PUT("/services/api/v2/hospital/patient/record", wa2.UpdateRecord)
	e.GET("/services/api/v2/hospital/patient/karte/record", wa2.GetKarteRecord)
	e.POST("/services/api/v2/hospital/patient/karte", wa2.AddKarte)
	e.PUT("/services/api/v2/hospital/patient/karte", wa2.UpdateKarte)
	e.GET("/services/api/v2/hospital/conf", wa2.GetConf)

	// Floor
	e.GET("/services/api/v2/floor/list", wa2.FloorLists)

	// Order
	e.GET("/services/api/v2/order/order/list", wa2.GetOrderOrderList)

	e.GET("/services/api/v2/hospital/inchgeat", wa2.GetInchgEat)
	e.PUT("/services/api/v2/hospital/inchgeat", wa2.UpdateInchgEat)
	e.GET("/services/api/v2/hospital/inchgmed", wa2.GetInchgMed)
	e.PUT("/services/api/v2/hospital/inchgmed", wa2.UpdateInchgMed)

	e.GET("/services/api/v2/hospital/pdf/order", wa2.GetOrderInchgShotPdfURL)

	// Floor
	// e.GET("/services/api/v2/hospital/floors", wa2.GetFloors)

	// Order
	e.GET("/services/api/v2/order/order/list", wa2.GetOrderOrderList)
	e.GET("/services/api/v2/hospital/order/incharge/inject", wa2.GetOrderInjections)
	e.POST("/services/api/v2/order/inchg/shot/barcode", wa2.ValidateBarcode)
	e.PUT("/services/api/v2/order/inchg/shot/uncheck", wa2.OrderInjectionUncheck)

	// floor
	e.GET("/services/api/v2/hospital/floors/list", wa2.FloorLists)

	if err = e.Start(""); err != nil {
		log.Fatalf("alert: %+v\n", err)
	}
}
