package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"hospital/conf"
	"hospital/errors"
	"reflect"
	"strings"
	"sync"
	"time"
)

// CreateDB : 病院DB生成用ID
const CreateDB = int64(-1)

// CommonID : DB共通ID
const CommonID = int64(0)

var _mutex sync.RWMutex
var _conMap map[int64]*sql.DB

func init() {
	_mutex = sync.RWMutex{}
	_conMap = map[int64]*sql.DB{}
}

func load(hospID int64, readLock bool) (con *sql.DB) {
	if readLock {
		_mutex.RLock()
		defer _mutex.RUnlock()
	}

	var ok bool
	if con, ok = _conMap[hospID]; !ok {
		con = nil
	}

	return
}

// New : DBコネクションを生成する（クローズはNG）。
func New(hospID int64) (con *sql.DB, err error) {
	// キャッシュを検索
	if con = load(hospID, true); con != nil {
		return
	}

	// DBコネクションを生成
	_mutex.Lock()
	defer _mutex.Unlock()

	if con = load(hospID, false); con != nil {
		return
	}

	cfg := conf.Get()
	hosps := cfg.DB.Hospitals
	for _, hosp := range hosps {
		if hosp.MinID <= hospID && hospID <= hosp.MaxID {
			ds := hosp.Source
			if hospID != CreateDB {
				ds = fmt.Sprintf(ds, hospID)
			}
			if con, err = sql.Open(cfg.DB.Name, ds); err != nil {
				err = errors.ErrFatal.Wrapf(err, "id:%d, ds:%s", hospID, ds)
				return
			}
			if err = con.Ping(); err != nil {
				err = errors.ErrNotFoundHosp.Wrapf(err, "id:%d, ds:%s", hospID, ds)
				return
			}
			break
		}
	}
	if con == nil {
		err = errors.ErrNotFoundHosp.Wrapf(err, "id:%d", hospID)
		return
	}

	_conMap[hospID] = con
	return
}

// Close : DBコネクションをクローズする。
func Close(hospID int64) (err error) {
	_mutex.Lock()
	defer _mutex.Unlock()

	if con := load(hospID, false); con != nil {
		con.Close()
	}

	delete(_conMap, hospID)
	return
}

// TxManager : DBトランザクション管理用構造体
type TxManager struct {
	Con      *sql.DB
	Tx       *sql.Tx
	isCommit bool
}

// NewTx : DBコネクションとDBトランザクションを生成する。
func NewTx(id int64) (*TxManager, error) {
	txm := &TxManager{}
	var err error
	if txm.Con, err = New(id); err != nil {
		return nil, err
	}
	txm.Tx, err = txm.Con.Begin()
	return txm, err
}

// Commit : DBトランザクションをコミットする。
func (txm *TxManager) Commit() (err error) {
	if err = txm.Tx.Commit(); err == nil {
		txm.isCommit = true
	}
	return
}

// Rollback : DBトランザクションをロールバックする。
func (txm *TxManager) Rollback() (err error) {
	if p := recover(); p != nil {
		txm.Tx.Rollback()
		panic(p)
	}
	if !txm.isCommit {
		err = txm.Tx.Rollback()
	}
	return
}

// NullInt32 : nilを許容するint32型。DBからnullの可能性のある数値を取得する場合に使用する。
type NullInt32 struct {
	sql.NullInt32
}

// MarshalJSON : nilの場合、"null"でJSONエンコードする。その他はjson.Marshal()と同じ。
func (i NullInt32) MarshalJSON() ([]byte, error) {
	if i.Valid {
		return json.Marshal(i.Int32)
	}
	return []byte("null"), nil
}

// UnmarshalJSON : "null"の場合、値はnilのstring型としてJSONデコードする。それ以外はjson.Unmarshal()と同じ。
func (i *NullInt32) UnmarshalJSON(data []byte) error {
	var n *int32
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	if n == nil {
		i.Int32 = 0
		i.Valid = false
	} else {
		i.Int32 = *n
		i.Valid = true
	}
	return nil
}

// NullInt64 : nilを許容するint32型。DBからnullの可能性のある数値を取得する場合に使用する。
type NullInt64 struct {
	sql.NullInt64
}

// MarshalJSON : nilの場合、"null"でJSONエンコードする。その他はjson.Marshal()と同じ。
func (i NullInt64) MarshalJSON() ([]byte, error) {
	if i.Valid {
		return json.Marshal(i.Int64)
	}
	return []byte("null"), nil
}

// UnmarshalJSON : "null"の場合、値はnilのstring型としてJSONデコードする。それ以外はjson.Unmarshal()と同じ。
func (i *NullInt64) UnmarshalJSON(data []byte) error {
	var n *int64
	if err := json.Unmarshal(data, &n); err != nil {
		return err
	}
	if n == nil {
		i.Int64 = 0
		i.Valid = false
	} else {
		i.Int64 = *n
		i.Valid = true
	}
	return nil
}

// NullString : nilを許容するstring型。DBからnullの可能性のある文字列を取得する場合に使用する。
type NullString struct {
	sql.NullString
}

// MarshalJSON : nilの場合、"null"でJSONエンコードする。その他はjson.Marshal()と同じ。
func (s NullString) MarshalJSON() ([]byte, error) {
	if s.Valid {
		return json.Marshal(s.String)
	}
	return []byte("null"), nil
}

// UnmarshalJSON : "null"の場合、値はnilのstring型としてJSONデコードする。それ以外はjson.Unmarshal()と同じ。
func (s *NullString) UnmarshalJSON(data []byte) error {
	var str *string
	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}
	if str == nil {
		s.String = ""
		s.Valid = false
	} else {
		s.String = *str
		s.Valid = true
	}
	return nil
}

// NullTime : nilを許容するstring型。DBからnullの可能性のある日時を取得する場合に使用する。
type NullTime struct {
	sql.NullTime
}

// String : nilの場合は""、nil以外の場合はtime.RFC3389でエンコードする。
func (t NullTime) String() (s string) {
	if t.Valid {
		b := make([]byte, 0, len(time.RFC3339))
		s = string(t.Time.AppendFormat(b, time.RFC3339))
	}
	return
}

// MarshalJSON : nilの場合は"null"、nil以外の場合はtime.RFC3339でJSONエンコードする。
func (t NullTime) MarshalJSON() ([]byte, error) {
	if t.Valid {
		b := make([]byte, 0, len(time.RFC3339)+2)
		b = append(b, '"')
		b = t.Time.AppendFormat(b, time.RFC3339)
		b = append(b, '"')
		return b, nil
	}
	return []byte("null"), nil
}

// UnmarshalJSON : "null"の場合、値がnilのtime.Time型としてJSONデコードする。それ以外はjson.Unmarshal()と同じ。
func (t *NullTime) UnmarshalJSON(data []byte) error {
	var tmp *time.Time
	if err := json.Unmarshal(data, &tmp); err != nil {
		s := string(data)
		tmp2, err := time.Parse(`"2006-01-02"`, s)
		if err != nil {
			return err
		}
		tmp = &tmp2
	}
	if tmp == nil {
		t.Valid = false
	} else {
		t.Valid = true
		t.Time = *tmp
	}
	return nil
}

// Scan : タイムゾーンを設定したうえでDBから取得する。
func (t *NullTime) Scan(value interface{}) error {
	t.Time, t.Valid = value.(time.Time)
	if t.Valid {
		t.Time = time.Date(t.Time.Year(), t.Time.Month(), t.Time.Day(), t.Time.Hour(),
			t.Time.Minute(), t.Time.Second(), t.Time.Nanosecond(), conf.Get().Server.Time.Location)
	}
	return nil
}

func existTag(tag string, exptTags []string) bool {
	if tag == "-" || tag == "" {
		return true
	}
	if exptTags != nil {
		for _, exptTag := range exptTags {
			if tag == exptTag {
				return true
			}
		}
	}
	return false
}

// SelectQuery : 構造体からSELECT文を生成する。
func SelectQuery(st interface{}, attr string, exptTags []string, fromAndAfter string) string {
	colLen := 0
	t := reflect.TypeOf(st)
	var q strings.Builder
	q.WriteString("SELECT ")
	fieldNames := make([]string, 0)
	tagFieldMap := map[string]reflect.StructField{}
	numField := t.NumField()
	for i := 0; i < numField; i++ {
		f := t.Field(i)
		v := f.Tag.Get(attr)
		if existTag(v, exptTags) {
			continue
		}
		tagFieldMap[v] = f
		fieldNames = append(fieldNames, f.Name)
		q.WriteString(v)
		if i < numField-1 {
			q.WriteString(",")
		}
		colLen++
	}
	q.WriteString(" ")
	q.WriteString(fromAndAfter)

	return q.String()
}

/*
// InsertQuery : 構造体からINSERT文を生成する。
func InsertQuery(sts interface{}, attr string, exptTags []string) string {
	colLen := 0
	t := reflect.TypeOf(*sts[0])
	var q strings.Builder
	q.WriteString("INSERT ")
	fieldNames := make([]string, 0)
	tagFieldMap := map[string]reflect.StructField{}
	numField := t.NumField()
	for i := 0; i < numField; i++ {
		f := t.Field(i)
		v := f.Tag.Get(attr)
		if existTag(v, exptTags) {
			continue
		}
		tagFieldMap[v] = f
		fieldNames = append(fieldNames, f.Name)
		q.WriteString(v)
		if i < numField-1 {
			q.WriteString(",")
		}
		colLen++
	}
	q.WriteString(") VALUES ")

	numBulk := len(sts)
	for i := 0; i < numBulk; i++ {
		q.WriteString("()")
		if i < numBulk-1 {
			q.WriteString(",")
		}
	}

	return q.String()
}
*/
/*
// SelectQuery : 構造体からSELECT文を生成する。
func SelectQuery(con *sql.DB, st interface{}, idColName, attr string, exptTags []string, fromAndAfter string, args ...interface{}) (ids []int64, sts []interface{}, err error) {
	existTag := func(tag string) bool {
		if tag == idColName || tag == "-" || tag == "" {
			return true
		}
		if exptTags != nil {
			for _, exptTag := range exptTags {
				if tag == exptTag {
					return true
				}
			}
		}
		return false
	}

	colLen := 0
	t := reflect.TypeOf(st)
	var q strings.Builder
	q.WriteString("SELECT ")
	fieldNames := make([]string, 0)
	tagFieldMap := map[string]reflect.StructField{}
	if idColName != "" {
		fieldNames = append(fieldNames, idColName)
		q.WriteString(idColName)
		q.WriteString(",")
		colLen++
	}
	num := t.NumField()
	for i := 0; i < num; i++ {
		f := t.Field(i)
		v := f.Tag.Get(attr)
		if existTag(v) {
			continue
		}
		tagFieldMap[v] = f
		fieldNames = append(fieldNames, f.Name)
		q.WriteString(v)
		if i < num-1 {
			q.WriteString(",")
		}
		colLen++
	}
	q.WriteString(" ")
	q.WriteString(fromAndAfter)
	s := q.String()
	log.Println(s)

	var rows *sql.Rows
	if len(args) == 0 {
		rows, err = con.Query(s)
	} else {
		rows, err = con.Query(s, args)
	}
	if err != nil {
		return
	}

	if idColName != "" {
		ids = make([]int64, 0)
	}
	for rows.Next() {
		row := make([]interface{}, colLen)
		rowp := make([]interface{}, colLen)
		for i := 0; i < colLen; i++ {
			rowp[i] = &row[i]
		}

		if err = rows.Scan(rowp...); err != nil {
			return
		}

		st := reflect.New(t).Interface()
		stv := reflect.ValueOf(st)
		for i, fieldName := range fieldNames {
			row := rowp[i].(*interface{})
			if i == 0 && fieldName == idColName {
				id := (*row).(int64)
				ids = append(ids, id)
			} else {
				if row == nil || (*row) == nil {
					continue
				}
				v := stv.Elem().FieldByName(fieldName)
				switch v.Type().Kind().String() {
				case "int":
					v.Set(reflect.ValueOf((int)((*row).(int64))))
				case "time.Time":
				default:
					v.Set(reflect.ValueOf(*row))
				}
			}
		}
		sts = append(sts, st)
	}

	return
}
*/
