package util

import (
	"database/sql"
	"hospital/conf"
	"hospital/db"
)

// Manager : 共通情報を管理する構造体
type Manager struct {
	hospID                          int64
	loginUserID                     int64
	loginFamilyName, loginFirstName string
	cfg                             *conf.HospConfig
	debugMsg                        string
}

// NewManager : 共通情報を管理する構造体を生成する。
func NewManager(hospID, loginUserID int64, loginFamilyName, loginFirstName string,
	cfg *conf.HospConfig, debugMsg string) Manager {
	return Manager{
		hospID:          hospID,
		loginUserID:     loginUserID,
		loginFamilyName: loginFamilyName,
		loginFirstName:  loginFirstName,
		cfg:             cfg,
		debugMsg:        debugMsg,
	}
}

// NewDB : DBコネクションを生成する。
func (m *Manager) NewDB() (*sql.DB, error) {
	return db.New(m.hospID)
}

// NewTx : DBトランザクションを生成する。
func (m *Manager) NewTx() (*db.TxManager, error) {
	return db.NewTx(m.hospID)
}

// EncryptID : IDを暗号化する。
func (m *Manager) EncryptID(id int64) string {
	return EncryptID(id)
}

// DecryptID : 暗号化IDを復号化する。
func (m *Manager) DecryptID(encID string) int64 {
	return DecryptID(encID)
}

// HospID : 病院IDを取得する。
func (m *Manager) HospID() int64 {
	return m.hospID
}

// LoginUserID : ログインユーザーidを取得する。
func (m *Manager) LoginUserID() int64 {
	return m.loginUserID
}

// LoginFamilyName : ログインユーザーの苗字を取得する。
func (m *Manager) LoginFamilyName() string {
	return m.loginFamilyName
}

// LoginFirstName : ログインユーザーの名前を取得する。
func (m *Manager) LoginFirstName() string {
	return m.loginFirstName
}

// Conf : 病院の設定情報を取得する。
func (m *Manager) Conf() *conf.HospConfig {
	return m.cfg
}

// DebugMsg : デバッグメッセージを取得する。
func (m *Manager) DebugMsg() string {
	return m.debugMsg
}

// SetDebugMsg : デバッグメッセージを設定する。
func (m *Manager) SetDebugMsg(msg string) {
	m.debugMsg = msg
}
