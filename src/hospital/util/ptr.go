package util

// ToPtr : 文字列のポインタを返却する。
func ToPtr(s string) *string {
	return &s
}
