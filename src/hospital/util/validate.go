package util

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

var (
	reTel = regexp.MustCompile("^[0-9]{2,4}[0-9]{2,4}-[0-9]{3,4}$")
)

// ValidationTel : 電話番号のバリデーションを定義する。
func ValidationTel(fl validator.FieldLevel) bool {
	return reTel.MatchString(fl.Field().String())
}
