package util

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

// RequestPdf : PDF構造体
type RequestPdf struct {
	body string
}

// NewRequestPdf : 新PDF構造体
func NewRequestPdf(body string) *RequestPdf {
	return &RequestPdf{
		body: body,
	}
}

// ParseTemplate : テンプレートにPDFデータを埋め込み
func (r *RequestPdf) ParseTemplate(templateFileName string, data interface{}) error {
	// テンプレートの読み込み
	funcs := template.FuncMap{
		"nl2br": func(text string) template.HTML {
			return template.HTML(strings.Replace(template.HTMLEscapeString(text), "\n", "<br>", -1))
		},
	}

	fileBytes, err := ioutil.ReadFile(templateFileName)
	if err != nil {
		log.Fatalf("Error while reading file data into byte array %s ", err)
	}

	t, err := template.New("templateFileName").Funcs(funcs).Parse(string(fileBytes))

	if err != nil {
		return err
	}

	// テンプレートにPDFデータを埋め込み
	buf := new(bytes.Buffer)
	if err := t.ExecuteTemplate(buf, "templateFileName", data); err != nil {
		return err
	}
	r.body = buf.String()
	return nil
}

// GeneratePDF : PDF作成
func (r *RequestPdf) GeneratePDF(pdfPath string) (bool, error) {
	t := time.Now().Unix()
	html := "./assets/dist/pdf/" + strconv.FormatInt(int64(t), 10) + ".html"

	// PDF変換対象のデータを書き込む一時HTMLファイルを作成
	err1 := ioutil.WriteFile(html, []byte(r.body), 0644)
	if err1 != nil {
		panic(err1)
	}

	// 一時HTMLファイルを確認
	f, err := os.Open(html)
	if f != nil {
		defer f.Close()
	}
	if err != nil {
		panic(err)
	}

	// PDFジェネレータの生成
	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		panic(err)
	}

	// グローバルオプションを設定
	pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)
	pdfg.Dpi.Set(300)

	// PDFジェネレータに対して変換対象となる上記一時HTMLファイルのPage型を設定
	pdfg.AddPage(wkhtmltopdf.NewPageReader(f))

	// 内部バッファにPDFを作成
	err = pdfg.Create()
	if err != nil {
		panic(err)
	}

	// バッファのPDF内容をディスク上のファイルに出力
	err = pdfg.WriteFile(pdfPath)
	if err != nil {
		panic(err)
	}

	// 一時HTMLファイルを削除
	e := os.Remove(html)
	if e != nil {
		panic(e)
	}

	// 終了
	return true, nil
}
