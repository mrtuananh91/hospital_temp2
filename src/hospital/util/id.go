package util

import (
	"crypto/des"
	crand "crypto/rand"
	"encoding/binary"
	"math/rand"
	"strconv"
	"strings"
)

func init() {
	var seed int64
	binary.Read(crand.Reader, binary.LittleEndian, &seed)
	rand.Seed(seed)
}

var _encIDStrMap = map[string]string{
	"0": "A",
	"1": "B",
	"l": "C",
	"o": "D",
}
var _decIDStrMap = map[string]string{
	"A": "0",
	"B": "1",
	"C": "l",
	"D": "o",
}

var _key = []byte{0xdd, 0xa4, 0xe1, 0x66, 0x2d, 0x48, 0x86, 0x63}

// EncryptID : IDを暗号化する。
func EncryptID(id int64) string {
	cb, _ := des.NewCipher(_key)
	in := make([]byte, 8)
	out := make([]byte, 8)
	binary.BigEndian.PutUint64(in, uint64(id))
	cb.Encrypt(out, in)
	s := strconv.FormatUint(binary.BigEndian.Uint64(out), 36)
	for k, v := range _encIDStrMap {
		s = strings.ReplaceAll(s, k, v)
	}
	return s
}

// DecryptID : IDを復号化する。
func DecryptID(s string) int64 {
	if s == "" {
		return 0
	}
	for k, v := range _decIDStrMap {
		s = strings.ReplaceAll(s, k, v)
	}
	encID, err := strconv.ParseUint(s, 36, 64)
	if err != nil {
		return 0
	}
	cb, _ := des.NewCipher(_key)
	in := make([]byte, 8)
	out := make([]byte, 8)
	binary.BigEndian.PutUint64(in, encID)
	cb.Decrypt(out, in)
	return int64(binary.BigEndian.Uint64(out))
}

const (
	_randChars        = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789"
	_randCharsIdxBits = 6
	_randCharsIdxMask = 1<<_randCharsIdxBits - 1
	_randCharsIdxMax  = 63 / _randCharsIdxBits
)

// GenRandomString : n文字のランダム文字列を生成する。
func GenRandomString(n int) string {
	b := make([]byte, n)
	cache, remain := rand.Int63(), _randCharsIdxMax
	for i := n - 1; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), _randCharsIdxMax
		}
		idx := int(cache & _randCharsIdxMask)
		if idx < len(_randChars) {
			b[i] = _randChars[idx]
			i--
		}
		cache >>= _randCharsIdxBits
		remain--
	}
	return string(b)
}
