import * as Moment from "moment";
import {CmnDate} from "./cmn-date";

export class MCHTMLElement extends HTMLElement {
	public static SS_MENU_SIDE = "menu-side";
	public static SS_MENU_BUTTON = "menu-button";
	public static SS_MENU_TAB = "menu-tab";
	public static SS_PATIENT_ID = "patient-id";
	public static SS_USER_NAME = "user-name";

	private static bodyElem = <HTMLBodyElement>document.querySelector("body");

	protected parentTarget: any;
	protected requestCode = 0;

	protected constructor(html: string, menuBtnTagMap: { [key: string]: string; } | null = null, menuTabTagMap: { [key: string]: string } | null = null) {
		super();

		this.innerHTML = html;

		const self = this;
		if (menuBtnTagMap) {
			for (let menuBtn in menuBtnTagMap) {
				this.$id(menuBtn).addEventListener("click", (evt: Event) => {
					evt.preventDefault();
					for (let [menuBtn, tag] of Object.entries(menuBtnTagMap)) {
						self.$id(menuBtn).classList.remove("active");
						const tagElem = self.$tag(tag);
						tagElem.style.display = "none";
						tagElem.removeAttribute("open");
					}

					const activeId = <string>(<HTMLElement>evt.target).getAttribute("id");
					self.$id(activeId).classList.add("active");
					const tagElem = self.$tag(menuBtnTagMap[activeId]);
					tagElem.style.removeProperty("display");
					tagElem.setAttribute("open", "true");

					sessionStorage.setItem(MCHTMLElement.SS_MENU_BUTTON, activeId);
					if (history.state != activeId) {
						history.pushState(activeId, "");
					}
				});
			}
		}

		if (menuTabTagMap) {
			for (let menuTab in menuTabTagMap) {
				this.$id(menuTab).addEventListener("click", (evt: Event) => {
					evt.preventDefault();
					for (let tag of Object.values(menuTabTagMap)) {
						self.$tag(tag).removeAttribute("open");
					}
					const activeId = <string>(<HTMLElement>evt.target).getAttribute("id");
					self.$tag(menuTabTagMap[activeId]).setAttribute("open", "true");
					sessionStorage.setItem(MCHTMLElement.SS_MENU_TAB, activeId);
				});
			}
		}
	}

	protected get random(): number {
		return (new Date()).getTime();
	}

	protected $id<T extends HTMLElement>(elemId: string, parElem: HTMLElement | null = this): T {
		return <T>(parElem === null ? document : parElem).querySelector(`#${elemId}`);
	}


	protected removeElemByClass(t:string):void{
		let n = this.querySelectorAll(t) as NodeListOf<HTMLElement>;
		n.forEach(function(e){
			e.remove();
	 });
	}

	protected hideElemByClass(t:string):void{
		let n = this.querySelectorAll(t) as NodeListOf<HTMLElement>;
		n.forEach(function(e){
			e.style.setProperty("display","none");
	 });
	}

	protected deleteFormDataByClass(t:string):void{
		let n = this.querySelectorAll(t) as NodeListOf<HTMLElement>;
		n.forEach(function(e){
			e.nodeValue="";
	 });
	}

	protected changeContent(change_id:string,class_name:string):void{
		this.hideElemByClass(`.${class_name}`);
		this.$id(change_id,<HTMLElement>this).style.setProperty("display","block");
	}

	protected $tag<T extends HTMLElement>(tagName: string, parElem: HTMLElement | null = this): T {
		return <T>(parElem === null ? document : parElem).querySelector(tagName);
	}

	protected $queryAll<T extends HTMLElement>(selector: string, parElem: HTMLElement | null = this): NodeListOf<T> {
		return (parElem === null ? document : parElem).querySelectorAll(selector);
	}

	protected get patientId(): string | null {
		const patientId = sessionStorage.getItem(MCHTMLElement.SS_PATIENT_ID);
		return patientId ? patientId : null;
	}

	protected get h13nId(): string | null {
		const h13nsDayElem: HTMLSelectElement | null = this.$id<HTMLSelectElement>("pt-h13ns-day", null);
		return h13nsDayElem ? h13nsDayElem.value : null;
	}

	protected get userName(): string | null {
		const userName = sessionStorage.getItem(MCHTMLElement.SS_USER_NAME);
		return userName;
	}

	protected currentMenuBtn(defaultMenuBtn: string): string {
		let s: string | null;
		if (MCHTMLElement.bodyElem.getAttribute("data-reload-menu-button") === "true") {
			MCHTMLElement.bodyElem.removeAttribute("data-reload-menu-button");
			s = sessionStorage.getItem("menu-button");
			if (!s) s = defaultMenuBtn;
		} else {
			s = defaultMenuBtn;
		}
		return s;
	}

	protected currentMenuTab(defaultMenuTab: string): string {
		let s: string | null;
		if (MCHTMLElement.bodyElem.getAttribute("data-reload-menu-tab") === "true") {
			MCHTMLElement.bodyElem.removeAttribute("data-reload-menu-tab");
			s = sessionStorage.getItem("menu-tab");
			if (!s) s = defaultMenuTab;
		} else {
			s = defaultMenuTab;
		}
		return s;
	}

	protected blockStart(): void {

	}

	protected blockEnd(): void {

	}

	protected present(id: string, data: any = null){
		let ele = this.$id(id, null);
		if (ele != null){
			ele.style.display = 'block';
			ele.setAttribute('open', 'true');
			if( ele instanceof MCHTMLElement){
				ele.parentTarget = this;
				if(data){
					ele.presentHandler(this.tagName.toLowerCase(), data);
				}
			}
		}else{
			console.error(`notFound: id ${id}`);
		}

	}
	protected dismiss(id: string = '', data: any = null){
		var ele = id == '' ? this : this.$id(id, null);
		if (ele != null){
			this.style.display = 'none';
			this.removeAttribute('open');
			if(this.parentTarget !== undefined && this.parentTarget instanceof MCHTMLElement && data){
				this.parentTarget.presentHandler(this.tagName.toLowerCase(), data);
			}
		}else{
			console.error(`notFound: id ${id}`);
		}
	}

	presentHandler(tagName: string, data: any){}

	antiXSS(text: string){
		return text.replaceAll('"', '&quot;').replaceAll('<', '&lt;').replaceAll('>', '&gt;');
	}
	fillDataToTempV2(temp: string, antiXSS: boolean, object: any) {
		if(object == null){
			temp = temp.replace(/{[a-zA-Z0-9]+}/g, '');
			return temp;
		}
		for (let [key, value] of Object.entries(object)) {
			if (antiXSS) {
				temp = temp.replaceAll(`{${key}}`, this.antiXSS(`${value}`));
			} else {
				temp = temp.replaceAll(`{${key}}`, this.antiXSS(`${value}`));
			}
		}
		return temp;
	}


	fillDataToTemp(temp: string, antiXSS: boolean, ...val: any[]) {
		for (let index = 0; index < val.length; index++) {
			if (antiXSS) {

				temp = temp.replaceAll(`{${index}}`, this.antiXSS(`${val[index]}`));
			} else {
				temp = temp.replaceAll(`{${index}}`, val[index]);
			}
		}
		return temp;
	}

	public static showLoading(isShow: boolean = true){
		var elem = MCHTMLElement.bodyElem.querySelector('#mc-loading');
		if(elem && elem instanceof HTMLElement){
			elem.style.display = isShow ? 'flex': 'none';
		}else{
			alert("zxczxc");
		}
	}

	public static toastFunc: (msg: string) => void;
	protected toast(msg: string) {
		MCHTMLElement.toastFunc(msg);
	}

	// json生年月日から生年月日文字列に変換する。
	protected birthday(s: string): [string, Moment.Moment] {
		const m = Moment(s);
		const dts = new Intl.DateTimeFormat('ja-JP-u-ca-japanese', { era: 'long' }).formatToParts(m.toDate());
		let era = "", year = "";
		for (let dt of dts) {
			switch (dt.type) {
				case "era":
					era = dt.value;
					break;
				case "year":
					year = dt.value;
					break;
				default:
					break;
			}
		}
		s = m.format(`${era}${year}年M月D日`);
		return [s, m];
	}

	// 日付表示用の文字列を出力
	protected dateFormat(date: string | Moment.Moment, type: string = "-", ymd: string = "ymd"): string {
		const ymdSymbol: any = {
			"ja": ["年", "月", "日"],
			"/": ["/", "/", ""],
			"-": ["-", "-", ""]
		};
		if (typeof date === "string")
			date = date !== "" ? Moment(date) : Moment();
		let format = "";
		//年月日か月日の場合分け
		switch (ymd) {
			case "ymd":
				format = `YYYY${ymdSymbol[type][0]}MM${ymdSymbol[type][1]}DD${ymdSymbol[type][2]}`;
				break;
			case "md":
				format = `MM${ymdSymbol[type][1]}DD${ymdSymbol[type][2]}`;
				break;
			default:
				break;
		}

		return date.format(format);
	}

	protected timeFormat(time: string | Moment.Moment, dbFormat: boolean = false): string {
		if (typeof time === "string")
			time = time !== "" ? Moment(time, "HH:mm") : Moment();
		if (dbFormat)
			return time.format();
		return time.format("HH:mm");
	}

	protected dateTimeFormat(datetime: string | Moment.Moment): string {
		if (typeof datetime === "string")
			datetime = datetime !== "" ? Moment(datetime) : Moment();
		return this.dateFormat(datetime) + " " + this.timeFormat(datetime);
	}

	protected week(date: string | Moment.Moment, option: string = "long"): string {
		const customOptions: any = {
			"parentheses": { option: "short", prefix: "(", suffix: ")" }
		};
		var isCustom: boolean = false
		if (customOptions.hasOwnProperty(option)) {
			isCustom = true;
			var selectOption: any = customOptions[option]
			option = selectOption["option"]
		}
		if (typeof date === "string") {
			date = Moment(date);
		}
		const dts = new Intl.DateTimeFormat('ja-JP-u-ca-japanese', { weekday: option }).formatToParts(date.toDate());
		let week = "";

		for (let dt of dts) {
			if (dt.type === "weekday") week = dt.value;
		}
		if (isCustom)
			week = selectOption["prefix"] + week + selectOption["suffix"]
		return week;
	}

	// 生年月日から年齢を算出する。
	protected age(birthday: string | Moment.Moment): number {
		if (typeof birthday === "string") {
			birthday = Moment(birthday);
		}
		return Moment().diff(birthday, "years");
	}

	// json性別値から性別文字列に変換する。
	protected gender(no: number): string {
		return no === 1 ? "男" : no === 2 ? "女" : "不明";
	}

	// json日時から日時文字列に変換する。
	protected datetime(s: string): string {
		return Moment(s).format("YYYY/M/D HH:mm");
	}

	// 患者画面の「入院日」を切り替えた際のイベント登録
	protected static prevEvtFunc: ((evt: Event | null) => void) | null = null;
	protected setChangeH13nDayEvent(evtFunc: (evt: Event | null) => void) {
		const h13nsDayElem = this.$id("pt-h13ns-day", null);
		if (MCHTMLElement.prevEvtFunc) {
			h13nsDayElem.removeEventListener("change", MCHTMLElement.prevEvtFunc);
		}

		h13nsDayElem.addEventListener("change", evtFunc.bind(this));
		MCHTMLElement.prevEvtFunc = evtFunc;
	}

	// クラスに定義したHTMLElement型メンバ変数（変数名は"**Elem"）の値を初期化する。
	protected reElemName = /.+Elem$/g;
	protected initElems() {
		const names = Object.getOwnPropertyNames(this);
		for (let name of names) {
			if (name.match(this.reElemName)) {
				const elem = <HTMLElement>(this as any)[name];
				if (elem instanceof HTMLInputElement) {
					elem.value = "";
					elem.checked = false;
				} else if (elem instanceof HTMLTextAreaElement || elem instanceof CmnDate) {
					elem.value = "";
				} else if (elem instanceof HTMLSelectElement) {
					elem.selectedIndex = 0;
				} else if (elem instanceof HTMLFormElement) {
				} else {
					elem.textContent = "";
				}
			}
		}
	}

	protected errorMsg(res: Response): void {
		switch (res.status) {
			case 404:
				alert("認証エラー");
				location.href = "/";
				break;
			case 400:
				res.json().then((json) => {
					const errs = json["errors"];
					let errMsg = "";
					try {
						for (let err of errs) {
							errMsg += JSON.stringify(err) + "\n";
						}
						alert(`通信エラー(${errMsg})`);
					} catch (e) {
						alert(`通信エラー`);
					}
				});
				break;
			default:
				alert(`通信エラー`);
				break;
		}
	}
}
