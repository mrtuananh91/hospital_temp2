import { MCHTMLElement } from "./basic";

import html from "./home-home.html";

export class HomeHome extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue != "true") {
			return;
		}
	}
}