import { MCHTMLElement } from "./basic";
import Quagga from 'quagga';

import html from "./order-inchg-barcode.html";
import { API } from "./api";

export class OrderInchgBarcode extends MCHTMLElement {

    private patientID: string = '';
    private step: number = 1;
    private mIndex: string = '';
    private startDate: string = '';
    private orderType: string = '2';
    private orderID: string = '';
    private floorID: string = '';
    private injectMedCodes: string[] = [];
    private orcaID: string = '';
    private stepCurr: string = ''

    private scannedLists: string[] = [];
    private status: boolean = true;

    private isStoped = false;
    private processing = false;

    public constructor() {
        super(html);
        this.bindAction();
    }

    public static get observedAttributes(): string[] {
        return ["open"];
    }

    public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
        if (attrName !== "open" || newValue !== "true") {
            this.stop();
            return;
        }
        this.start();
    }

    presentHandler(tagName: string, data: any) {
        switch (tagName) {
            case 'mc-order-inchg-shot':
                console.log(data);
                this.patientID = data.patientID;
                this.mIndex = data.index;
                this.step = data.step;
                this.stepCurr = `${data.step}`;
                this.startDate = data.startDate;
                this.orderID = data.orderID;
                this.floorID = data.floorID;
                this.status = data.status;
                this.injectMedCodes = data.injectMedCodes;
                this.orcaID = data.orcaID;
                this.scannedLists = [];
                break;
        }
    }

    bindAction() {
        this.$id('mc-order-ingch-barcode-btn-back').addEventListener('click', () => {
            this.dismiss();
        });
    }

    start() {
        this.isStoped = false;

        Quagga.init({
            inputStream: {
                name: "Live",
                type: "LiveStream",
                target: this.$id('mc-order-ingch-barcode-target')
            },
            decoder: {
                readers: ['code_128_reader', 'ean_reader', 'ean_8_reader']
            }
        }, (err: Error) => {
            if (this.isStoped) {
                Quagga.stop();
                return true;
            }
            if (err) {
                this.dismiss();
                return
            }

            Quagga.start();
        });

        Quagga.onDetected((result: any) => {
            // Validate barcode
            if(!this.processing){
                this.processing = true;
                setTimeout(() => {this.processing = false;}, 3000); // 読み取れば一旦３秒を待つ
                this.barcodeChecking(result.codeResult.code);
            }
        });
    }

    stop() {
        this.isStoped = true;
        Quagga.stop();
    }

    barcodeChecking(barcode: string) {
        // Show loading
        let videoEle = this.$id<HTMLVideoElement>('mc-order-ingch-barcode-target').querySelector('video');
        videoEle?.pause();
        switch (this.step) {
            case 1:
                this.step1Handler(barcode, videoEle);
                break;
            case 2:
                this.step2Handler(barcode, videoEle);
                break;
            case 3:
                this.step3Handler(barcode, videoEle);
                break;
        }

    }

    step1Handler(barcode: string, videoEle: HTMLVideoElement | null) {
        let status = this.validateMedBarcode(barcode);
        if (status === 'ng') {
            alert(`ID: ${barcode} \n薬剤のバーコードが正しくありません。`);
        } else if (status === 'scanned') {
            alert(`ID: ${barcode} \nこちらの薬剤のバーコードを読み取りました。他の薬剤バーコードをスキャンしてください。`)
        } else if (status === 'done') {
            this.submitData(videoEle);
            return;
        } else {
            alert(`ID: ${barcode} \n次の薬剤のバーコードをスキャンしてください。`);
        }
        videoEle?.play();
    }

    step2Handler(barcode: string, videoEle: HTMLVideoElement | null) {
        if (this.stepCurr == '2') {
            if (this.orderID != barcode) {
                alert(`ID: ${barcode} \nバーコードシールが正しくありません。`);
            } else {
                alert(`ID: ${barcode} \n薬剤のバーコードをスキャンしてください。`);
                this.stepCurr = '2-1';
            }
            videoEle?.play();
        } else {
            // 薬剤バーコードをスキャン
            this.step1Handler(barcode, videoEle);
        }

    }

    step3Handler(barcode: string, videoEle: HTMLVideoElement | null) {
        if (this.stepCurr === '3') {
            if (this.orcaID != barcode) {
                alert(`ID: ${barcode} \n患者のバーコードが正しくありません。`);
            } else {
                alert(`ID: ${barcode} \nバーコードシールをスキャンしてください。`);
                this.stepCurr = '3-1';
            }
            videoEle?.play();
        } else {
            if (this.orderID != barcode) {
                alert(`ID: ${barcode} \nバーコードシールが正しくありません。`);
                videoEle?.play();
            } else {
                this.submitData(videoEle);

            }
        }
    }

    validateMedBarcode(barcode: string) {
        // 既に読み取ったバーコードがスキャンされた場合
        if (this.scannedLists.includes(barcode)) {
            return 'scanned';
        }
        // バーコードが正しくない場合
        if (!this.injectMedCodes.includes(barcode)) {
            return 'ng';
        }
        this.scannedLists.push(barcode);
        if (this.equal(this.scannedLists, this.injectMedCodes)) {
            // Submit
            return 'done';
        }

        return 'accept';
    }

    submitData(videoEle: HTMLVideoElement | null) {
        MCHTMLElement.showLoading();
        API.barcodeValidate({
            floor_id: this.floorID,
            order_id: this.orderID,
            step: this.step,
            start_date: this.startDate,
            order_type: this.orderType,
            patient_id: this.patientID,
            status: this.status,
        }).then(async (res) => {
            MCHTMLElement.showLoading(false);
            if (res.ok) {
                const json = await res.json();
                this.dismiss('', {
                    index: this.mIndex,
                    step: this.step,
                    patientID: this.patientID,
                    status: this.status,
                    body: json,
                });
            }
            else {
                // alert('err:err');
                this.scannedLists = [];
            }
        }).catch((err) => {
            console.log(err);
        }).finally(() => {
            videoEle?.play();
            MCHTMLElement.showLoading(false);
        });
    }

    equal(arr1: Array<string>, arr2: Array<string>): boolean {
        if (!arr2) return false;
        if (arr1.length != arr2.length) return false;

        arr1.sort();
        arr2.sort();

        for (var i = 0, l = arr1.length; i < l; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }
}
