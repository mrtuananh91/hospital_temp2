import { MCHTMLElement } from "./basic";
import { API,H13 } from "./api";

import html from "./pt-sum-hphistory.html";

export class PtSumHpHistory extends MCHTMLElement {
	private formElem: HTMLFormElement;
    private encId:HTMLInputElement;
    private encPatientId: string;
    private startDateTime:HTMLInputElement;
    private endDateTime:HTMLInputElement;
    private hospStartAge:HTMLInputElement;
    private hospEndAge:HTMLInputElement;
    private hospWay:HTMLInputElement;
    private hospWayDet:HTMLInputElement;
    private hospFacility:HTMLInputElement;
    private hospFacilityDet:HTMLInputElement;
    private hospCondition:HTMLInputElement;
    private lifeIndependence:HTMLInputElement;
    private startBedsore:HTMLInputElement;
    private startBedsoreComment:HTMLInputElement;
    private endBedsore:HTMLInputElement;
    private endBedsoreComment:HTMLInputElement;
    private hospBt:HTMLInputElement;
    private hospBph:HTMLInputElement;
    private hospBpl:HTMLInputElement;
    private hospSpo2:HTMLInputElement;
    private vitalComment:HTMLInputElement;
	private note:HTMLInputElement;
	public constructor() {
		super(html);
		const self = this
		this.$id("pt-sum-hp-new").addEventListener("click", async (evt: Event) => {
							let enc_id_elem = <HTMLInputElement>self.$id("pt-sum-hp-enc_id")
							enc_id_elem.value =  "-1"
		             		self.startDateTime.value = ""
		               		self.endDateTime.value   = ""
		               		self.hospStartAge.value  = ""
		               		self.hospEndAge.value	 = ""
		               		self.hospWay.value		 = ""
		               		self.hospWayDet.value	 = ""
		               		self.hospFacility.value  = ""
							self.hospFacilityDet.value 	= ""
		           		   	self.hospCondition.value  	= ""
		           			self.lifeIndependence.value = ""
		               		self.startBedsore.value  	= ""
		        			self.startBedsoreComment.value = ""
		            		self.endBedsore.value 			= ""
		          			self.endBedsoreComment.value	= ""
		               		self.hospBt.value				= ""
		               		self.hospBph.value				= ""
		               		self.hospBpl.value				= ""
		               		self.hospSpo2.value				= ""
		               		self.vitalComment.value 		= ""
                 		    self.note.value                 = ""
		});
		this.formElem = this.$id("pt-sum-hp-form");
		this.encId= this.$id("pt-sum-hp-enc_id");
		this.encPatientId= <string>this.patientId;
		this.startDateTime = this.$id("pt-sum-hp-start_datetime");
		this.endDateTime = this.$id("pt-sum-hp-end_datetime");
		this.hospStartAge = this.$id("pt-sum-hp-hosp_start_age");
		this.hospEndAge = this.$id("pt-sum-hp-hosp_end_age");
		this.hospWay = this.$id("pt-sum-hp-hosp_way");
		this.hospWayDet = this.$id("pt-sum-hp-hosp_way_det");
		this.hospFacility = this.$id("pt-sum-hp-hosp_facility");
		this.hospFacilityDet = this.$id("pt-sum-hp-hosp_facility_det");
		this.hospCondition = this.$id("pt-sum-hp-hosp_condition");
		this.lifeIndependence = this.$id("pt-sum-hp-life_independence");
		this.startBedsore = this.$id("pt-sum-hp-start_bedsore");
		this.startBedsoreComment = this.$id("pt-sum-hp-start_bedsore_comment");
		this.endBedsore = this.$id("pt-sum-hp-end_bedsore");
		this.endBedsoreComment = this.$id("pt-sum-hp-end_bedsore_comment");
		this.hospBt = this.$id("pt-sum-hp-hosp_bt");
		this.hospBph = this.$id("pt-sum-hp-hosp_bph");
		this.hospBpl = this.$id("pt-sum-hp-hosp_bpl");
		this.hospSpo2 = this.$id("pt-sum-hp-hosp_spo2");
		this.vitalComment = this.$id("pt-sum-hp-vital_comment");
		this.note = this.$id("pt-sum-hp-note");
		this.setSubmitEvent();
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}
    private setSubmitEvent():void{
        const self = this;
		this.formElem.addEventListener("submit", async (evt: Event) => {
			evt.stopPropagation();
			evt.preventDefault();

			self.blockStart();
			try {
                let h13 = new H13();
				h13.encId = self.encId.value;
                h13.encPatientId        = self.encPatientId
		        h13.startDateTime       = self.startDateTime.value.split("/").join("-")
				h13.endDateTime         = self.endDateTime.value.split("/").join("-")
				h13.endDateTime 		= h13.endDateTime == ""? null : h13.endDateTime
		        h13.hospStartAge        = parseInt(self.hospStartAge.value)
		        h13.hospEndAge          = parseInt(self.hospEndAge.value)
		        h13.hospWay             = parseInt(self.hospWay.value)
		        h13.hospWayDet          = self.hospWayDet.value
		        h13.hospFacility        = parseInt(self.hospFacility.value)
		        h13.hospFacilityDet     = self.hospFacilityDet.value
		        h13.hospCondition       = self.hospCondition.value
		        h13.lifeIndependence    = parseInt(self.lifeIndependence.value)
		        h13.startBedsore        = self.startBedsore.value
		        h13.startBedsoreComment = self.startBedsoreComment.value
		        h13.endBedsore          = self.endBedsore.value
		        h13.endBedsoreComment   = self.endBedsoreComment.value
		        h13.hospBt              = parseFloat(self.hospBt.value)
		        h13.hospBph             = parseInt(self.hospBph.value)
		        h13.hospBpl             = parseInt(self.hospBpl.value)
		        h13.hospSpo2            = parseInt(self.hospSpo2.value)
		        h13.vitalComment        = self.vitalComment.value
				h13.note                = self.note.value
				let res
				if (h13.encId == "-1"){
					res = await API.addHospitalization(h13);
				}else{
					res = await API.updateHospitalization(h13);
				}
				if (!res.ok) {
                    self.errorMsg(res);
                    return;
                }else{
					self.toast("入退院履歴を更新しました。");
					self.attributeChangedCallback("open","","true")
                }
			} finally {
				self.blockEnd();
			}
		});
	}
	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}
		// テーブル全削除
		this.$queryAll("tr[id^='pt-sum-hphistory-list-item'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});
		const self = this;
		(async () => {
            const res = await API.getHospitalization(<string>self.encPatientId);
			if (res.ok) {
				const json = await res.json();
				let srcElem = <HTMLTableRowElement>self.$id("pt-sum-hphistory-list-item");
				let prevElem = srcElem;
				const hps = json["hospitalization"]
				for(var i = 0;i<hps.length;i++){
					console.log(hps[i]);
					let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
					trElem.style.display = "table-row";
					trElem.setAttribute("id", `pt-sum-hphistory-list-item${i}`);
					let elem = self.$id("pt-sum-hphistory-num", trElem);
					elem.setAttribute("id", `pt-sum-hp-history-num${i}`);
					elem.textContent= String(i);
					elem = self.$id("pt-sum-hphistory-start_date_time", trElem);
					elem.setAttribute("id", `pt-sum-hphistory-start_date_time${i}`);
					elem.textContent =  hps[i]["start_date_time"].substr(0,10);
					elem = self.$id("pt-sum-hphistory-end_date_time", trElem);
					elem.setAttribute("id", `pt-sum-hphistory-end_date_time${i}`);
					elem.textContent =  hps[i]["end_date_time"]!=null?hps[i]["end_date_time"].substr(0,10):"";
					elem = self.$id("pt-sum-hphistory-num_day", trElem);
					elem.setAttribute("id", `pt-sum-hphistory-num_day${i}`);
					elem.textContent =  self.calculateNumDay(hps[i]["start_date_time"],hps[i]["end_date_time"]);
					elem = self.$id("pt-sum-hphistory-button", trElem);
					elem.setAttribute("id", `pt-sum-hphistory-button${i}`);
					elem.setAttribute("global_i", `${i}`);
					const hp = hps[i]
					elem.addEventListener("click", (evt: Event) => {
							let encIdElem = <HTMLInputElement>self.$id("pt-sum-hp-enc_id")
							encIdElem.value = <string>hp["enc_id"]
		             		self.startDateTime.value = hp["start_date_time"].substr(0,10)
		             		self.endDateTime.value = hp["end_date_time"]?hp["end_date_time"].substr(0,10):null
		               		self.hospStartAge.value  = hp["hosp_start_age"]
		               		self.hospEndAge.value  = hp["hosp_end_age"]
		               		self.hospWay.value		 = hp["hosp_way"]
		               		self.hospWayDet.value		 = hp["hosp_way_det"]
		               		self.hospFacility.value		 = hp["hosp_facility"]
		               		self.hospFacilityDet.value		 = hp["hosp_facility_det"]
		               		self.hospCondition.value		 = hp["hosp_condition"]
		           		   	self.lifeIndependence.value  	= hp["life_independence"]
		               		self.startBedsore.value  	= hp["start_bedsore"]
		        			self.startBedsoreComment.value = hp["start_bedsore_comment"]
		            		self.endBedsore.value 			= hp["end_bedsore"]
		          			self.endBedsoreComment.value	= hp["end_bedsore_comment"]
		               		self.hospBt.value				= hp["hosp_bt"]
		               		self.hospBph.value				= hp["hosp_bph"]
		               		self.hospBpl.value				= hp["hosp_bpl"]
		               		self.hospSpo2.value				= hp["hosp_spo2"]
		               		self.vitalComment.value 		= hp["vital_comment"]
							self.note.value                 = hp["note"]

					});
					(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
					prevElem = trElem;

				}
			} else {
				self.errorMsg(res);
			}
		})();
    }
    public calculateNumDay(startDate:string,endDate:string):string{
			if(!endDate){
				return "";
			}
            var tmpStartDate:string[] = startDate.split("-");
            var tmpEndDate:string[] = endDate.split("-");
            var startDateDate:Date = new Date(parseInt(tmpStartDate[0]),parseInt(tmpStartDate[1]),parseInt(tmpStartDate[2]));
            var endDateDate:Date = new Date(parseInt(tmpEndDate[0]),parseInt(tmpEndDate[1]),parseInt(tmpEndDate[2]));
            var termDay = (endDateDate.getTime() - startDateDate.getTime()) / 86400000;
            return String(termDay)
    }
}
