import * as Moment from "moment";

export class API {
	public static get random(): number {
		return (new Date()).getTime();
	}

	public static login(account: string, password: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/credential?_=${API.random}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				"account": account,
				"password": password,
			}),
		});
	}

	public static searchPatients(patientID: string, name: string = "", offset: number = 0, limit: number = 100, autoComp: boolean = false, all: boolean = false): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient?_=${API.random}` +
			`&patient_id=${encodeURIComponent(patientID)}` +
			`&name=${encodeURIComponent(name)}` +
			`&andDisabled=${all}&autoComp=${autoComp}` +
			`&offset=${offset}&limit=${limit}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}
	public static addPatients(pts: Patient[]): Promise<Response> {
		console.log(pts)
		return fetch(`/services/api/v2/hospital/patient?_=${API.random}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ "patients": pts }),
		});
	}

	public static addICCon(iccon: ICCon[]): Promise<Response> {
		return fetch(`/services/api/v2/hospital/iccon?_=${API.random}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ "iccon": iccon }),
		});

	}

	public static getUsers(): Promise<Response> {
		return fetch(`/services/api/v2/hospital/user?_=${API.random}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}

	public static getUser(userID: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/user?_=${API.random}` +
			`&userID=${userID}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}

	public static getRecordsByPatientId(patientID: string, recordType: number, h13nId: string, offset: number = 0, limit: number = 10): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/record?_=${API.random}` +
			`&EncPatientID=${encodeURIComponent(patientID)}` +
			`&RecordType=${recordType}` +
			`&Ench13nID=${encodeURIComponent(h13nId)}` +
			`&offset=${offset}&limit=${limit}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}

	// public static getRecordsImg(){
	// 	return fetch(`/services/api/v2/hospital/patient/record?_=${API.random}` +
	// 		`&EncPatientID=${encodeURIComponent(recordId)}` , {
	// 		method: "GET",
	// 		cache: "no-cache",
	// 		credentials: "same-origin"
	// 	});
	// }

	public static addRecord(rcd: Record[]): Promise<Response> {
		console.log("eeeeeeee", rcd);
		return fetch(`/services/api/v2/hospital/patient/record?_=${API.random}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ "record": rcd })
		});
	}
	public static updateRecord(rcd: Record[]): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/record?_=${API.random}`, {
			method: "PUT",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ "record": rcd })
		});
	}

	public static getHospitalization(patientID: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/hospitalization?_=${API.random}` +
			`&id=${encodeURIComponent(patientID)}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}
	public static addHospitalization(h13: H13): Promise<Response> {
		console.log(h13)
		return fetch(`/services/api/v2/hospital/patient/hospitalization?_=${API.random}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(h13),
		});
	}
	public static updateHospitalization(h13: H13): Promise<Response> {
		console.log(h13)
		return fetch(`/services/api/v2/hospital/patient/hospitalization?_=${API.random}`, {
			method: "PUT",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(h13),
		});
	}

	public static getRecords(patient_id: string, h13nId: string, recType: number = -1): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/karte/record?_=${API.random}` +
			`&patient_id=${patient_id}&h13n_id=${h13nId}&record_type=${recType}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}

	public static addKarte(patient_id: string, h13nId: string, title: string, substituteInput: boolean, diagDeptId: number, diagTypeId: number, actStartDate: string, actStartTime: string, body: string, memo: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/karte?_=${API.random}` +
			`&patient_id=${patient_id}&h13n_id=${h13nId}`, {
			method: "POST",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				"karte_title": title,
				"substitute_input": substituteInput,
				"diagdept_id": diagDeptId,
				"diagtype_id": diagTypeId,
				"act_start_datetime": Moment(`${actStartDate} ${actStartTime}`, "YYYY/MM/DD HH:mm").format(),
				"karte_body": body,
				"karte_memo": memo
			})
		});
	}

	public static updateKarte(karteId: string, title: string, substituteInput: boolean, diagDeptId: number, diagTypeId: number, actStartDate: string, actStartTime: string, body: string, memo: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/patient/karte?_=${API.random}`, {
			method: "PUT",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				"karte_id": karteId,
				"karte_title": title,
				"substitute_input": substituteInput,
				"diagdept_id": diagDeptId,
				"diagtype_id": diagTypeId,
				"act_start_datetime": Moment(`${actStartDate} ${actStartTime}`, "YYYY/MM/DD HH:mm").format(),
				"karte_body": body,
				"karte_memo": memo
			})
		});
	}

	public static getConf(): Promise<Response> {
		return fetch(`/services/api/v2/hospital/conf?_=${API.random}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});
	}

	public static getOrderOrderList(floor: string, selectedDate: string): Promise<Response> {
		return fetch(`/services/api/v2/order/order/list?_=${API.random}` +
			`& floor=${encodeURIComponent(floor)}` +
			`& selectedDate=${encodeURIComponent(selectedDate)}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin"
		});

	}

	public static printPdf(type: string, floorID: string, date: string ): Promise<Response> {
		return fetch(`/services/api/v2/hospital/pdf/order?_=${API.random}` +
			`&type=${encodeURIComponent(type)}` +
			`&floor_id=${floorID}` +
			`&date=${date}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin",
		});
	}

	public static getOrderInchgShot(floorId: string, date: string): Promise<Response> {
		return fetch(`/services/api/v2/hospital/order/incharge/inject?_=${API.random}` +
			`&floor_id=${floorId}` +
			`&date=${date}`, {
			method: "GET",
			cache: "no-cache",
			credentials: "same-origin",
		});
	}

	// barcode validate
	public static barcodeValidate(body: any): Promise<Response> {
		return fetch(`/services/api/v2/order/inchg/shot/barcode?_=${API.random}`,
			{
				method: "POST",
				cache: "no-cache",
				credentials: "same-origin",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(body)
			});
	}

	// orderInjectionUncheck
	public static orderInjectionUncheck(body: any): Promise<Response> {
		return fetch(`/services/api/v2/order/inchg/shot/uncheck?_=${API.random}`,
			{
				method: "PUT",
				cache: "no-cache",
				credentials: "same-origin",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(body)
			});
	}

	public static getFloorLists(): Promise<Response> {
		return fetch(`/services/api/v2/floor/list?_=${API.random}`,
			{
				method: "GET",
				cache: "no-cache",
				credentials: "same-origin"
			});
	}

	public static searchInchgEat(order_date: Date): Promise<Response> {
		//日付の整形
		const year_str: string = String(order_date.getFullYear());
		const month_str = String(1 + order_date.getMonth()).padStart(2, '0')	//月だけ+1すること
		const day_str = String(order_date.getDate()).padStart(2, '0')
		const order_date_str = year_str + "-" + month_str + "-" + day_str;
		console.log(order_date_str);

		//リクエストURL
		const url = `/services/api/v2/hospital/inchgeat?_=${API.random}` +
			`&order_date=${encodeURIComponent(order_date_str)}`;

		console.log(url)

		return fetch(
			url,
			{
				method: "GET",
				cache: "no-cache",
				credentials: "same-origin"
			}
		);
	}

	public static updateInchgEatStatus(order_act_id: number, status_no: number, checked: boolean): Promise<Response> {
		return fetch(`/services/api/v2/hospital/inchgeat?_=${API.random}`, {
			method: "PUT",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				"order_act_id": order_act_id,
				"status_no": status_no,
				"checked": checked
			}),
		});
	}

	public static searchInchgMed(order_date: Date): Promise<Response> {

		console.log("[Higuchi][api]searchInchgMed()");
		console.log("order_date ==" + order_date);

		//日付の整形
		const year_str: string = String(order_date.getFullYear());
		const month_str = String(1 + order_date.getMonth()).padStart(2, '0')	//月だけ+1すること
		const day_str = String(order_date.getDate()).padStart(2, '0')
		const order_date_str = year_str + "-" + month_str + "-" + day_str;
		console.log(order_date_str);

		//リクエストURL
		const url = `/services/api/v2/hospital/inchgmed?_=${API.random}` +
			`&order_date=${encodeURIComponent(order_date_str)}`;

		console.log(url)

		return fetch(
			url,
			{
				method: "GET",
				cache: "no-cache",
				credentials: "same-origin"
			}
		);
	}

	public static updateInchgMedStatus(order_act_id: number, status_no: number, checked: boolean): Promise<Response> {
		console.log("[Higuchi][api]updateInchgMedStatus() status_no ==" + status_no);
		console.log("[Higuchi][api]updateInchgMedStatus() checked ==" + checked);
		return fetch(`/services/api/v2/hospital/inchgmed?_=${API.random}`, {
			method: "PUT",
			cache: "no-cache",
			credentials: "same-origin",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				"order_act_id": order_act_id,
				"status_no": status_no,
				"checked": checked
			}),
		});
	}
}
class MovacalObject {
	// for JSON.stringify()。キャメル形式の変数名をスネーク形式に変換。
	public toJSON(): { [key: string]: any } {
		let h: { [key: string]: any } = {};
		for (let pn of Object.getOwnPropertyNames(this)) {
			if (pn === "toJSON") continue;
			const value = (this as any)[pn];
			const key = pn.replace(/([A-Z])/g, (s) => { return "_" + s.charAt(0).toLowerCase(); });
			h[key] = value == undefined ? null : value;
		}
		return h;
	}
}

export class Patient extends MovacalObject {
	public id!: string;
	public orcaId!: string;
	public familyName!: string;
	public firstName!: string;
	public familyNameKana!: string;
	public firstNameKana!: string;
	public gender!: number;
	public deadDatetime!: string;
	public birthday!: string | null;
	public bloodType!: string;
	public postalCode!: string | null;
	public country!: string | null;
	public address!: string | null;
	public tel!: string | null;
	public mobilephone!: string | null;
	public emgName1!: string | null;
	public emgName2!: string | null;
	public emgName3!: string | null;
	public emgTel1!: string | null;
	public emgTel2!: string | null;
	public emgTel3!: string | null;
	public emgMobilephone1!: string | null;
	public emgMobilephone2!: string | null;
	public emgMobilephone3!: string | null;
	public emgRel1!: string | null;
	public emgRel2!: string | null;
	public emgRel3!: string | null;
	public keyPersonName!: string | null;
	public keyPersonRel!: string | null;
	public familyInfo!: string | null;
	public diseaseHistory!: string | null;
	public familyDiseaseHistory!: string | null;
	public note!: string | null;
	public deadDateTime!: string | null;
}
export class Hospitalization extends MovacalObject {
	public id!: string;
	public orcaId!: string;
	public familyName!: string;
	public firstName!: string;
	public familyNameKana!: string;
	public firstNameKana!: string;
	public gender!: number;
	public deadDatetime!: string;
	public birthday!: string | null;
	public bloodType!: string;
	public postalCode!: string | null;
	public country!: string | null;
	public address!: string | null;
	public tel!: string | null;
	public mobilephone!: string | null;
	public emgName1!: string | null;
	public emgName2!: string | null;
	public emgName3!: string | null;
	public emgTel1!: string | null;
	public emgTel2!: string | null;
	public emgTel3!: string | null;
	public emgMobilephone1!: string | null;
	public emgMobilephone2!: string | null;
	public emgMobilephone3!: string | null;
	public emgRel1!: string | null;
	public emgRel2!: string | null;
	public emgRel3!: string | null;
	public keyPersonName!: string | null;
	public keyPersonRel!: string | null;
	public familyInfo!: string | null;
	public diseaseHistory!: string | null;
	public familyDiseaseHistory!: string | null;
	public note!: string | null;
	public deadDateTime!: string | null;
}
export class H13 extends MovacalObject {
	public encId!: string;
	public encPatientId!: string;
	public roomId!: string;
	public startDateTime!: string | null;
	public endDateTime!: string | null;
	public hospStartAge!: number;
	public hospEndAge!: number;
	public hospWay!: number;
	public hospWayDet!: string;
	public hospFacility!: number;
	public hospFacilityDet!: string;
	public hospCondition!: string;
	public lifeIndependence!: number;
	public startBedsore!: string;
	public startBedsoreComment!: string;
	public endBedsore!: string;
	public endBedsoreComment!: string;
	public hospBt!: number;
	public hospBph!: number;
	public hospBpl!: number;
	public hospSpo2!: number;
	public vitalComment!: string;
	public note!: string;
}

export class Record extends MovacalObject {
	public id!: string;
	public patientId!: string | null;
	public h13nId!: string | null;
	public recordType!: number | null;
	public actDate!: string | null;
	public status!: number | null;
	public actDatetime!: string | null;
	public actTime!: string | null | number;
	public actDetail!: string | null;
	public note!: string | null;
	public userName!: string | null;
	public userId!: string | null;
	public file1!: string | null;
	public file2!: string | null;
	public file3!: string | null;
	
}

export class ICCon extends MovacalObject {
	public id!: string;
	public orcaId!: string;
	public date!: string;
	public doctor!: string;
	public patient!: string;
	public title!: string;
	public content!: string;
}

