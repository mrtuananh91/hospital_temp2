import { MCHTMLElement } from "./basic";
import { PtKarteAddKarte } from "./pt-karte-add-karte";
import { PtKarteAddMed } from "./pt-karte-add-med";

import html from "./pt-karte-add.html";

export class PtKarteAdd extends MCHTMLElement {
	private karteElem: PtKarteAddKarte;
	private medElem: PtKarteAddMed;

	public constructor() {
		super(html);

		this.karteElem = this.$tag("mc-pt-karte-add-karte");
		this.medElem = this.$tag("mc-pt-karte-add-med");
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			this.karteElem.removeAttribute("open");
			this.medElem.removeAttribute("open");
			return;
		}

		this.karteElem.setAttribute("open", "true");
		this.medElem.setAttribute("open", "true");

		const self = this;
		this.setChangeH13nDayEvent((evt: Event | null) => {
			self.karteElem.removeAttribute("open");
			self.karteElem.setAttribute("open", "true");
			self.medElem.removeAttribute("open");
			self.medElem.setAttribute("open", "true");
		});
	}
}
