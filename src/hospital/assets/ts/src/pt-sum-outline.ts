import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt-sum-outline.html";

export class PtSumOutline extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.setChangeH13nDayEvent(this.show)
		this.show(null);
	}

	private async show(evt: Event | null) {
		const patientId = this.patientId;
		if (!patientId) return;

		const res = await API.searchPatients(<string>patientId);
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		const json = await res.json();
		const pts = json["patients"];
		const pt = pts[0];
		this.$id("pt-sum-outline-country").textContent = pt["country"];
		this.$id("pt-sum-outline-address").textContent = pt["address"];
		this.$id("pt-sum-outline-tel").textContent = pt["tel"];
		this.$id("pt-sum-outline-mobilephone").textContent = pt["mobilephone"];
		this.$id("pt-sum-outline-emg-name1").textContent = pt["emg_name1"];
		this.$id("pt-sum-outline-emg-tel1").textContent = pt["emg_tel1"];
		this.$id("pt-sum-outline-emg-mobilephone1").textContent = pt["emg_mobilephone1"];
		this.$id("pt-sum-outline-emg-rel1").textContent = pt["emg_rel1"];
		this.$id("pt-sum-outline-emg-name2").textContent = pt["emg_name2"];
		this.$id("pt-sum-outline-emg-tel2").textContent = pt["emg_tel2"];
		this.$id("pt-sum-outline-emg-mobilephone2").textContent = pt["emg_mobilephone2"];
		this.$id("pt-sum-outline-emg-rel2").textContent = pt["emg_rel2"];
		this.$id("pt-sum-outline-emg-name3").textContent = pt["emg_name3"];
		this.$id("pt-sum-outline-emg-tel3").textContent = pt["emg_tel3"];
		this.$id("pt-sum-outline-emg-mobilephone3").textContent = pt["emg_mobilephone3"];
		this.$id("pt-sum-outline-emg-rel3").textContent = pt["emg_rel3"];
		this.$id("pt-sum-outline-keyperson-name").textContent = pt["key_person_name"];
		this.$id("pt-sum-outline-keyperson-rel").textContent = pt["key_person_rel"];
		this.$id("pt-sum-outline-family-info").textContent = pt["family_info"];
		this.$id("pt-sum-outline-disease-history").textContent = pt["disease_history"];
		this.$id("pt-sum-outline-family-disease-history").textContent = pt["family_disease_history"];
		this.$id("pt-sum-outline-note").textContent = pt["note"];
		const h13nDays = pt["h13n_days"];
		if (h13nDays) {
			const h13nId = this.h13nId;
			for (let h13nDay of h13nDays) {
				if (h13nDay["id"] === h13nId) {
					this.$id("pt-sum-outline-hosp-datetime").textContent = this.datetime(h13nDay["start_datetime"]);
				}
			}
		}
	}
}