import { MCHTMLElement } from "./basic";

import html from "./pt-dt.html";

export class PtDt extends MCHTMLElement {
	private static menuTabTagMap = {
		"pt-dt-tab-1-1-tab": "mc-pt-dt-rec",
		"pt-dt-tab-1-2-tab": "mc-pt-dt-assess",
		"pt-dt-tab-1-3-tab": "mc-pt-dt-doc",
	};
	private static startTab = "pt-dt-tab-1-1-tab";

	public constructor() {
		super(html, null, PtDt.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(PtDt.startTab)).click();
	}
}