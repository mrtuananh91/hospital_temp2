import * as Moment from "moment";
import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt-list.html";

export class PtList extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.blockStart();
		try {
			this.show();
		} finally {
			this.blockEnd();
		}
	}

	private async show() {
		// テーブル全削除
		this.$queryAll("tr[id^='pt-list-item'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});

		const res = await API.searchPatients("", "", 0, -1, false, false);
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		const json = await res.json();
		const pts: any[] | null = json["patients"];
		if (!pts) return;

		let srcElem = <HTMLTableRowElement>this.$id("pt-list-item");
		let prevElem = srcElem;
		const self = this;
		pts.forEach((pt: { [key: string]: any }, i) => {
			i++;
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.display = "table-row";
			trElem.setAttribute("id", `pt-list-item${i}`);

			// ID
			let elem = self.$id("pt-list-id", trElem);
			elem.setAttribute("id", `pt-list-id${i}`);
			elem.textContent = pt["orca_id"];
			// 氏名
			elem = self.$id("pt-list-name", trElem);
			elem.setAttribute("id", `pt-list-name${i}`);
			elem.textContent = pt["family_name"] + pt["first_name"];
			elem.addEventListener("click", (evt: Event) => {
				sessionStorage.removeItem("menu-button");
				self.$tag("mc-pt", null).style.display = "none";
				const pt2Elem = self.$tag("mc-pt2", null);
				pt2Elem.setAttribute("open", pt["patient_id"]);
				pt2Elem.style.removeProperty("display");
			});
			// 生年月日
			elem = self.$id("pt-list-birthday", trElem);
			elem.setAttribute("id", `pt-list-birthday${i}`);
			const [s, birthday] = self.birthday(pt["birthday"]);
			elem.textContent = s;
			// 年齢
			elem = self.$id("pt-list-age", trElem);
			const age = self.age(birthday);
			elem.setAttribute("id", `pt-list-age${i}`);
			elem.textContent = `${age}歳`;
			// 性別
			elem = self.$id("pt-list-gender", trElem);
			elem.setAttribute("id", `pt-list-gender${i}`);
			elem.textContent = self.gender(pt["gender"]);

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});
	}
}
