import { MCHTMLElement } from "./basic";
import { API } from "./api";
import html from "./order-inchg-eat.html";

export class OrderInchgEat extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.show();

	}

	private async show() {

		let today = new Date();
		const res = await API.searchInchgEat(today);

		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		// テーブル全削除
		this.$queryAll("tr[id^='order-inchg-eat-item'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});

		const json = await res.json();
		const result: any[] | null = json["inchgeat"];
		if (!result) return;

		let srcElem = <HTMLTableRowElement>this.$id("order-inchg-eat-item");
		let prevElem = srcElem;
		const self = this;
		result.forEach((tmp: { [key: string]: any }, i) => {
			i++;
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.display = "table-row";
			trElem.setAttribute("id", `order-inchg-eat-item${i}`);

			let elem;
			let tmp_id;
			let inputElem: HTMLInputElement;
			let chkbox: HTMLInputElement

			//患者名
			tmp_id = "order-inchg-eat-name";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = tmp["family_name"] + tmp["first_name"];

			//患者ID
			tmp_id = "order-inchg-eat-orca-id";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = "(" + tmp["orca_id"] + ")";

			// オーダー
			tmp_id = "order-inchg-eat-order";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = "オーダー";

			// アレルギー
			tmp_id = "order-inchg-eat-allergy";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = "アレルギー";

			//TBD（なにになるか確定したら記入）
			{

				//td
				tmp_id = "order-inchg-eat-status1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());

				//チェックボックス表示
				tmp_id = "status1_chkbox";
				inputElem = self.$id(tmp_id, trElem);
				inputElem.setAttribute("id", tmp_id + i.toString());
				if(tmp["act_status1"]){
					inputElem.checked = true;
				}
				if(tmp["act_status2"]){
					inputElem.disabled = true;
				}

				//チェックボックスチェック時動作
				inputElem.addEventListener("change", (evt: Event) => {
					chkbox = this.$id("status1_chkbox" + i.toString());
					this.updateInchgEatStatus(tmp["order_act_id"], 1, chkbox.checked);
				});

				//登録ユーザー表示
				tmp_id = "act_user1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_user1"];

				//登録時間表示
				tmp_id = "act_datetime1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_datetime1"];

			}

			//TBD（なにになるか確定したら記入）
			{
				//td
				tmp_id = "order-inchg-eat-status2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());

				//チェックボックス表示
				tmp_id = "status2_chkbox";
				inputElem = self.$id(tmp_id, trElem);
				inputElem.setAttribute("id", tmp_id + i.toString());
				if(tmp["act_status2"]){
					inputElem.checked = true;
				}
				if(!tmp["act_status1"]){
					inputElem.disabled = true;
				}

				//チェックボックスチェック時動作
				inputElem.addEventListener("change", (evt: Event) => {
					chkbox = this.$id("status2_chkbox" + i.toString());
					this.updateInchgEatStatus(tmp["order_act_id"], 2, chkbox.checked);
				});

				//登録ユーザー表示「
				tmp_id = "act_user2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_user2"];

				//登録時間表示
				tmp_id = "act_datetime2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_datetime2"];
			}

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});
	}

	private async updateInchgEatStatus(order_act_id: number, status_no: number, checked: boolean) {
		const self = this;
		const res = await API.updateInchgEatStatus(order_act_id, status_no, checked);
		if (!res.ok) {
			//self.errorMsg(res);
			return;
		}
		this. show()
	}

}

