import { MCHTMLElement } from "./basic";

import html from "./home-task.html";

export class HomeTask extends MCHTMLElement {
	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue == null) {
			return;
		}
	}
}