import { MCHTMLElement } from "./basic";
import { API } from "./api";
import html from "./order-inchg-med.html";

export class OrderInchgMed extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}
		this.show();
	}

	private async show() {

		console.log("[Higuchi][orderinchgmed]show()");	//デバッグ用

		let today = new Date();
		const res = await API.searchInchgMed(today);

		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		// テーブル全削除
		this.$queryAll("tr[id^='order-inchg-med-item'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});

		const json = await res.json();
		const result: any[] | null = json["inchgmed"];
		//if (!result) return;
		if (!result) {
			console.log("resultが空");
			return;
		}
		console.log(result);
		console.log("resultにデータあり");

		console.log("order-inchg-med-item");
		let srcElem = <HTMLTableRowElement>this.$id("order-inchg-med-item");
		let prevElem = srcElem;
		const self = this;
		result.forEach((tmp: { [key: string]: any }, i) => {
			i++;
			console.log(i.toString() + "番目");
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.display = "table-row";
			trElem.setAttribute("id", `order-inchg-med-item${i}`);

			let elem;
			let tmp_id;
			let inputElem: HTMLInputElement;
			let chkbox: HTMLInputElement

			//患者名
			console.log("order-inchg-med-name");
			tmp_id = "order-inchg-med-name";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = tmp["family_name"] + tmp["first_name"];

			//患者ID
			console.log("order-inchg-med-orca-id");
			tmp_id = "order-inchg-med-orca-id";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.textContent = "(" + tmp["orca_id"] + ")";

			// オーダー
			tmp_id = "order-inchg-med-order";
			elem = self.$id(tmp_id, trElem);
			elem.setAttribute("id", tmp_id + i.toString());
			elem.innerText = tmp["order"];

			//TBD（なにになるか確定したら記入）
			{

				//td
				tmp_id = "order-inchg-med-status1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());

				//チェックボックス表示
				tmp_id = "status1_chkbox";
				inputElem = self.$id(tmp_id, trElem);
				inputElem.setAttribute("id", tmp_id + i.toString());
				if(tmp["act_status1"]){
					inputElem.checked = true;
				}
				if(tmp["act_status2"]){
					inputElem.disabled = true;
				}

				//チェックボックスチェック時動作
				inputElem.addEventListener("change", (evt: Event) => {
					console.log("[Higuchi][order-inchg-med.ts]チェックボックスがクリックされました");
					chkbox = this.$id("status1_chkbox" + i.toString());
					this.updateInchgMedStatus(tmp["order_act_id"], 1, chkbox.checked);
				});

				//登録ユーザー表示
				tmp_id = "act_user1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_user1"];

				//登録時間表示
				tmp_id = "act_datetime1";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_datetime1"];

			}

			//TBD（なにになるか確定したら記入）
			{
				//td
				tmp_id = "order-inchg-med-status2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());

				//チェックボックス表示
				tmp_id = "status2_chkbox";
				inputElem = self.$id(tmp_id, trElem);
				inputElem.setAttribute("id", tmp_id + i.toString());
				if(tmp["act_status2"]){
					inputElem.checked = true;
				}
				if(!tmp["act_status1"]){
					inputElem.disabled = true;
				}

				//チェックボックスチェック時動作
				inputElem.addEventListener("change", (evt: Event) => {
					console.log("[Higuchi][order-inchg-med.ts]チェックボックスがクリックされました");
					chkbox = this.$id("status2_chkbox" + i.toString());
					this.updateInchgMedStatus(tmp["order_act_id"], 2, chkbox.checked);
				});

				//登録ユーザー表示「
				tmp_id = "act_user2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_user2"];

				//登録時間表示
				tmp_id = "act_datetime2";
				elem = self.$id(tmp_id, trElem);
				elem.setAttribute("id", tmp_id + i.toString());
				elem.textContent = tmp["act_datetime2"];
			}

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});
	}

	private async updateInchgMedStatus(order_act_id: number, status_no: number, checked: boolean) {

		console.log("[Higuchi][updateInchgMedStatus()]");

		const self = this;
		const res = await API.updateInchgMedStatus(order_act_id, status_no, checked);
		if (!res.ok) {
			//self.errorMsg(res);
			return;
		}
		this. show()
	}

}
