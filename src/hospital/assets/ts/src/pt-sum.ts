import { MCHTMLElement } from "./basic";

import html from "./pt-sum.html";

export class PtSum extends MCHTMLElement {
	private static menuTabTagMap = {
		"pt-sum-tab-1-1-tab": "mc-pt-sum-outline",
		"pt-sum-tab-1-2-tab": "mc-pt-sum-iccon",
		"pt-sum-tab-1-3-tab": "mc-pt-sum-task",
		"pt-sum-tab-1-4-tab": "mc-pt-sum-docbox",
		"pt-sum-tab-1-5-tab": "mc-pt-sum-hphistory",
	}
	private static startTab = "pt-sum-tab-1-1-tab";

	public constructor() {
		super(html, null, PtSum.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(PtSum.startTab)).click();
	}
}