import { MCHTMLElement } from "./basic";

import html from "./pt.html";

export class Pt extends MCHTMLElement {
	private static menuBtnTagMap = {
		"pt-bed-menu": "mc-pt-bed",
		"pt-list-menu": "mc-pt-list",
		"pt-add-menu": "mc-pt-add",
	}
	private static startMenu = "pt-bed-menu";

	public constructor() {
		super(html, Pt.menuBtnTagMap, null);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		const patientId = this.patientId;
		if (patientId) {
			this.style.display = "none";
			const pt2Elem = this.$tag("mc-pt2", null);
			pt2Elem.setAttribute("open", patientId);
			pt2Elem.style.removeProperty("display");
		} else {
			this.$id(this.currentMenuBtn(Pt.startMenu)).click();
		}
	}
}