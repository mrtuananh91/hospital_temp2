import { MCHTMLElement } from "./basic";

import html from "./pt-karte.html";

export class PtKarte extends MCHTMLElement {
	private static menuTabTagMap = {
		"pt-karte-tab-1-1-tab": "mc-pt-karte-view",
		"pt-karte-tab-1-2-tab": "mc-pt-karte-add",
		"pt-karte-tab-1-3-tab": "mc-pt-karte-order",
		"pt-karte-tab-1-4-tab": "mc-pt-karte-exam",
		"pt-karte-tab-1-5-tab": "mc-pt-karte-meddoc",
	};
	private static startTab = "pt-karte-tab-1-1-tab";

	public constructor() {
		super(html, null, PtKarte.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(PtKarte.startTab)).click();
	}
}