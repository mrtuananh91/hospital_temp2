import { CmnDate } from "./cmn-date";
import { CmnTime } from "./cmn-time";

import { Home } from "./home";
import { HomeHome } from "./home-home";
import { HomeSchedule } from "./home-schedule";
import { HomeTask } from "./home-task";
import { HomeMysetting } from "./home-mysetting";
import { Login } from "./login";
import { MCHTMLElement } from "./basic";
import { Pt } from "./pt";
import { PtBed } from "./pt-bed";
import { PtList } from "./pt-list";
import { PtAdd } from "./pt-add";
import { Pt2 } from "./pt2";
import { PtKarte } from "./pt-karte";
import { PtKarteView } from "./pt-karte-view";
import { PtKarteAdd } from "./pt-karte-add";
import { PtKarteAddKarte } from "./pt-karte-add-karte";
import { PtKarteAddMed } from "./pt-karte-add-med";
import { PtKarteOrder } from "./pt-karte-order";
import { PtKarteOrderAdd } from "./pt-karte-orderadd";
import { PtKarteExam } from "./pt-karte-exam";
import { PtKarteMeddoc } from "./pt-karte-meddoc";
import { PtSum } from "./pt-sum";
import { PtSumOutline } from "./pt-sum-outline";
import { PtSumICCon } from "./pt-sum-iccon";
import { PtSumTask } from "./pt-sum-task";
import { PtSumDocBox } from "./pt-sum-docbox";
import { PtSumHpHistory } from "./pt-sum-hphistory";
import { PtOrder } from "./pt-order";
import { PtNurse } from "./pt-nurse";
import { PtNurseRec } from "./pt-nurse-rec";
import { PtNurseVital } from "./pt-nurse-vital";
import { PtNurseAssess } from "./pt-nurse-assess";
import { PtNursePlan } from "./pt-nurse-plan";
import { PtNurseDoc } from "./pt-nurse-doc";
import { PtReha } from "./pt-reha";
import { PtRehaRec } from "./pt-reha-rec";
import { PtRehaAssess } from "./pt-reha-assess";
import { PtRehaDoc } from "./pt-reha-doc";
import { PtDt } from "./pt-dt";
import { PtDtRec } from "./pt-dt-rec";
import { PtDtAssess } from "./pt-dt-assess";
import { PtDtDoc } from "./pt-dt-doc";
import { PtSkd } from "./pt-skd";
import { PtTemp } from "./pt-temp";
import { Order } from "./order";
import { OrderOrder } from "./order-order";
import { OrderOrderList } from "./order-order-list";
import { OrderOrderDetail } from "./order-order-detail";
import { OrderInchg } from "./order-inchg";
import { OrderInchgShot } from "./order-inchg-shot";
import { OrderInchgBarcode } from "./order-inchg-barcode";
import { OrderInchgEat } from "./order-inchg-eat";
import { OrderInchgMed } from "./order-inchg-med";
import { OrderInchgSpecimen } from "./order-inchg-specimen";
import { OrderInchgSpecimenIn } from "./order-inchg-specimen-in";
import { OrderInchgSpecimenOut } from "./order-inchg-specimen-out";
import { OrderInchgSpecimenAdd } from "./order-inchg-specimen-add";
import { OrderInchgPhys } from "./order-inchg-phys";
import { OrderInchgBfp } from "./order-inchg-bfp";
import { OrderInchgRad } from "./order-inchg-rad";
import { OrderInchgReha } from "./order-inchg-reha";
import { OrderInchgOpe } from "./order-inchg-ope";
import { OrderInchgBlood } from "./order-inchg-blood";
import { OrderEmg } from "./order-emg";
import { Bed } from "./bed";
import { BedBed } from "./bed-bed";
import { BedReport } from "./bed-report";
import { Ope } from "./ope";
import { Rec } from "./rec";
import { Conf } from "./conf";
import { ConfUser } from "./conf-user";

class Entry {
	private menuTagMap = {
		"home-menu": "mc-home",
		"pt-menu": "mc-pt",
		"order-menu": "mc-order",
		"bed-menu": "mc-bed",
		"ope-menu": "mc-ope",
		"rec-menu": "mc-rec",
		"conf-menu": "mc-conf"
	};

	private allDisplayNone() {
		for (let [menu, tag] of Object.entries(this.menuTagMap)) {
			const tagElem = <HTMLElement>document.querySelector(tag);
			tagElem.style.display = "none";
			tagElem.removeAttribute("open");
			(<HTMLElement>document.querySelector(`#${menu}`)).classList.remove("active");
		}
		(<HTMLElement>document.querySelector("mc-pt2")).style.display = "none";
	}

	public init(toastFunc: (msg: string) => void) {
		if (document.querySelector("mc-login")) {
			sessionStorage.removeItem("menu-side");
			sessionStorage.removeItem("menu-button");
			sessionStorage.removeItem("patient-id");
			sessionStorage.removeItem("menu-tab");
			customElements.define("mc-login", Login);
			return;
		}

		MCHTMLElement.toastFunc = toastFunc;

		customElements.define("cmn-date", CmnDate);
		customElements.define("cmn-time", CmnTime);

		customElements.define("mc-home", Home);
		customElements.define("mc-home-home", HomeHome);
		customElements.define("mc-home-schedule", HomeSchedule);
		customElements.define("mc-home-task", HomeTask);
		customElements.define("mc-home-mysetting", HomeMysetting);

		customElements.define("mc-pt", Pt);
		customElements.define("mc-pt-bed", PtBed);
		customElements.define("mc-pt-list", PtList);
		customElements.define("mc-pt-add", PtAdd);

		customElements.define("mc-pt2", Pt2);
		customElements.define("mc-pt-karte", PtKarte);
		customElements.define("mc-pt-karte-view", PtKarteView);
		customElements.define("mc-pt-karte-add", PtKarteAdd);
		customElements.define("mc-pt-karte-add-karte", PtKarteAddKarte);
		customElements.define("mc-pt-karte-add-med", PtKarteAddMed);
		customElements.define("mc-pt-karte-order", PtKarteOrder);
		customElements.define("mc-pt-karte-orderadd", PtKarteOrderAdd);
		customElements.define("mc-pt-karte-exam", PtKarteExam);
		customElements.define("mc-pt-karte-meddoc", PtKarteMeddoc);
		customElements.define("mc-pt-sum", PtSum);
		customElements.define("mc-pt-sum-outline", PtSumOutline);
		customElements.define("mc-pt-sum-iccon", PtSumICCon);
		customElements.define("mc-pt-sum-task", PtSumTask);
		customElements.define("mc-pt-sum-docbox", PtSumDocBox);
		customElements.define("mc-pt-sum-hphistory", PtSumHpHistory);
		customElements.define("mc-pt-order", PtOrder);
		customElements.define("mc-pt-nurse", PtNurse);
		customElements.define("mc-pt-nurse-rec", PtNurseRec);
		customElements.define("mc-pt-nurse-vital", PtNurseVital);
		customElements.define("mc-pt-nurse-assess", PtNurseAssess);
		customElements.define("mc-pt-nurse-plan", PtNursePlan);
		customElements.define("mc-pt-nurse-doc", PtNurseDoc);
		customElements.define("mc-pt-reha", PtReha);
		customElements.define("mc-pt-reha-rec", PtRehaRec);
		customElements.define("mc-pt-reha-assess", PtRehaAssess);
		customElements.define("mc-pt-reha-doc", PtRehaDoc);
		customElements.define("mc-pt-dt", PtDt);
		customElements.define("mc-pt-dt-rec", PtDtRec);
		customElements.define("mc-pt-dt-assess", PtDtAssess);
		customElements.define("mc-pt-dt-doc", PtDtDoc);
		customElements.define("mc-pt-skd", PtSkd);

		customElements.define("mc-pt-temp", PtTemp);

		//customElements.define("mc-hosp", Hosp);
		//customElements.define("mc-summary", Summary);
		//customElements.define("mc-summary-summary", SummarySummary);

		customElements.define("mc-order", Order);
		customElements.define("mc-order-order", OrderOrder);
		customElements.define("mc-order-order-list", OrderOrderList);
		customElements.define("mc-order-order-detail", OrderOrderDetail);
		customElements.define("mc-order-inchg", OrderInchg);
		customElements.define("mc-order-inchg-shot", OrderInchgShot);
		customElements.define("mc-order-inchg-barcode", OrderInchgBarcode);
		customElements.define("mc-order-inchg-eat", OrderInchgEat);
		customElements.define("mc-order-inchg-med", OrderInchgMed);
		customElements.define("mc-order-inchg-specimen", OrderInchgSpecimen);
		customElements.define("mc-order-inchg-specimen-in", OrderInchgSpecimenIn);
		customElements.define("mc-order-inchg-specimen-out", OrderInchgSpecimenOut);
		customElements.define("mc-order-inchg-specimen-add", OrderInchgSpecimenAdd);
		customElements.define("mc-order-inchg-phys", OrderInchgPhys);
		customElements.define("mc-order-inchg-bfp", OrderInchgBfp);
		customElements.define("mc-order-inchg-rad", OrderInchgRad);
		customElements.define("mc-order-inchg-reha", OrderInchgReha);
		customElements.define("mc-order-inchg-ope", OrderInchgOpe);
		customElements.define("mc-order-inchg-blood", OrderInchgBlood);
		customElements.define("mc-order-emg", OrderEmg);

		customElements.define("mc-bed", Bed);
		customElements.define("mc-bed-bed", BedBed);
		customElements.define("mc-bed-report", BedReport);

		customElements.define("mc-ope", Ope);
		customElements.define("mc-rec", Rec);
		customElements.define("mc-conf", Conf);
		customElements.define("mc-conf-user", ConfUser);

		// T.B.D (Not support Safari?)
		let reload = false;
		const pes = performance.getEntriesByType("navigation");
		for (let pe of pes) {
			if ((<any>pe).type === "reload") {
				reload = true;
				break;
			}
		}
		const bodyElem = <HTMLBodyElement>document.querySelector("body");
		bodyElem.setAttribute("data-reload-menu-side", "true");
		bodyElem.setAttribute("data-reload-menu-button", "true");
		bodyElem.setAttribute("data-reload-menu-tab", "true");

		const self = this;
		for (let [menu, tag] of Object.entries(this.menuTagMap)) {
			const navElem = <HTMLElement>document.querySelector(`#${menu}`);
			if (navElem === null) {
				continue;
			}

			navElem.addEventListener("click", (evt: Event) => {
				evt.preventDefault();
				self.allDisplayNone();
				const menuSideId = <string>navElem.getAttribute("id");
				sessionStorage.setItem(MCHTMLElement.SS_MENU_SIDE, menuSideId);

				if (bodyElem.getAttribute("data-reload-menu-side") === "true") {
					bodyElem.removeAttribute("data-reload-menu-side");
				} else {
					sessionStorage.removeItem(MCHTMLElement.SS_MENU_BUTTON);
					sessionStorage.removeItem(MCHTMLElement.SS_PATIENT_ID);
				}

				const tagElem = <HTMLElement>document.querySelector(tag);
				tagElem.style.removeProperty("display");
				tagElem.setAttribute("open", "true");
				navElem.classList.add("active");
			});
		}

		const menuSideId = sessionStorage.getItem("menu-side");
		if (menuSideId) {
			(<HTMLElement>document.querySelector(`#${menuSideId}`)).click();
		} else {
			sessionStorage.setItem("menu-side", "home-menu");
		}
	}
}

interface Window {
	movacal: Entry;
}

declare const window: Window;
window.movacal = new Entry();
