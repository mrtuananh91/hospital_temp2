import { MCHTMLElement } from "./basic";

import html from "./order-inchg.html";

export class OrderInchg extends MCHTMLElement {
	private static menuTabTagMap = {
		"order-inchg-tab-1-1-tab": "mc-order-inchg-shot",
		"order-inchg-tab-1-2-tab": "mc-order-inchg-eat",
		"order-inchg-tab-1-3-tab": "mc-order-inchg-med",
		"order-inchg-tab-1-4-tab": "mc-order-inchg-specimen",
		"order-inchg-tab-1-5-tab": "mc-order-inchg-phys",
		"order-inchg-tab-1-6-tab": "mc-order-inchg-bfp",
		"order-inchg-tab-1-7-tab": "mc-order-inchg-rad",
		"order-inchg-tab-1-8-tab": "mc-order-inchg-reha",
		"order-inchg-tab-1-9-tab": "mc-order-inchg-ope",
		"order-inchg-tab-1-10-tab": "mc-order-inchg-blood",
	};
	private static startTab = "order-inchg-tab-1-1-tab";

	public constructor() {
		super(html, null, OrderInchg.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(OrderInchg.startTab)).click();
	}
}