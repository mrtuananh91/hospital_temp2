import { MCHTMLElement } from "./basic";

import html from "./order-order.html";

export class OrderOrder extends MCHTMLElement {
	private static menuTabTagMap = {
		"order-order-tab-1-1-tab": "mc-order-order-list",
		"order-order-tab-1-2-tab": "mc-order-order-detail",
	};
	private static startTab = "order-order-tab-1-1-tab";

	public constructor() {
		super(html, null, OrderOrder.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(OrderOrder.startTab)).click();
	}
}