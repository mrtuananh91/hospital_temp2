import { MCHTMLElement } from "./basic";

import html from "./pt-reha.html";

export class PtReha extends MCHTMLElement {
	private static menuTabTagMap = {
		"pt-reha-tab-1-1-tab": "mc-pt-reha-rec",
		"pt-reha-tab-1-2-tab": "mc-pt-reha-assess",
		"pt-reha-tab-1-3-tab": "mc-pt-reha-doc",
	};
	private static startTab = "pt-reha-tab-1-1-tab";

	public constructor() {
		super(html, null, PtReha.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(PtReha.startTab)).click();
	}
}
