import * as Moment from "moment";
import { API } from "./api";
import { MCHTMLElement } from "./basic";
import { CmnDate } from "./cmn-date";
import { CmnTime } from "./cmn-time";

import html from "./pt-karte-add-karte.html";

export class PtKarteAddKarte extends MCHTMLElement {
	private dlgCreate = false;
	private createElem: HTMLButtonElement;
	private dlgKarteIdElem: HTMLInputElement;
	private dlgTitleElem: HTMLSpanElement;
	private dlgCloseElem: HTMLButtonElement;
	private dlgDiagDeptElem: HTMLSelectElement;
	private dlgDiagTypeElem: HTMLSelectElement;
	private dlgActStartDateElem: CmnDate;
	private dlgActStartTimeElem: CmnTime;
	private dlgKarteTitleElem: HTMLInputElement;
	private dlgSubstituteInputElem: HTMLInputElement;
	private dlgKarteBodyElem: HTMLTextAreaElement;
	private dlgKarteMemoElem: HTMLTextAreaElement;
	private submitElem: HTMLButtonElement;

	public constructor() {
		super(html);

		this.createElem = this.$id("pt-karte-add-karte-create");
		this.dlgKarteIdElem = this.$id("pt-karte-add-karte-dialog-karteid");
		this.dlgTitleElem = this.$id("pt-karte-add-karte-dialog-title");
		this.dlgCloseElem = this.$id("pt-karte-add-karte-dialog-close");
		this.dlgDiagDeptElem = this.$id("pt-karte-add-karte-dialog-diagdept");
		this.dlgDiagTypeElem = this.$id("pt-karte-add-karte-dialog-diagtype");
		this.dlgActStartDateElem = this.$id("pt-karte-add-karte-dialog-act-startdate");
		this.dlgActStartTimeElem = this.$id("pt-karte-add-karte-dialog-act-starttime");
		this.dlgKarteTitleElem = this.$id("pt-karte-add-karte-dialog-karte-title");
		this.dlgSubstituteInputElem = this.$id("pt-karte-add-karte-dialog-substitute-input");
		this.dlgKarteBodyElem = this.$id("pt-karte-add-karte-dialog-karte-body");
		this.dlgKarteMemoElem = this.$id("pt-karte-add-karte-dialog-karte-memo");
		this.submitElem = this.$id("pt-karte-add-karte-dialog-submit");

		this.setCreateEvent();
		this.setSubmitEvent();
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.show();
	}

	public async show() {
		// テーブル全削除
		this.$queryAll("tr[id^='pt-karte-add-karte-item'").forEach((elem: HTMLElement) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});
		for (let i = this.dlgDiagDeptElem.options.length - 1; i >= 0; i--) {
			this.dlgDiagDeptElem.options.remove(i);
		}
		for (let i = this.dlgDiagTypeElem.options.length - 1; i >= 0; i--) {
			this.dlgDiagTypeElem.options.remove(i);
		}

		//
		const patientId = <string>this.patientId;
		const h13nId = <string>this.h13nId;
		const res = await API.getRecords(patientId, h13nId, 0);
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		const json = await res.json();
		const recs: any[] | null = json["records"];
		if (!recs) return;

		let srcElem = <HTMLTableRowElement>this.$id("pt-karte-add-karte-item");
		let prevElem = srcElem;
		const self = this;
		recs.forEach((rec: { [key: string]: any }, i) => {
			i++;
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.removeProperty("display");
			trElem.setAttribute("id", `pt-karte-add-karte-item${i}`);

			// Title
			let elem = self.$id("pt-karte-add-karte-title", trElem);
			elem.setAttribute("id", `pt-karte-add-karte-title${i}`);
			elem.textContent = `${rec["karte_title"]} ${self.datetime(rec["act_start_datetime"])} ${rec["input_user_name"]}`;

			// Body
			elem = self.$id("pt-karte-add-karte-body", trElem);
			elem.setAttribute("id", `pt-karte-add-karte-body${i}`);
			elem.textContent = rec["karte_body"];

			// Note
			elem = self.$id("pt-karte-add-karte-memo", trElem);
			elem.setAttribute("id", `pt-karte-add-karte-memo${i}`);
			elem.textContent = rec["karte_memo"];

			// Edit
			elem = self.$id("pt-karte-add-karte-edit", trElem);
			elem.setAttribute("id", `pt-karte-add-karte-edit${i}`);
			elem.addEventListener("click", (evt: Event) => {
				self.dlgTitleElem.textContent = "編集";
				self.dlgKarteIdElem.value = rec["karte_id"];
				self.dlgDiagDeptElem.value = rec["diagdept_id"];
				self.dlgDiagTypeElem.value = rec["diagtype_id"];
				const m = Moment(rec["act_start_datetime"]);
				self.dlgActStartDateElem.value = m.format("YYYY/MM/DD");
				self.dlgActStartTimeElem.value = m.format("HH:mm");
				self.dlgKarteTitleElem.value = rec["karte_title"];
				self.dlgSubstituteInputElem.checked = rec["substitute_input"];
				self.dlgKarteBodyElem.value = rec["karte_body"];
				self.dlgKarteMemoElem.value = rec["karte_memo"];
				self.dlgCreate = false;
			});

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});

		//
		const res2 = await API.getConf();
		if (!res2.ok) {
			this.errorMsg(res2);
		}

		const json2 = await res2.json();
		const dds = json2["diagdepts"];
		for (let dd of dds) {
			const optElem = <HTMLOptionElement>document.createElement("option");
			optElem.value = `${dd["diagdept_id"]}`;
			optElem.textContent = dd["diagdept_name"];
			this.dlgDiagDeptElem.options.add(optElem);
		}
		const dts = json2["diagtypes"];
		for (let dt of dts) {
			const optElem = <HTMLOptionElement>document.createElement("option");
			optElem.value = `${dt["diagtype_id"]}`;
			optElem.textContent = dt["diagtype_name"];
			this.dlgDiagTypeElem.options.add(optElem);
		}
	}

	private setCreateEvent() {
		const self = this;
		this.createElem.addEventListener("click", (evt: Event) => {
			self.dlgTitleElem.textContent = "新規作成";
			if (!self.dlgCreate) {
				self.dlgKarteIdElem.value = "";
				self.dlgDiagDeptElem.selectedIndex = 0;
				self.dlgDiagTypeElem.selectedIndex = 0;
				self.dlgActStartDateElem.value = "";
				self.dlgActStartTimeElem.value = "";
				self.dlgKarteTitleElem.value = "";
				self.dlgSubstituteInputElem.checked = false;
				self.dlgKarteBodyElem.value = "";
				self.dlgKarteMemoElem.value = "";
			}
			self.dlgCreate = true;
		});
	}

	private setSubmitEvent() {
		const self = this;
		this.submitElem.addEventListener("click", async (evt: Event) => {
			evt.stopPropagation();
			evt.preventDefault();

			self.blockStart();
			try {
				const karteId = self.dlgKarteIdElem.value;
				let res: Response;
				let msg: string;
				if (karteId) {
					res = await API.updateKarte(karteId,
						self.dlgKarteTitleElem.value, self.dlgSubstituteInputElem.checked,
						self.dlgDiagDeptElem.value ? parseInt(self.dlgDiagDeptElem.value) : 0,
						self.dlgDiagTypeElem.value ? parseInt(self.dlgDiagTypeElem.value) : 0,
						self.dlgActStartDateElem.value, this.dlgActStartTimeElem.value,
						self.dlgKarteBodyElem.value, self.dlgKarteMemoElem.value);
					msg = "カルテを編集しました。"
				} else {
					res = await API.addKarte(<string>self.patientId, <string>self.h13nId,
						self.dlgKarteTitleElem.value, self.dlgSubstituteInputElem.checked,
						self.dlgDiagDeptElem.value ? parseInt(self.dlgDiagDeptElem.value) : 0,
						self.dlgDiagTypeElem.value ? parseInt(self.dlgDiagTypeElem.value) : 0,
						self.dlgActStartDateElem.value, this.dlgActStartTimeElem.value,
						self.dlgKarteBodyElem.value, self.dlgKarteMemoElem.value);
					msg = "カルテを登録しました。";
				}
				if (!res.ok) {
					self.errorMsg(res);
					return;
				}

				self.toast(msg);
				self.dlgCreate = false;
				self.dlgCloseElem.click();
				self.$id("pt-karte-tab-1-2-tab", null).click();
			} finally {
				self.blockEnd();
			}
		});
	}
}