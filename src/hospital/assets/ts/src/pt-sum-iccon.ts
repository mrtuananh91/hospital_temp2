import { MCHTMLElement } from "./basic";

import html from "./pt-sum-iccon.html";

export class PtSumICCon extends MCHTMLElement {

	private formElem: HTMLFormElement;
	private orcaidElem: HTMLInputElement;
	private nameElem: HTMLInputElement;
	private genderElem: HTMLInputElement;
	private birthdayElem: HTMLInputElement;
	private ageElem: HTMLInputElement;
	private bloodTypeElem: HTMLInputElement;
	private addBtnElem: HTMLButtonElement;
	private newBtnElem: HTMLButtonElement;
	private backBtn1Elem: HTMLButtonElement;
	private backBtn2Elem: HTMLButtonElement;

	private dateElem: HTMLInputElement;
	private doctorElem: HTMLInputElement;
	private patientElem: HTMLInputElement;
	private titleElem: HTMLInputElement;
	private contentElem: HTMLTextAreaElement;

	private icconHeadTableElem: HTMLTableElement;
	private icconListTableElem: HTMLTableElement;
	private icconAddTableElem: HTMLTableElement;
	private icconNewTableElem: HTMLTableElement;

	public constructor() {
		super(html);

		this.formElem = this.$id("pt-sum-iccon-add-form");
		this.orcaidElem = this.$id("pt-sum-iccon-add-orcaid");
		this.nameElem = this.$id("pt-sum-iccon-add-name");
		this.genderElem = this.$id("pt-sum-iccon-add-gender");
		this.birthdayElem = this.$id("pt-sum-iccon-add-birthday");
		this.ageElem = this.$id("pt-sum-iccon-add-age");
		this.bloodTypeElem = this.$id("pt-sum-iccon-add-bloodtype");
		this.addBtnElem = this.$id("pt-sum-iccon-list-add");
		this.newBtnElem = this.$id("pt-sum-iccon-list-new");
		this.backBtn1Elem = this.$id("pt-sum-iccon-add-back");
		this.backBtn2Elem = this.$id("pt-sum-iccon-new-back");

		this.dateElem = this.$id("pt-sum-iccon-add-date");
		this.doctorElem = this.$id("pt-sum-iccon-add-doctor");
		this.patientElem = this.$id("pt-sum-iccon-add-patient");
		this.titleElem = this.$id("pt-sum-iccon-add-title");
		this.contentElem = this.$id("pt-sum-iccon-add-content");

		this.icconHeadTableElem = this.$id("pt-sum-iccon-head-table");
		this.icconListTableElem = this.$id("pt-sum-iccon-list-table");
		this.icconAddTableElem = this.$id("pt-sum-iccon-add-table");
		this.icconNewTableElem = this.$id("pt-sum-iccon-new-table");

		//		this.setSubmitEvent();
		this.regAddEvent();
		this.regNewEvent();
		this.regBack1Event();
		this.regBack2Event();
}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

//		this.show(null);
	}
/*
	private async show(: string) {
		this.blockStart();
		try {
			const res = await API.searchPatients();
			if (!res.ok) {
				this.errorMsg(res);
				return;
			}

			const json = await res.json();
			const pts = json["patients"];
			const pt = pts[0];
//			this.orcaidElem.value = pt["orca_id"];
			this.contentElem.value = pts;
			this.orcaidElem.value = "1234";
			this.nameElem.value = `${pt["family_name"]} ${pt["first_name"]}`;
			this.genderElem.value = this.gender(pt["gender"]);
			const [s, birthday] = this.birthday(pt["birthday"]);
			this.$id("pt-sum-iccon-add-birthday").textContent = s;
			this.$id("pt-sum-iccon-add-age").textContent = `${this.age(birthday)}`;
			this.$id("pt-sum-iccon-add-bloodtype").textContent = pt["blood_type"];
		} finally {
			this.blockEnd();
		}
	}
*/
/*	private setSubmitEvent(): void {
		const self = this;
		this.formElem.addEventListener("submit", async (evt: Event) => {
			evt.stopPropagation();
			evt.preventDefault();

			self.blockStart();
			try {
				let iccon = new ICCon();
				iccon.id = "";
				iccon.orcaId = self.dateElem.value;
				iccon.date = self.dateElem.value;
				iccon.doctor = self.doctorElem.value;
				iccon.patient = self.patientElem.value;
				iccon.title = self.titleElem.value;
				iccon.content = self.contentElem.value;

				const res = await API.ICCon([iccon]);
				if (!res.ok) {
					self.errorMsg(res);
					return;
				}

				self.toast("説明内容を登録しました。");
				this.initElems();
			} finally {
				self.blockEnd();
			}
		});
	}
*/
	private regAddEvent() {
		this.addBtnElem.addEventListener("click",(evt: Event)=> {
			this.icconHeadTableElem.style.display = "block";
			this.icconListTableElem.style.display = "none";
			this.icconAddTableElem.style.display = "block";
			this.icconNewTableElem.style.display = "none";
		});
	}

	private regNewEvent() {
		this.newBtnElem.addEventListener("click",(evt: Event)=> {
			this.icconHeadTableElem.style.display = "block";
			this.icconListTableElem.style.display = "none";
			this.icconAddTableElem.style.display = "none";
			this.icconNewTableElem.style.display = "block";
		});
	}

	private regBack1Event() {
		this.backBtn1Elem.addEventListener("click",(evt: Event)=> {
			this.icconHeadTableElem.style.display = "block";
			this.icconListTableElem.style.display = "block";
			this.icconAddTableElem.style.display = "none";
			this.icconNewTableElem.style.display = "none";
		});
	}
	private regBack2Event() {
		this.backBtn2Elem.addEventListener("click",(evt: Event)=> {
			this.icconHeadTableElem.style.display = "block";
			this.icconListTableElem.style.display = "block";
			this.icconAddTableElem.style.display = "none";
			this.icconNewTableElem.style.display = "none";
		});
	}
}
