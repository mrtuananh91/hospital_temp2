import { API, Record } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt-nurse-rec.html";
import {CmnTime} from "./cmn-time";
import {CmnDate} from "./cmn-date";
import * as Moment from "moment";

export class PtNurseRec extends MCHTMLElement {
    private addFormElem: HTMLFormElement;
    private addActDateElem: HTMLInputElement;
    private addActTimeElem: HTMLInputElement;
    private addActDetailElem: HTMLTextAreaElement;
    private AddNoteElem: HTMLTextAreaElement;
    private AddUserNameElem: HTMLParagraphElement;

    private updateFormElem: HTMLFormElement;
    private updateActDateElem: HTMLInputElement;
    private updateActTimeElem: HTMLInputElement;
    private updateActDetailElem: HTMLTextAreaElement;
    private updateNoteElem: HTMLTextAreaElement;
    private updateUserNameElem: HTMLParagraphElement;

    // private updateTemporaryData!: { [s: string]: UpdateTemporaryData};
    private updateTemporaryData!: { [s: string]: {[s: string]: string} };
    // private updateTemporaryData!: any[];
    private updateTargetId: string | null;

    private recordId(itemId: number): string | null {
        const elem = this.$id("pt-nurse-rec-list2-record-id" + itemId, null);
        return elem ? (<HTMLInputElement>elem).value : null;
    }

    private get RecordType() {
        return 1
    }

    public constructor() {
        super(html);
        this.addFormElem = this.$id("pt-nurse-rec-add-form");
        this.addActDateElem = this.$id("pt-nurse-rec-add-act-date");
        this.addActTimeElem = this.$id("pt-nurse-rec-add-act-time");
        this.addActDetailElem = this.$id("pt-nurse-rec-add-act-detail");
        this.AddNoteElem = this.$id("pt-nurse-rec-add-note");
        this.AddUserNameElem = this.$id("pt-nurse-rec-add-user-name");

        this.updateFormElem = this.$id("pt-nurse-rec-update-form");
        this.updateActDateElem = this.$id("pt-nurse-rec-update-act-date");
        this.updateActTimeElem = this.$id("pt-nurse-rec-update-act-time");
        this.updateActDetailElem = this.$id("pt-nurse-rec-update-act-detail");
        this.updateNoteElem = this.$id("pt-nurse-rec-update-note");
        this.updateUserNameElem = this.$id("pt-nurse-rec-update-user-name");

        this.updateTargetId = "";
    }

    public static get observedAttributes(): string[] {
        return ["open"];
    }

    public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
        if (attrName != "open" || newValue !== "true") {
            return;
        }
        this.setChangeH13nDayEvent(this.show)
        this.show();
    }
    private async show() {
        this.blockStart();
        try {
            let recordType: number = 1;
            let patientId = this.patientId ?? "0";
            let h13nId = this.h13nId ?? "0";
            const res = await API.getRecordsByPatientId(patientId, recordType, h13nId);
            if (!res.ok) {
                this.errorMsg(res);
                return;
            }
            const json = await res.json();
            const rcd: any[] | null = json["records"];
            console.log("aaaaaa",rcd)
            if (!rcd) return;
            // テーブル全削除
            this.$queryAll("tr[id^='pt-nurse-rec-list-item']").forEach((elem) => {
                if (elem.style.display !== "none") {
                    (<HTMLElement>elem.parentElement).removeChild(elem);
                }
            });
            this.$queryAll("table[id^='pt-nurse-rec-list2-item'").forEach((elem) => {
                if (elem.style.display !== "none") {
                    (<HTMLElement>elem.parentElement).removeChild(elem);
                }
            });
            this.addFormElem.addEventListener("submit", (evt: Event) => {
                this.addRecord();
            });
            this.updateFormElem.addEventListener("submit", (evt: Event) => {
                this.updateRecord(this.updateTargetId);
            });
            let dateListElem = <HTMLTableRowElement>this.$id("pt-nurse-rec-list-item");
            let timeLineElem = <HTMLTableRowElement>this.$id("pt-nurse-rec-list2-item");
            let prevElem = dateListElem;
            let timeLinePrevElem = timeLineElem;

            let addElem = this.$id('pt-nurse-rec-add');
            addElem.addEventListener("click", (evt: Event) => {
                this.protAddForm();
            });
            this.$id("pt-nurse-rec-update-record-close").addEventListener("click", (evt: Event) => {
                console.log("一時保存")
                this.temporaryUpdate();
            });
            rcd.forEach((rd: { [key: string]: any }, i) => {
                i++;
                let trElem = <HTMLTableRowElement>dateListElem.cloneNode(true);
                trElem.style.display = "table-row";
                trElem.setAttribute("id", `pt-nurse-rec-list-item${i}`);

                // date
                let elm = this.$id("pt-nurse-rec-list-date", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list-date${i}`);
                elm.textContent = this.dateFormat(rd["act_datetime"], "/", "md") + " " + this.week(rd["act_datetime"], "parentheses");

                elm = this.$id("pt-nurse-rec-list-time", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list-time${i}`);
                elm.textContent = this.timeFormat(rd["act_datetime"]);

                trElem.addEventListener("click", (evt: Event) => {
                    //対象のタイムラインまでスクロール
                    let target = <HTMLElement>document.getElementById(`pt-nurse-rec-list2-item${i}`);
                    target.scrollIntoView(true);
                });

                (<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
                prevElem = trElem;
            });
            rcd.forEach((rd: { [key: string]: any }, i) => {
                i++;
                let trElem = <HTMLTableRowElement>timeLineElem.cloneNode(true);
                trElem.style.display = "table";
                trElem.setAttribute("id", `pt-nurse-rec-list2-item${i}`);

                let elm = this.$id("pt-nurse-rec-list2-act-date", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-act-date${i}`);
                elm.textContent = this.dateFormat(rd["act_datetime"]);

                elm = this.$id("pt-nurse-rec-list2-act-time", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-act-time${i}`);
                elm.textContent = this.timeFormat(rd["act_datetime"]);

                elm = this.$id("pt-nurse-rec-list2-reg-datetime", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-reg-datetime${i}`);
                elm.textContent = this.dateTimeFormat(rd["reg_datetime"]);

                elm = this.$id("pt-nurse-rec-list2-user-name", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-user-name${i}`);
                elm.textContent = rd["user_name"];

                elm = this.$id("pt-nurse-rec-list2-act-detail", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-act-detail${i}`);
                elm.textContent = rd["act_detail"];

                elm = this.$id("pt-nurse-rec-list2-note", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-note${i}`);
                elm.textContent = rd["note"];

                elm = this.$id("pt-nurse-rec-list2-record-id", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-record-id${i}`);
                elm.textContent = rd["id"];

                elm = this.$id("pt-nurse-rec-list2-img", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-img${i}`);
                elm.setAttribute("src", "http://localhost:10080/dist/img/user1-128x128.jpg");

                // elm = this.$id("pt-nurse-rec-update-record", trElem);
                // elm.setAttribute("id", `pt-nurse-rec-update-record${i}`);

                elm = this.$id("pt-nurse-rec-list2-action-update", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-action-update${i}`);
                elm.addEventListener("click", (evt: Event) => {
                    // todo 画像関係
                    this.updateTargetId = rd["id"];
                    //元データ入力
                    this.protUpdateForm(i,rd["id"]);
                // let inputFile = document.getElementById(`input-file`);
                // if (inputFile) {
                //     inputFile.addEventListener("change", (evt: Event) => {
                //         let file = (<HTMLInputElement>inputFile).files ?? null;
                //         let reader = new FileReader();
                //         if (file) {
                //             reader.onload = function ( event ) {
                //                 console.log( event.target.result ) ;
                //             }
                //             reader.readAsText(file[0]);
                //         }
                //     });
                // }
                    this.updateTargetId = rd["id"];
                });

                elm = this.$id("pt-nurse-rec-list2-action-print", trElem);
                elm.setAttribute("id", `pt-nurse-rec-list2-action-print${i}`);
                elm.addEventListener("click", (evt: Event) => {
                    //todo 対象のPDF出力=>印刷画面　もしくは　対象のPDF出力
                });

                (<HTMLElement>timeLinePrevElem.parentElement).insertBefore(trElem, timeLinePrevElem.nextSibling);
                timeLinePrevElem = trElem;
            });
        } finally {
            this.blockEnd();
        }
    }

    private async updateRecord(recordID: string | null) {
        const status = 1;
        this.blockStart();
        if (recordID === null) return;
        try {
            let rd = new Record();
            rd.id = recordID;
            rd.status = status;
            rd.actDatetime = Moment(`${this.updateActDateElem.value} ${this.updateActTimeElem.value}`, "YYYY/MM/DD HH:mm").format();
            rd.actDetail = this.updateActDetailElem.value ? this.updateActDetailElem.value : null;
            rd.note = this.updateNoteElem.value;
            rd.userName = this.userName;

            const res = await API.updateRecord([rd]);
            if (!res.ok) {
                this.errorMsg(res);
                return;
            }
            this.show();
            this.toast("看護記録を変更しました。");
            this.initElems();
        } finally {
            this.blockEnd();
        }
    }
    private async addRecord() {
        const status = 1;
        this.blockStart();
        try {
            let rd = new Record();
            rd.id = "";
            rd.patientId = this.patientId;
            rd.status = status;
            rd.actDatetime = Moment(`${this.addActDateElem.value} ${this.addActTimeElem.value}`, "YYYY/MM/DD HH:mm").format();
            rd.actDetail = this.addActDetailElem.value ? this.addActDetailElem.value : null;
            rd.note = this.AddNoteElem.value;
            rd.userName = this.AddUserNameElem.innerText ? this.addActDetailElem.value : null;
            rd.recordType = this.RecordType;
            rd.userName = this.userName;
            rd.h13nId = this.h13nId;
            const res = await API.addRecord([rd]);
            if (!res.ok) {
                this.errorMsg(res);
                return;
            }
            this.show();
            this.toast("看護記録を作成しました。");
            this.initElems();
        } finally {
            this.blockEnd();
        }
    }
    protected protAddForm() {
        (<CmnDate>this.$id(`pt-nurse-rec-add-act-date`)).value = (<CmnDate>this.$id(`pt-nurse-rec-add-act-date`)).value !== "" ? (<CmnDate>this.$id(`pt-nurse-rec-add-act-date`)).value : this.dateFormat("");
        (<CmnTime>this.$id(`pt-nurse-rec-add-act-time`)).value = (<CmnTime>this.$id(`pt-nurse-rec-add-act-time`)).value !== "" ? (<CmnTime>this.$id(`pt-nurse-rec-add-act-time`)).value : this.timeFormat("");
        this.$id(`pt-nurse-rec-add-user-name`).textContent = this.userName;
    }
    protected protUpdateForm(itemId: number | null, rocordId: string | null) {
        let orgActDateElem = this.$id(`pt-nurse-rec-list2-act-date${itemId}`).textContent ?? "";
        let orgActDate = this.dateFormat(orgActDateElem);
        let orgActTimeElem = this.$id(`pt-nurse-rec-list2-act-time${itemId}`).textContent ?? "";
        let orgActTime = this.timeFormat(orgActTimeElem);
        let orgActDetail = this.$id(`pt-nurse-rec-list2-act-detail${itemId}`).textContent;
        let orgNote = this.$id(`pt-nurse-rec-list2-note${itemId}`).textContent;

        let inputActDateElem = (<CmnDate>this.$id(`pt-nurse-rec-update-act-date`))
        let inputActTimeElem = (<CmnTime>this.$id(`pt-nurse-rec-update-act-time`))
        let inputActDetailElem = (<HTMLTextAreaElement>this.$id(`pt-nurse-rec-update-act-detail`))
        let inputNoteElem = (<HTMLTextAreaElement>this.$id(`pt-nurse-rec-update-note`))

        //一時保存されていたら描写
        console.log(this.updateTemporaryData, rocordId)
        if (rocordId && this.updateTemporaryData.hasOwnProperty(rocordId)) {
            console.log("protdata",this.updateTemporaryData[rocordId],this.updateTemporaryData[rocordId].actDetail)
            inputActDateElem.value = this.updateTemporaryData[rocordId].actDate ?? orgActDate;
            inputActTimeElem.value = this.updateTemporaryData[rocordId].actTime ?? orgActTime;
            inputActDetailElem.value = this.updateTemporaryData[rocordId].actDetail ?? orgActDetail;
            inputNoteElem.value = this.updateTemporaryData[rocordId].note ?? orgNote;
        } else {
            inputActDateElem.value = orgActDate;
            inputActTimeElem.value = orgActTime;
            inputActDetailElem.value = orgActDetail ?? "";
            inputNoteElem.value = orgNote ?? "";
        }
        this.$id(`pt-nurse-rec-update-user-name`).textContent = this.userName;
        // document.querySelector('label[for=pt-nurse-rec-update-file1]').textContent = fileName
    }
    protected temporaryUpdate():void{
        let inputActDateElem = (<CmnDate>this.$id(`pt-nurse-rec-update-act-date`))
        let inputActTimeElem = (<CmnTime>this.$id(`pt-nurse-rec-update-act-time`))
        let inputActDetailElem = (<HTMLTextAreaElement>this.$id(`pt-nurse-rec-update-act-detail`))
        let inputNoteElem = (<HTMLTextAreaElement>this.$id(`pt-nurse-rec-update-note`))
        if (this.updateTargetId != null) {
            // let temp = new UpdateTemporaryData();
            let temp = {actDate:"",actTime:"",actDetail:"",note:""};
            temp.actDate = inputActDateElem.value;
            temp.actTime = inputActTimeElem.value;
            temp.actDetail = inputActDetailElem.value;
            temp.note = inputNoteElem.value;
            console.log("333333333",this.updateTargetId)
            this.updateTemporaryData[this.updateTargetId] = temp
        }else {
            console.error("updateTarget unknown");
        }
    }
    // protected async getFileBinary(inputId: string): string | null{
    //     let inputElem = this.$id(inputId);
    //     let file = (<HTMLInputElement>inputElem).files ?? null;
    //     let reader = new FileReader();
    //     if (file) {
    //         reader.onload = function ( event ) {
    //             return event.target.result;
    //         }
    //         reader.readAsText(file[0]);
    //     }
    // }

}

class UpdateTemporaryData {
    public actDate!: string | null;
    public actTime!: string | null;
    public actDetail!: string | null;
    public note!: string | null;
}
