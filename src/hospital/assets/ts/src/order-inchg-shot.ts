import { API } from "./api";
import { MCHTMLElement } from "./basic";
import $ from 'jquery'
import 'jqueryui'

import html from "./order-inchg-shot.html";

export class OrderInchgShot extends MCHTMLElement {

	private floorId: string = "all";
	private timestamp: number = (new Date()).getTime();

	public constructor() {
		super(html);
		this.bindAction();
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}
		this.fetchFloorLists();
	}

	presentHandler(tagName: string, data: any) {
		console.log("zxczxczxczxcxxxx", data);
		// バーコード読み取りページから結果を受け取る
		switch (tagName) {
			case 'mc-order-inchg-barcode':

				this.checkMedBarcode(data);
				break;
		}
	}

	bindAction() {
		$('#mc-order-inchg-shot-date-input').change((ev) => {
			const target = ev.target;
			if (target instanceof HTMLInputElement) {
				this.timestamp = new Date(target.value || '').getTime();
				this.fetchData();
			}
		});

		this.$id('mc-order-inchg-shot-date').addEventListener('click', (_) => {
			this.$id('mc-order-inchg-shot-date-input').focus();
		});

		this.$id('mc-order-inchg-shot-nextdate').addEventListener('click', (_) => {
			this.timestamp += 86400000;
			$('#mc-order-inchg-shot-date-input').datepicker("setDate", new Date(this.timestamp));
			this.fetchData();
		});

		this.$id('mc-order-inchg-shot-prevdate').addEventListener('click', (ev) => {
			this.timestamp -= 86400000;
			$('#mc-order-inchg-shot-date-input').datepicker("setDate", new Date(this.timestamp));
			this.fetchData();
		});

		this.$id('mc-order-incharge-injection-floor').addEventListener('click', (ev) => {
			if (ev.target instanceof HTMLElement) {
				this.floorId = ev.target.getAttribute('data-floor-id') || 'all';
				this.fetchData();
			}
		});

		this.$id('mc-order-inchg-shot-date').addEventListener('click', (_) => {
			this.$id('mc-order-inchg-shot-date-input').focus();
		});

		this.$id('mc-order-inchg-shot-modal-pdf-1').addEventListener('click', (_) => {
			this.printPdf("1");
		});

		this.$id('mc-order-inchg-shot-modal-pdf-2').addEventListener('click', (_) => {
			this.printPdf("2");
		});

		this.$id('mc-order-inchg-shot-modal-pdf-3').addEventListener('click', (_) => {
			this.printPdf("3");
		});
	}

	async printPdf(type: string) {
		let res = await API.printPdf(type, this.floorId, this.isoDate(this.timestamp));
		if (!res.ok) return;

		let pdfUrl = await res.json();
		window.open(pdfUrl, '_blank');
	}

	setupCheckAction() {
		let btnCheckBoxs = this.$queryAll('.mc-order-inchg-barcode-btn-scan');
		if (btnCheckBoxs instanceof Array) {
			for (let elem of btnCheckBoxs) {
				elem.removeEventListener('click');
			}
		}

		btnCheckBoxs.forEach((elem) => {
			elem.addEventListener('click', (ev) => {
				if (ev.currentTarget instanceof HTMLElement) {
					let object = JSON.parse(ev.currentTarget.getAttribute('data-object') || '{}');

					console.log(object);
					let step = ev.currentTarget.getAttribute('data-step') || '';
					let index = ev.currentTarget.getAttribute('data-key') || '';
					let checked = ev.currentTarget.getAttribute('data-checked');

					let status = true;

					let startDate = this.isoDate(this.timestamp);

					// 前のチェックボックスをチェック
					let stepInt = parseInt(step);

					// チェックを外す
					if (checked === 'checked') {
						if (stepInt < 3) {
							const div = this.$id(`mc-order-inchg-med-div-${stepInt + 1}-${index}`);
							console.log(`mc-order-inchg-med-div-${stepInt + 1}-${index}`);
							if (div instanceof HTMLDivElement && div.getAttribute('data-checked') == 'checked') {
								alert('チェックが外せません。')
								return;
							}
						}

						this.uncheckMedBarcode(Object.assign(object, {startDate: startDate, index: index,}));
						return;
					}

					//　チェックを入れる
					if (stepInt == 2) {
						// ステップ１でチェックされたかどうかチェック
						const div = this.$id(`mc-order-inchg-med-div-1-${index}`);
						if (div instanceof HTMLDivElement && div.getAttribute('data-checked') != 'checked') {
							alert("薬剤準備を実施してください。");
							return;
						}
					}
					else if (stepInt == 3) {
						// ステップ２で実施されていない場合は確認メッセージを出す
						const div = this.$id(`mc-order-inchg-med-div-2-${index}`);
						if (div instanceof HTMLDivElement && div.getAttribute('data-checked') != 'checked') {
							let ok = confirm('薬剤混合が実施されていませんが、よろしいですか。');
							if (!ok) return;
							status = false;
						}
					}

					// カメラ立ち上げる確認
					let ok = false;
					if (stepInt == 2) {
						ok = confirm("バーコードシールをスキャンしてください。");
					}
					else if (stepInt == 3) {
						ok = confirm("患者のバーコードをスキャンしてください。");
					}
					else {
						ok = confirm("薬剤のバーコードをスキャンしてください。");
					}
					if(ok){
						// バーコード読み取りページを呼び出す
						this.present('mc-order-inchg-barcode', Object.assign(object, {status: status, index: index, startDate: startDate,}));
					}
				}
			});
		});
	}

	checkMedBarcode(data: any) {

		console.log('checkMedBarcode', data);
		const checkInput = this.$id(`mc-order-inchg-med-check-${data.step}-${data.index}`);
		if (checkInput instanceof HTMLInputElement) {
			checkInput.checked = true;
		}

		const span = this.$id(`mc-order-inchg-med-text-${data.step}-${data.index}`);
		if (span instanceof HTMLElement) {
			span.textContent = data.body.first_name + data.body.family_name
		}

		const uncheck = this.$id(`mc-order-inchg-med-uncheck-${data.step}-${data.index}`);
		if (uncheck instanceof HTMLElement && !data.status) {
			uncheck.textContent = '（未確認）';
		}

		const date = this.$id(`mc-order-inchg-med-date-${data.step}-${data.index}`);
		if (date instanceof HTMLElement) {
			date.textContent = data.body.datetime;
		}

		const div = this.$id(`mc-order-inchg-med-div-${data.step}-${data.index}`);
		if (div instanceof HTMLDivElement) {
			div.setAttribute('data-checked', 'checked');
		}

	}

	async uncheckMedBarcode(data: any) {
		alert(JSON.stringify(data));
		var okClicked = confirm('この実施ステータスを取り消します。よろしいですか。');
		if (okClicked) {
			// request API
			let res = await API.orderInjectionUncheck({
				floor_id: data.floorID,
                order_id: data.orderID,
                step: data.step,
                start_date: data.startDate,
                order_type: data.orderType,
                patient_id: data.patientID
			})
			if(!res.ok) return;
			let passed = (await res.json()).passed

			if(!passed) return;

			const checkInput = this.$id(`mc-order-inchg-med-check-${data.step}-${data.index}`);
			if (checkInput instanceof HTMLInputElement) {
				checkInput.checked = false;
			}

			const span = this.$id(`mc-order-inchg-med-text-${data.step}-${data.index}`);
			if (span instanceof HTMLElement) {
				span.textContent = '';
			}

			const uncheck = this.$id(`mc-order-inchg-med-uncheck-${data.step}-${data.index}`);
			if (uncheck instanceof HTMLElement) {
				uncheck.textContent = '';
			}

			const date = this.$id(`mc-order-inchg-med-date-${data.step}-${data.index}`);
			if (date instanceof HTMLElement) {
				date.textContent = '';
			}

			const div = this.$id(`mc-order-inchg-med-div-${data.step}-${data.index}`);
			if (div instanceof HTMLDivElement) {
				div.setAttribute('data-checked', 'unchecked');
			}
		}
	}

	japanDate(timestamp: number): string {
		//2020年1月12日
		return window.moment(new Date(timestamp)).format('YYYY年M月D日');
	}
	isoDate(timestamp: number): string {
		// 2021-01-18
		return window.moment(new Date(timestamp)).format('YYYY-MM-DD HH:mm:ss');
	}

	createElementFromHTML(htmlString: string) {
		var div = document.createElement('div');
		div.innerHTML = htmlString.trim();
		return div.firstChild;
	}

	async fetchFloorLists() {
		const res = await API.getFloorLists();
		if (!res.ok) {
			alert('フロアー一覧を取得できません。');
			return;
		}
		const json = await res.json();
		this.$id('mc-order-incharge-injection-floor').textContent = '';
		if (json instanceof Array) {
			const temp = `<li class="nav-item" id="{0}"><a class="nav-link {1}"  data-toggle="tab" data-floor-id="{2}">{3}</a></li>`;
			for (let [index, item] of json.entries()) {
				if (index === 0) {
					this.floorId = item.floor_id;
					this.fetchData();
				}
				let eleStr = this.fillDataToTemp(temp, true, `mc-order-incharge-injection-floor-li-${index}`, index === 0 ? 'active' : '', item.floor_id, `${item.floor_num}F`)
				this.$id('mc-order-incharge-injection-floor').appendChild(this.createElementFromHTML(eleStr) || new Node());
			}
		}
		this.$id('mc-order-incharge-injection-floor').appendChild(this.createElementFromHTML('<li class="nav-item"><a class="nav-link" href="#All" data-toggle="tab" data-floor-id="all">全て</a></li>') || new Node());
	}

	async fetchData() {
		// remove all template
		this.$id('mc-order-inchg-shot-date').innerText = this.japanDate(this.timestamp)
		const res = await API.getOrderInchgShot(this.floorId, this.isoDate(this.timestamp));

		const srcElem = <HTMLTableRowElement>this.$id("mc-order-incharge-injection-tr");
		const tbody = <HTMLElement>srcElem.parentElement;
		const rows = tbody.getElementsByTagName('tr');
		const length = rows.length;
		for (let i = 1; i < length; i++) {
			tbody.removeChild(rows[i]);
		}

		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		const json = await res.json();
		if (json instanceof Array) {
			// fill datatable
			const tdGroupTemp = `
			<div class="td-group">
				<div class="mc-order-inchg-barcode-btn-scan" id="mc-order-inchg-med-div-{step}-{index}" data-checked="{checked}" data-key="{index}" data-object="{object}" data-step={step}>
					<input type="checkbox" id="mc-order-inchg-med-check-{step}-{index}" style="pointer-events:none" disabled {checked}>
				</div>
				<div class="content">
					<span id="mc-order-inchg-med-text-{step}-{index}">{act_user}</span>
					<div id="mc-order-inchg-med-uncheck-{step}-{index}" style="color:red">{act_status}</div>
					<div id="mc-order-inchg-med-date-{step}-{index}">{act_datetime}</div>
				</div>
			</div>
			`;
			for (let [index, item] of json.entries()) {
				let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
				trElem.setAttribute("id", `mc-order-incharge-injection-tr-${index}`);
				trElem.style.display = 'table-row';

				let elemPatient = this.$id("mc-order-incharge-injection-td-patient", trElem);
				elemPatient.setAttribute("id", `mc-order-incharge-injection-td-patient-${index}`);
				elemPatient.innerHTML = this.fillDataToTemp('{0}<br/>{1}{2}<br/>{3}', true, item.room_number, item.family_name, item.first_name, item.orca_id);

				let elemInjection = this.$id("mc-order-incharge-injection-td-injection", trElem);
				elemInjection.setAttribute("id", `mc-order-incharge-injection-td-injection-${index}`);
				let _injectionRender = '';

				let elemStep1 = this.$id("mc-order-incharge-injection-td-step-1", trElem);
				elemStep1.setAttribute("id", `mc-order-incharge-injection-td-step-1-${index}`);
				let _elemStep1Render = '';

				let elemStep2 = this.$id("mc-order-incharge-injection-td-step-2", trElem);
				elemStep2.setAttribute("id", `mc-order-incharge-injection-td-step-2-${index}`);
				let _elemStep2Render = '';

				let elemStep3 = this.$id("mc-order-incharge-injection-td-step-3", trElem);
				elemStep3.setAttribute("id", `mc-order-incharge-injection-td-step-3-${index}`);
				let _elemStep3Render = '';


				let injectMedCodes = [];
				if (item.order_injections instanceof Array) {
					for (let [i, injection] of item.order_injections.entries()) {
						// console.log(injection, "inject");
						injectMedCodes.push(injection.code);
						_injectionRender += this.fillDataToTemp("{0} {1}<br/>", true, injection.name, injection.quantum);
					}
				}
				elemInjection.innerHTML = `ID: ${item.order_id}<br/>${_injectionRender}`;

				if (item.order_act != null) {
					let order_act = item.order_act;
					console.log(item);
					_elemStep1Render = this.fillDataToTempV2(
						tdGroupTemp,
						true, {
							index: index,
							step: 1,
							checked: order_act.act_user1 != null ? 'checked' : '',
							act_datetime: order_act.act_datetime1 || '',
							act_user: order_act.act_user1 || '',
							act_status: '',
							object: JSON.stringify({
								step: 1,
								name: order_act.act_user1,
								orderID: item.order_id,
								floorID: item.floor_id,
								patientID: item.id,
								injectMedCodes: injectMedCodes,
							})
						}
					);

					_elemStep2Render = this.fillDataToTempV2(
						tdGroupTemp,
						true, {
							step: 2,
							index: index,
							checked: order_act.act_user2 != null ? 'checked' : '',
							act_datetime: order_act.act_datetime2 || '',
							act_user: order_act.act_user2 || '',
							act_status: '',
							object: JSON.stringify({
								step: 2,
								name: order_act.act_user2,
								orderID: item.order_id,
								orcaID: item.orca_id,
								floorID: item.floor_id,
								patientID: item.id,
								injectMedCodes: injectMedCodes,
							})
						}
					);

					_elemStep3Render = this.fillDataToTempV2(
						tdGroupTemp,
						true, {
							step: 3,
							index: index,
							checked: order_act.act_user3 != null ? 'checked' : '',
							act_datetime: order_act.act_datetime3 || '',
							act_user: order_act.act_user3 || '',
							act_status: order_act.act_status3 === 'false'? '（未確認）' : '',
							object: JSON.stringify({
								step: 3,
								name: order_act.act_user3,
								orderID: item.order_id,
								orcaID: item.orca_id,
								floorID: item.floor_id,
								patientID: item.id,
								injectMedCodes: injectMedCodes,
							})
						});
				} else {
					_elemStep1Render = this.fillDataToTempV2(tdGroupTemp, true, {
						step: 1,
						index: index,
						checked: '',
						act_datetime: '',
						act_user: '',
						act_status: '',
						object: JSON.stringify({
							step: 1,
							name: '',
							orderID: item.order_id,
							orcaID: item.orca_id,
							floorID: item.floor_id,
							patientID: item.id,
							injectMedCodes: injectMedCodes,
						})});
					_elemStep2Render = this.fillDataToTempV2(tdGroupTemp, true, {
						step: 2,
						index: index,
						checked: '',
						act_datetime: '',
						act_user: '',
						act_status: '',
						object: JSON.stringify({
							step: 2,
							name: '',
							orderID: item.order_id,
							orcaID: item.orca_id,
							floorID: item.floor_id,
							patientID: item.id,
							injectMedCodes: injectMedCodes,
						})});
					_elemStep3Render = this.fillDataToTempV2(tdGroupTemp, true, {
						step: 3,
						index: index,
						checked: '',
						act_datetime: '',
						act_user: '',
						act_status: '',
						object: JSON.stringify({
							step: 3,
							name: '',
							orderID: item.order_id,
							orcaID: item.orca_id,
							floorID: item.floor_id,
							patientID: item.id,
							injectMedCodes: injectMedCodes,
						})});
				}

				elemStep1.innerHTML = _elemStep1Render;
				elemStep2.innerHTML = _elemStep2Render;
				elemStep3.innerHTML = _elemStep3Render;

				(<HTMLElement>srcElem.parentElement).appendChild(trElem);
			}
		}

		this.setupCheckAction()
	}
}
