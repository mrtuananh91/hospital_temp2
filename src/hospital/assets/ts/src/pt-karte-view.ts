import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt-karte-view.html";

export class PtKarteView extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.setChangeH13nDayEvent(this.show);
		this.show(null);
	}

	private async show(evt: Event | null) {
		// テーブル全削除
		this.$queryAll("div[id^='pt-karte-view-rec-section'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});

		const patientId = <string>this.patientId;
		const h13nId = <string>this.h13nId;
		const res = await API.getRecords(patientId, h13nId);
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		const json = await res.json();
		const recs: any[] | null = json["records"];
		if (!recs) return;

		let srcElem = <HTMLDivElement>this.$id("pt-karte-view-rec-section");
		let prevElem = srcElem;
		const self = this;
		recs.forEach((rec: { [key: string]: any }, i) => {
			i++;
			let divElem = <HTMLDivElement>srcElem.cloneNode(true);
			divElem.style.removeProperty("display");
			divElem.setAttribute("id", `pt-karte-view-rec-section${i}`);
			if (i > 1) {
				divElem.classList.add("mt-4");
			}

			// Title
			const titleElem = self.$id("pt-karte-view-rec-title", divElem);

			// Type
			const recType = rec["record_type"];
			let elem = self.$id("pt-karte-view-rec-type", divElem);
			elem.setAttribute("id", `pt-karte-view-rec-type${i}`);
			switch (recType) {
				case 0: // 診療記録
					const title = rec["karte_title"];
					elem.textContent = title ? title : "診療記録";
					break;
				case 1: // 看護記録
					elem.textContent = "看護記録";
					titleElem.classList.add("bgc-mh001");
					break;
				case 2: // リハ記録
					elem.textContent = "リハ記録";
					titleElem.classList.add("bgc-mh002");
					break;
				case 3: // 栄養士記録
					elem.textContent = "栄養士記録";
					titleElem.classList.add("bgc-mh003");
					break;
			}

			// ActDatetime
			elem = self.$id("pt-karte-view-rec-datetime", divElem);
			elem.setAttribute("id", `pt-karte-view-rec-datetime${i}`);
			elem.textContent = self.datetime(rec["act_start_datetime"]);

			// 記録者
			elem = self.$id("pt-karte-view-rec-username", divElem);
			elem.setAttribute("id", `pt-karte-view-rec-username${i}`);
			elem.textContent = rec["input_user_name"];

			// Detail
			elem = self.$id("pt-karte-view-rec-body-body", divElem);
			elem.setAttribute("id", `pt-karte-view-rec-body-body${i}`);
			elem.textContent = rec["karte_body"];

			// Note
			elem = self.$id("pt-karte-view-rec-body-memo", divElem);
			elem.setAttribute("id", `pt-karte-view-rec-body-memo${i}`);
			elem.textContent = rec["karte_memo"];

			(<HTMLElement>prevElem.parentElement).insertBefore(divElem, prevElem.nextSibling);
			prevElem = divElem;
		});
	}
}
