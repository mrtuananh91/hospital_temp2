import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./order-order-list.html";

export class OrderOrderList extends MCHTMLElement {

	public constructor() {
		super(html);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.blockStart();
		try {
			this.show();
		} finally {
			this.blockEnd();
		}
	}

	private async show() {
		let floor = "1";
		let selectedDate = "2021-01-02";
		const res = await API.getOrderOrderList(floor, selectedDate);
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		// テーブル全削除
		this.$queryAll("tr[id^='order-order-list-item'").forEach((elem) => {
			if (elem.style.display !== "none") {
				(<HTMLElement>elem.parentElement).removeChild(elem);
			}
		});

		const json = await res.json();
		const orders: any[] | null = json; //json["orders"];
		if (!orders) return;

		let srcElem = <HTMLTableRowElement>this.$id("order-order-list-item");
		let prevElem = srcElem;
		const self = this;
		orders.forEach((pt: { [key: string]: any }, i) => {
			i++;
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.display = "table-row";
			trElem.setAttribute("id", `order-order-list-item${i}`);

			// ID & 氏名
			let elem = self.$id("order-order-list-pt-name", trElem);
			elem.setAttribute("id", `order-order-list-pt-name${i}`);
			elem.textContent = pt["room_id"] + '　' + pt["family_name"] + pt["first_name"];
			this.displayModal(elem);

			// 注射
			elem = self.$id("order-order-list-inject", trElem);
			elem.setAttribute("id", `order-order-list-inject${i}`);
			elem.textContent = pt["inject"];
			this.displayModal(elem);

			// 食事・朝
			elem = self.$id("order-order-list-eat-morning", trElem);
			elem.setAttribute("id", `order-order-list-eat-morning${i}`);
			elem.textContent = pt["eat_morning"];
			this.displayModal(elem);
			// 食事・昼
			elem = self.$id("order-order-list-eat-lunch", trElem);
			elem.setAttribute("id", `order-order-list-eat-lunch${i}`);
			elem.textContent = pt["eat_lunch"];
			this.displayModal(elem);
			// 食事・夜
			elem = self.$id("order-order-list-eat-dinner", trElem);
			elem.setAttribute("id", `order-order-list-eat-dinner${i}`);
			elem.textContent = pt["eat_dinner"];
			this.displayModal(elem);

			// 処方薬・朝
			elem = self.$id("order-order-list-med-morning", trElem);
			elem.setAttribute("id", `order-order-list-med-morning${i}`);
			elem.textContent = pt["med_morning"];
			this.displayModal(elem);
			// 処方薬・昼
			elem = self.$id("order-order-list-med-lunch", trElem);
			elem.setAttribute("id", `order-order-list-med-lunch${i}`);
			elem.textContent = pt["med_lunch"];
			this.displayModal(elem);
			// 処方薬・夜
			elem = self.$id("order-order-list-med-dinner", trElem);
			elem.setAttribute("id", `order-order-list-med-dinner${i}`);
			elem.textContent = pt["med_dinner"];
			this.displayModal(elem);

			// リハビリ
			elem = self.$id("order-order-list-reha", trElem);
			elem.setAttribute("id", `order-order-list-reha${i}`);
			elem.textContent = pt["reha"];
			this.displayModal(elem);

			// 検査
			elem = self.$id("order-order-list-inspec", trElem);
			elem.setAttribute("id", `order-order-list-inspec${i}`);
			elem.textContent = pt["inspec"];
			this.displayModal(elem);

			// レントゲン
			elem = self.$id("order-order-list-roentgen", trElem);
			elem.setAttribute("id", `order-order-list-roentgen${i}`);
			elem.textContent = pt["roentgen"];
			this.displayModal(elem);

			// 手術
			elem = self.$id("order-order-list-surgery", trElem);
			elem.setAttribute("id", `order-order-list-surgery${i}`);
			elem.textContent = pt["surgery"];
			this.displayModal(elem);

			// 輸血
			elem = self.$id("order-order-list-transfusion", trElem);
			elem.setAttribute("id", `order-order-list-transfusion${i}`);
			elem.textContent = pt["transfusion"];
			this.displayModal(elem);

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});
	}

	private displayModal(elem: HTMLElement) {
		elem.addEventListener("click", (evt: Event) => {
			elem.setAttribute("data-toggle", `modal`);
			elem.setAttribute("data-target", `#modal-order-order-list`);
			// document.getElementById('modal-order-order-list-name')?.innerHTML
		});
	}
}