import { MCHTMLElement } from "./basic";

import html from "./bed.html";

export class Bed extends MCHTMLElement {
	private static menuBtnTabMap = {
		"bed-bed-menu": "mc-bed-bed",
		"bed-report-menu": "mc-bed-report",
	};
	private static startMenu = "bed-bed-menu";

	public constructor() {
		super(html, Bed.menuBtnTabMap, null);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue == null) {
			return;
		}

		this.$id(this.currentMenuBtn(Bed.startMenu)).click();
	}
}