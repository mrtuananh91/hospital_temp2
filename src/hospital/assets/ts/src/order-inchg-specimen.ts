import { MCHTMLElement } from "./basic";

import html from "./order-inchg-specimen.html";

export class OrderInchgSpecimen extends MCHTMLElement {
	private static menuBtnTagMap = {
		"order-inchg-tab-4-1-tab": "mc-order-inchg-specimen-in",
		"order-inchg-tab-4-2-tab": "mc-order-inchg-specimen-out",
		"order-inchg-tab-4-3-tab": "mc-order-inchg-specimen-add",
	}
	private static startMenu = "order-inchg-tab-4-1-tab";

	public constructor() {
		super(html, OrderInchgSpecimen.menuBtnTagMap, null);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuBtn(OrderInchgSpecimen.startMenu)).click();
	}
}
