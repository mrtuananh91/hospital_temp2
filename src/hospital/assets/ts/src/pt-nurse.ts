import { MCHTMLElement } from "./basic";

import html from "./pt-nurse.html";

export class PtNurse extends MCHTMLElement {
	private static menuTabTagMap = {
		"pt-nurse-tab-1-1-tab": "mc-pt-nurse-rec",
		"pt-nurse-tab-1-2-tab": "mc-pt-nurse-vital",
		"pt-nurse-tab-1-3-tab": "mc-pt-nurse-assess",
		"pt-nurse-tab-1-4-tab": "mc-pt-nurse-plan",
		"pt-nurse-tab-1-5-tab": "mc-pt-nurse-doc",
	}
	private static startTab = "pt-nurse-tab-1-1-tab";

	public constructor() {
		super(html, null, PtNurse.menuTabTagMap);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuTab(PtNurse.startTab)).click();
	}
}