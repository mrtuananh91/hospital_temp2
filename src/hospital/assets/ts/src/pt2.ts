import * as Moment from "moment";
import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt2.html";

export class Pt2 extends MCHTMLElement {
	private static menuBtnTagMap = {
		"pt-karte-menu": "mc-pt-karte",
		"pt-sum-menu": "mc-pt-sum",
		"pt-order-menu": "mc-pt-order",
		"pt-nurse-menu": "mc-pt-nurse",
		"pt-reha-menu": "mc-pt-reha",
		"pt-dt-menu": "mc-pt-dt",
		"pt-skd-menu": "mc-pt-skd",
	}
	private static startMenu = "pt-karte-menu";

	private h13nsDaysElem: HTMLSelectElement;

	public constructor() {
		super(html, Pt2.menuBtnTagMap, null);

		this.h13nsDaysElem = this.$id("pt-h13ns-day");
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue == null) {
			return;
		}

		this.show(newValue);
	}

	private async show(patientId: string) {
		this.blockStart();
		try {
			const res = await API.searchPatients(patientId);
			if (!res.ok) {
				this.errorMsg(res);
				return;
			}

			const json = await res.json();
			const pts = json["patients"];
			const pt = pts[0];
			this.$id("pt-orcaid").textContent = pt["orca_id"];
			this.$id("pt-name").textContent = `${pt["family_name"]} ${pt["first_name"]}`;
			this.$id("pt-gender").textContent = this.gender(pt["gender"]);
			const [s, birthday] = this.birthday(pt["birthday"]);
			this.$id("pt-birthday").textContent = s;
			this.$id("pt-age").textContent = `${this.age(birthday)}`;
			this.$id("pt-bloodtype").textContent = pt["blood_type"];
			if (this.h13nsDaysElem.options.length > 0) {
				for (let i = this.h13nsDaysElem.options.length - 1; i >= 0; i--) {
					this.h13nsDaysElem.removeChild(this.h13nsDaysElem.options[i]);
				}
			}
			if (pt["h13n_days"]) {
				for (let h13n of pt["h13n_days"]) {
					const optElem = document.createElement("option");
					optElem.value = h13n["id"];
					let s = "";
					if (h13n["start_datetime"]) {
						s = Moment(h13n["start_datetime"]).format("YYYY/M/D");
					}
					if (h13n["end_datetime"]) {
						s += ("-" + Moment(h13n["end_datetime"]).format("YYYY/M/D"));
					}
					optElem.textContent = s;
					this.h13nsDaysElem.options.add(optElem);
				}
			}
			sessionStorage.setItem(MCHTMLElement.SS_PATIENT_ID, patientId);
			this.$id(this.currentMenuBtn(Pt2.startMenu)).click();
		} finally {
			this.blockEnd();
		}
	}
}
