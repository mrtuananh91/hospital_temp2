import { API } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./login.html";

export class Login extends MCHTMLElement {
	private actElem: HTMLInputElement;
	private pwdElem: HTMLInputElement;
	private formElem: HTMLFormElement;
	private errMsgElem: HTMLParagraphElement;

	public constructor() {
		super(html);

		this.actElem = <HTMLInputElement>this.$id("login-id");
		this.pwdElem = <HTMLInputElement>this.$id("login-password");
		this.formElem = <HTMLFormElement>this.$id("login-form");
		this.errMsgElem = <HTMLParagraphElement>this.$id("login-errmsg");
		this.setLoginEvent();
	}

	private setLoginEvent(): void {
		const self = this;
		this.formElem.addEventListener("submit", async (evt: Event) => {
			evt.stopPropagation();
			evt.preventDefault();

			self.blockStart();
			try {
				const res = await API.login(self.actElem.value, self.pwdElem.value);
				if (res.ok) {
					location.href = "/home";
				} else {
					self.errMsgElem.style.visibility = "visible";
					self.actElem.classList.add("is-invalid");
					self.pwdElem.classList.add("is-invalid");
					self.pwdElem.value = "";
					self.pwdElem.focus();
					return false;
				}
			} finally {
				self.blockEnd();
			}
		});
	}
}