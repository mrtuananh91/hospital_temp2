import { MCHTMLElement } from "./basic";

import html from "./cmn-time.html";

export class CmnTime extends MCHTMLElement {
	private inputElem: HTMLInputElement;

	public constructor() {
		super(html);

		const id = <string>this.getAttribute("id") + "-time";
		this.$id("cmn-timepicker").setAttribute("id", id);

		const label = this.getAttribute("data-label");
		const labelElem = this.$tag("label")
		labelElem.setAttribute("for", id);
		labelElem.textContent = label;
		if (!label) {
			labelElem.style.display = "none";
		}

		this.inputElem = this.$tag("input");
		this.inputElem.setAttribute("data-target", `#${id}`);

		const divElem = this.$tag("div[data-toggle]");
		divElem.setAttribute("data-target", `#${id}`);
	}

	public get value(): string {
		return this.inputElem.value;
	}

	public set value(v: string) {
		this.inputElem.value = v;
	}

	/*public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}
	}*/
}