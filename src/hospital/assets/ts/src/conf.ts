import { MCHTMLElement } from "./basic";

import html from "./conf.html";


export class Conf extends MCHTMLElement {
	private static menuBtnTagMap = {
		"conf-user-menu": "mc-conf-user"
	}
	private static startMenu = "conf-user-menu";

	public constructor() {
		super(html, Conf.menuBtnTagMap, null);
	}


	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue == null) {
			return;
		}

		this.$id(this.currentMenuBtn(Conf.startMenu)).click();
	}
}
