import { API, Patient } from "./api";
import { MCHTMLElement } from "./basic";

import html from "./pt-add.html";

export class PtAdd extends MCHTMLElement {
	private formElem: HTMLFormElement;
	private idElem: HTMLInputElement;
	private familyNameElem: HTMLInputElement;
	private firstNameElem: HTMLInputElement;
	private familyNameKanaElem: HTMLInputElement;
	private firstNameKanaElem: HTMLInputElement;
	private genderElem: HTMLSelectElement;
	private deadElem: HTMLInputElement;
	private deadDateTimeElem: HTMLInputElement;
	private birthdayElem: HTMLInputElement;
	private ageElem: HTMLTableDataCellElement;
	private bloodTypeElem: HTMLInputElement;
	private pictElem: HTMLInputElement;
	private postalCodeElem: HTMLInputElement;
	private countryElem: HTMLInputElement;
	private addressElem: HTMLInputElement;
	private telElem: HTMLInputElement;
	private mobilephoneElem: HTMLInputElement;
	private emgName1Elem: HTMLInputElement;
	private emgName2Elem: HTMLInputElement;
	private emgName3Elem: HTMLInputElement;
	private emgTel1Elem: HTMLInputElement;
	private emgTel2Elem: HTMLInputElement;
	private emgTel3Elem: HTMLInputElement;
	private emgMobilephone1Elem: HTMLInputElement;
	private emgMobilephone2Elem: HTMLInputElement;
	private emgMobilephone3Elem: HTMLInputElement;
	private emgRel1Elem: HTMLInputElement;
	private emgRel2Elem: HTMLInputElement;
	private emgRel3Elem: HTMLInputElement;
	private keyPersonNameElem: HTMLInputElement;
	private keyPersonRelElem: HTMLInputElement;
	private familyInfoElem: HTMLTextAreaElement;
	private diseaseHistoryElem: HTMLTextAreaElement;
	private familyDiseaseHistoryElem: HTMLTextAreaElement;
	private noteElem: HTMLTextAreaElement;

	public constructor() {
		super(html);

		this.formElem = this.$id("pt-add-form");
		this.idElem = this.$id("pt-add-id");
		this.familyNameElem = this.$id("pt-add-familyname");
		this.firstNameElem = this.$id("pt-add-firstname");
		this.familyNameKanaElem = this.$id("pt-add-familyname-kana");
		this.firstNameKanaElem = this.$id("pt-add-firstname-kana");
		this.genderElem = this.$id("pt-add-gender");
		this.deadElem = this.$id("pt-add-dead");
		this.deadDateTimeElem = this.$id("pt-add-dead-datetime");
		this.birthdayElem = this.$id("pt-add-birthday");
		this.ageElem = this.$id("pt-add-age");
		this.bloodTypeElem = this.$id("pt-add-bloodtype");
		this.pictElem = this.$id("pt-add-pict");
		this.postalCodeElem = this.$id("pt-add-postalcode");
		this.countryElem = this.$id("pt-add-country");
		this.addressElem = this.$id("pt-add-address");
		this.telElem = this.$id("pt-add-tel");
		this.mobilephoneElem = this.$id("pt-add-mobilephone");
		this.emgName1Elem = this.$id("pt-add-emgname1");
		this.emgName2Elem = this.$id("pt-add-emgname2");
		this.emgName3Elem = this.$id("pt-add-emgname3");
		this.emgTel1Elem = this.$id("pt-add-emgtel1");
		this.emgTel2Elem = this.$id("pt-add-emgtel2");
		this.emgTel3Elem = this.$id("pt-add-emgtel3");
		this.emgMobilephone1Elem = this.$id("pt-add-emgmobilephone1");
		this.emgMobilephone2Elem = this.$id("pt-add-emgmobilephone2");
		this.emgMobilephone3Elem = this.$id("pt-add-emgmobilephone3");
		this.emgRel1Elem = this.$id("pt-add-emgrel1");
		this.emgRel2Elem = this.$id("pt-add-emgrel1");
		this.emgRel3Elem = this.$id("pt-add-emgrel1");
		this.keyPersonNameElem = this.$id("pt-add-keyperson-name");
		this.keyPersonRelElem = this.$id("pt-add-keyperson-rel");
		this.familyInfoElem = this.$id("pt-add-family-info");
		this.diseaseHistoryElem = this.$id("pt-add-disease-history");
		this.familyDiseaseHistoryElem = this.$id("pt-add-family-disease-history");
		this.noteElem = this.$id("pt-add-note");

		this.setDeadEvent();
		this.setBirthdayEvent();
		this.setSubmitEvent();
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		//this.initElems();
	}

	protected initElems() {
		super.initElems();
		this.deadDateTimeElem.disabled = true;
	}

	private setDeadEvent(): void {
		const self = this;
		this.deadElem.addEventListener("change", (evt: Event) => {
			self.deadDateTimeElem.disabled = !self.deadElem.checked;
		});
	}

	private setBirthdayEvent(): void {
		const self = this;
		this.birthdayElem.addEventListener("change", (evt: Event) => {
			self.ageElem.textContent = `${self.age(self.birthdayElem.value)}歳`;
		});
	}

	private setSubmitEvent(): void {
		const self = this;
		this.formElem.addEventListener("submit", async (evt: Event) => {
			evt.stopPropagation();
			evt.preventDefault();

			self.blockStart();
			try {
				let pt = new Patient();
				pt.id = "";
				pt.orcaId = self.idElem.value;
				pt.familyName = self.familyNameElem.value;
				pt.firstName = self.firstNameElem.value;
				pt.familyNameKana = self.familyNameKanaElem.value;
				pt.firstNameKana = self.firstNameKanaElem.value;
				pt.gender = parseInt(self.genderElem.value, 10);
				pt.birthday = self.birthdayElem.value ? self.birthdayElem.value : null;
				pt.bloodType = self.bloodTypeElem.value;
				pt.postalCode = self.postalCodeElem.value ? self.postalCodeElem.value : null;
				pt.country = self.countryElem.value ? self.countryElem.value : null;
				pt.address = self.addressElem.value ? self.addressElem.value : null;
				pt.tel = self.telElem.value ? self.telElem.value : null;
				pt.mobilephone = self.mobilephoneElem.value ? self.mobilephoneElem.value : null;
				pt.emgName1 = self.emgName1Elem.value ? self.emgName1Elem.value : null;
				pt.emgName2 = self.emgName2Elem.value ? self.emgName2Elem.value : null;
				pt.emgName3 = self.emgName3Elem.value ? self.emgName3Elem.value : null;
				pt.emgTel1 = self.emgTel1Elem.value ? self.emgTel1Elem.value : null;
				pt.emgTel2 = self.emgTel2Elem.value ? self.emgTel2Elem.value : null;
				pt.emgTel3 = self.emgTel3Elem.value ? self.emgTel3Elem.value : null;
				pt.emgMobilephone1 = self.emgMobilephone1Elem.value ? self.emgMobilephone1Elem.value : null;
				pt.emgMobilephone2 = self.emgMobilephone2Elem.value ? self.emgMobilephone2Elem.value : null;
				pt.emgMobilephone3 = self.emgMobilephone3Elem.value ? self.emgMobilephone3Elem.value : null;
				pt.emgRel1 = self.emgRel1Elem.value ? self.emgRel1Elem.value : null;
				pt.emgRel2 = self.emgRel2Elem.value ? self.emgRel2Elem.value : null;
				pt.emgRel3 = self.emgRel3Elem.value ? self.emgRel3Elem.value : null;
				pt.keyPersonName = self.keyPersonNameElem.value ? self.keyPersonNameElem.value : null;
				pt.keyPersonRel = self.keyPersonRelElem.value ? self.keyPersonRelElem.value : null;
				pt.familyInfo = self.familyInfoElem.value ? self.familyInfoElem.value : null;
				pt.diseaseHistory = self.diseaseHistoryElem.value ? self.diseaseHistoryElem.value : null;
				pt.familyDiseaseHistory = self.familyDiseaseHistoryElem.value ? self.familyDiseaseHistoryElem.value : null;
				pt.note = self.noteElem.value ? self.noteElem.value : null;
				if (self.deadElem.checked) {
					pt.deadDatetime = self.deadDateTimeElem.value;
				}

				const res = await API.addPatients([pt]);
				if (!res.ok) {
					self.errorMsg(res);
					return;
				}

				self.toast("患者を登録しました。");
				self.initElems();
			} finally {
				self.blockEnd();
			}
		});
	}
}