import { MCHTMLElement } from "./basic";

import html from "./home.html";

export class Home extends MCHTMLElement {
	private static menuBtnTagMap = {
		"home-home-menu": "mc-home-home",
		"home-schedule-menu": "mc-home-schedule",
		"home-task-menu": "mc-home-task",
		"home-mysetting-menu": "mc-home-mysetting",
	};
	private static startMenu = "home-home-menu";

	public constructor() {
		super(html, Home.menuBtnTagMap, null);
	}

	public static get observedAttributes(): string[] {
		return ["open", "reload"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue == null) {
			return;
		}

		this.$id(this.currentMenuBtn(Home.startMenu)).click();
	}
}