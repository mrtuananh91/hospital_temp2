import { MCHTMLElement } from "./basic";
import { API } from "./api";
import html from "./conf-user.html";

export class ConfUser extends MCHTMLElement {

	public constructor() {
		super(html);

		//新規登録ボタン
		this.$id("conf-user-input-btn", this).addEventListener("click", (evt: Event) => {
			
			this.changeContent("conf-user-input","conf-user-area");
		});

		//戻るボタン from input
		this.$id("back-list-frominput", this).addEventListener("click", (evt: Event) => {
			this.changeContent("conf-user-view","conf-user-area");
		});
		//戻るボタン from edit
		this.$id("back-list-fromedit", this).addEventListener("click", (evt: Event) => {
			this.changeContent("conf-user-view","conf-user-area");
		});



	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}
		this.show();
	}

	private async show() {
		const res = await API.getUsers();
		
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}


		// テーブル削除
		this.removeElemByClass(".user-list-item-row");

		const json = await res.json();
		const user: any[] | null = json["users"];
		if (!user) return;


		let srcElem = <HTMLTableRowElement>this.$id("user-list-item");
		let prevElem = srcElem;
		const self = this;
		user.forEach((user: { [key: string]: any }, i) => {
			i++;
			let trElem = <HTMLTableRowElement>srcElem.cloneNode(true);
			trElem.style.display = "table-row";
			trElem.setAttribute("id", `user-list-item${i}`);
			trElem.setAttribute("class", `user-list-item-row`);

			// ID
			let elem = self.$id("user-list-id", trElem);
			elem.setAttribute("id", `user-list-id${i}`);
			elem.textContent = user["account"].substr(5);
			// 氏名
			elem = self.$id("user-list-name", trElem);
			elem.setAttribute("id", `user-list-name${i}`);
			elem.textContent = user["family_name"] + user["first_name"];

			//ボタン
			elem = self.$id("user-list-edit", trElem);
			elem.setAttribute("id", `user-list-edit${i}`);
	

			let btn = self.$id("user-list-btn", elem);
			btn.setAttribute("id", `user-list-edit-btn${i}`);
			btn.addEventListener("click", (evt: Event) => {
				self.changeContent("conf-user-edit","conf-user-area");
				self.getUser(user["id"]);
			});
			elem.appendChild(btn);

			(<HTMLElement>prevElem.parentElement).insertBefore(trElem, prevElem.nextSibling);
			prevElem = trElem;
		});
	}

	private async getUser(user_id:string){
		const res = await API.getUser(user_id);
		
		if (!res.ok) {
			this.errorMsg(res);
			return;
		}

		// データ削除
		this.deleteFormDataByClass(".conf-user-edit-item");

		const json = await res.json();
		const user: { [key: string]: any } | null = json["users"][0];
		if (!user) return;

		
		let em = <HTMLInputElement>this.$id("conf-user-edit-id");
		em.value= user["account"].substr(5);

		em = <HTMLInputElement>this.$id("conf-user-edit-family-name");
		em.value= user["family_name"];

		em = <HTMLInputElement>this.$id("conf-user-edit-first-name");
		em.value= user["first_name"];

		em = <HTMLInputElement>this.$id("conf-user-edit-family-name-kana");
		em.value= user["family_name_kana"];

		em = <HTMLInputElement>this.$id("conf-user-edit-first-name-kana");
		em.value= user["first_name_kana"];

		em = <HTMLInputElement>this.$id("conf-user-edit-gender");
		em.value= user["gender"];



	}
}