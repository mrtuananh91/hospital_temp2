import { MCHTMLElement } from "./basic";

import html from "./order.html";

export class Order extends MCHTMLElement {
	private static menuBtnTagMap = {
		"order-order-menu": "mc-order-order",
		"order-inchg-menu": "mc-order-inchg",
		"order-emg-menu": "mc-order-emg",
	}
	private static startMenu = "order-order-menu";

	public constructor() {
		super(html, Order.menuBtnTagMap, null);
	}

	public static get observedAttributes(): string[] {
		return ["open"];
	}

	public attributeChangedCallback(attrName: string, oldValue: string, newValue: string) {
		if (attrName != "open" || newValue !== "true") {
			return;
		}

		this.$id(this.currentMenuBtn(Order.startMenu)).click();
	}


}