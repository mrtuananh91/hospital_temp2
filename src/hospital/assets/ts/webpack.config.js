"use strict"

const path = require("path");

module.exports = {
	mode: "development",
	entry: {
		"main": path.join(__dirname, "src", "entry.ts")
	},
	output: {
		"path": path.join(__dirname, "..", "dist", "js"),
		"filename": "movacalhospital.js"
	},
	devtool: false,
	optimization: {
		minimize: true
	},
	module: {
		rules: [{
			test: /\.ts$/,
			exclude: /node_modules/,
			use: "ts-loader"
		},/* {
			test: /\.(gif|jpg|png)$/,
			loader: "url-loader"
		},*/ {
			test: /\.html$/,
			loader: "html-loader",
			options: {
				attributes: false
			}
		}]
	},
	resolve: {
		extensions: [".ts"]
	},
	externals: {
		"moment": "window.moment",
		"jquery": "jQuery",
		"jqueryui": "jQuery"
	}
}