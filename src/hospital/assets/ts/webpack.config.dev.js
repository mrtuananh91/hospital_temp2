"use strict"

const path = require("path");

module.exports = {
	mode: "development",
	entry: {
		"main": path.join(__dirname, "src", "entry.ts")
	},
	output: {
		"path": path.join(__dirname, "..", "dist", "js"),
		"filename": "movacalhospital.js"
	},
	module: {
		rules: [{
			test: /\.ts$/,
			exclude: /node_modules/,
			use: "ts-loader"
		},/* {
			test: /\.(gif|jpg|png)$/,
			loader: "url-loader"
		},*/ {
			test: /\.html$/,
			loader: "html-loader",
			options: {
				attributes: false
			}
		}]
	},
	watchOptions: {
		ignored: /node_modules/,
		aggregateTimeout: 300,
		poll: 500
	},
	resolve: {
		extensions: [".ts"]
	},
	externals: {
		"moment": "window.moment",
		"jquery": "jQuery",
		"jqueryui": "jQuery",
		"quagga": "Quagga"
	}
}
