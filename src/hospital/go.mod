module hospital

go 1.15

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.6.1
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gomodule/redigo v1.8.2
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/lib/pq v1.8.0
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)
